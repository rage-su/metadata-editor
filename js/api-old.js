/*
  * api.js
	API for RAGE taxonomy services and RAGE asset services.
	
  * Copyright 2015-2016 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

var rage = (function(opt){

	var SERVER = "http://62.44.100.149:9000";
	var configTaxon = {
		baseUrl: SERVER+"/taxonomies",
		accept: "application/ld+json",
		contentType: "application/ld+json;charset=utf-8"
	};
	
	var configAsset = {
		baseUrl: SERVER+"/assets",
		accept: "application/ld+json",
		contentType: "application/ld+json;charset=utf-8"
	};
	
	function listAsset(opt) {
		var url = configAsset.baseUrl,
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", configAsset.accept);
		xhr.send();		
	}

	function list(opt) {
		var url = configTaxon.baseUrl,
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
					if(typeof(Storage) !== "undefined")
						localStorage.setItem(url,xhr.responseText);
				  	opt.success(xhr.responseText);
				  } else {
					if( typeof(Storage) !== "undefined")
					{
						var oldResponseText = localStorage.getItem(url);
						if (oldResponseText!=null)
						{
							if (opt.cache) opt.cache(url);
							opt.success(oldResponseText);
							return;
						}
					}
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", configTaxon.accept);
		xhr.send();		
	}

	function get(opt) {
		var url = configTaxon.baseUrl.concat("/", opt.taxonId),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", configTaxon.accept);
		xhr.send();		
	}
	
	function getByURI(opt) {
		var url = configTaxon.baseUrl.concat("/search", "?themeTaxonomy=", opt.themeTaxonomy),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200 && xhr.responseText!='[ ]') {
					if(typeof(Storage) !== "undefined")
						localStorage.setItem(url,xhr.responseText);
				  	opt.success(xhr.responseText);
				  } else {
					if( typeof(Storage) !== "undefined")
					{
						var oldResponseText = localStorage.getItem(url);
						if (oldResponseText!=null && oldResponseText!='[ ]')
						{
							if (opt.cache) opt.cache(url);
							opt.success(oldResponseText);
							return;
						}
					}
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", configTaxon.accept);
		xhr.send();		
	}	

	function post(opt) {
		var url = configTaxon.baseUrl.concat("/", opt.taxonId),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("POST", url, true);
		xhr.setRequestHeader("Content-Type", configTaxon.contentType);
		xhr.send(opt.data);		
	}


	function erase(opt) {
		var url = configTaxon.baseUrl.concat("/", opt.taxonId),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("DELETE", url, true);
		xhr.setRequestHeader("Accept", configTaxon.accept);
		xhr.send();			
	}

	return {
		SERVER: SERVER,
		listAsset: listAsset,
		list: list,		
		get: get,
		getByURI: getByURI,
		post: post,
		erase: erase
	}

}());

