/*

  *	metadata-ui.js
	Visual elements (widgets) used by the RAGE Metadata Editor.

  * Copyright 2015-2016 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/


UIWidgets={};

// Helper function - get style of a node
UIWidgets.style = function(node,parentNode)
{
	// test complex style (parent & child)
	if (parentNode)
	{
		var uri = parentNode.uri+' '+node.uri;
		if (uiStyles[uri])
			return uiStyles[uri];
	}

	// test simple style (child)
	if (uiStyles[node.uri])
		return uiStyles[node.uri];
	return undefined;
}

// Helper function - dispatches the creation of a node
UIWidgets.createNodeUI = function(asset,node,parentNode,originalParentNode,level) //<<< asset should disappear
{
	var row;

	var widgetName;
	var style = UIWidgets.style(node,parentNode);

	if (style && style.widget)
		widgetName = style.widget;
	else
	{
		widgetName = 'TextLine';
		console.log('Assume',node.uri,'is',widgetName);
	}

	if (!style)
	{
		console.log('Undefined style in UIWidgets.createNodeUI()');
		console.log('\tasset\t=',asset);
		console.log('\tnode\t=',node);
		console.log('\tparent\t=',parent);
		console.log('\tlevel\t=',level);
	}
	
	var widget = UIWidgets[widgetName];
	if (!widget)
		console.error('Unknown widget name',widgetName);
	row = widget.create(asset,node,parentNode,level);
	row.widget = widget;
	row.node = node;
	row.nodeParentNode = parentNode;
	row.nodeOriginalParentNode = originalParentNode;
	row.nodeLevel = level;

	if (style.tab)
	{
		row.setAttribute('tab',style.tab);
	}
	else
	{
		row.setAttribute('tab','Main');
	}
	
	if (level==1)
	{
		row.setAttribute('toplevel','1');
	}
	
	// mark the row pink to indicate a problem -- there is no widget for this row
	if (!style || !style.widget)
		row.style.backgroundColor = 'LightPink';

	return row;
}

// Helper function - dispatches the population of metadata to nodes
UIWidgets.populateNodeUI = function(asset,html,meta)
{
	html.widget.write(asset,html,meta);
}

/*	========================================================
	UIWidgets.Class

	Class. Base class for all widgets. Includes useful helper
	methods. Inherited by all widgets.

	Constructor
		Class() - constructor

	Retrieve styling info
		getWidth(node,parentNode,defaultValue)
		getHeight(node,parentNode,defaultValue)
		getHint(node,parentNode,defaultValue)
		getLabel(node,parentNode,defaultValue)
		getSkip(node,parentNode,defaultValue)
		getURI(node,parentNode,defaultValue)
		getAutoExpand(node,parentNode,defaultValue)

	Creation of HTML/DOM elements
		labelElem(row)
		createBr()
		createLabel(node,parentNode)
		createToggleLabel(node,parentNode)
		createTextBox(node,parentNode)
		createTextArea(node,parentNode)
		createListBox(options,namingStyle,addEmptyOption)
			listBoxOnChange(event)
			listBoxOnMouseKey(event)
		createLangBox()
		createFileBox()
		createBlock(asset,node,parentNode,originalParentNode,level)
		createButton(asset,name,indented,taxonomy)

	Others
		writeBlock(block,node,meta)
		xmlBlock(...)

	Methods for XML generation
		extractText(meta)
		indent(level)
		tagName = function(asset,row)

	Class hierarchy
		Class
		  +-- TextLineClass
		  |     +-- TextAreaClass
		  |     +-- TextDateClass
		  |		+-- TaxonomyConcept
		  +-- ListBoxClass
		  |     +-- ListTaxonomiesClass
		  |     +-- ListTaxonomyClass
		  +-- LabelBlockClass
          +-- ListBlockClass
	========================================================
*/
UIWidgets.Class = function()
{
}

UIWidgets.Class.prototype.getWidth = function(node,parentNode,defaultValue)
{
	if (uiStyles.skin && uiStyles.skin.width)
		return uiStyles.skin.width;
		
	var style = UIWidgets.style(node,parentNode);
	return (style&&style.width)||defaultValue;
}

UIWidgets.Class.prototype.getHeight = function(node,parentNode,defaultValue)
{
	var style = UIWidgets.style(node,parentNode);
	return (style&&style.height)||defaultValue;
}

UIWidgets.Class.prototype.getHint = function(node,parentNode,defaultValue)
{
	var style = UIWidgets.style(node,parentNode);
	return (style&&style.hint)||defaultValue;
}

UIWidgets.Class.prototype.getLabel = function(node,parentNode,defaultValue)
{
	var style = UIWidgets.style(node,parentNode);
	return (style&&style.label)||defaultValue;
}

UIWidgets.Class.prototype.getSkip = function(node,parentNode,defaultValue)
{
	var style = UIWidgets.style(node,parentNode);
	return (style&&style.skip)||defaultValue;
}

UIWidgets.Class.prototype.getURI = function(node,parentNode,defaultValue)
{
	var style = UIWidgets.style(node,parentNode);
	return (style&&style.uri)||defaultValue;
}

UIWidgets.Class.prototype.getAutoExpand = function(node,parentNode,defaultValue)
{
	var style = UIWidgets.style(node,parentNode);
	return (style&&style.autoexpand)||defaultValue;
}

UIWidgets.Class.prototype.getReadonly = function(node,parentNode,defaultValue)
{
	var style = UIWidgets.style(node,parentNode);
	return (style&&style.readonly)||defaultValue;
}

UIWidgets.Class.prototype.getHidden = function(node,parentNode,defaultValue)
{
	var style = UIWidgets.style(node,parentNode);
	return (style&&style.hidden)||defaultValue;
}

UIWidgets.Class.prototype.getColor = function(node,parentNode,defaultValue)
{
	var style = UIWidgets.style(node,parentNode);
	return style.color||defaultValue;
}

UIWidgets.Class.prototype.labelElem = function(row)
{
	return row.firstChild;
}

// Helper function - creates just a <br> element
UIWidgets.Class.prototype.createBr = function()
{
	return document.createElement('br');
}

// Helper function - creates just a static or toggable label
UIWidgets.Class.prototype.createLabel = function(node,parentNode)
{
	var elem = document.createElement('div');
	elem.className = 'label';

	// set the caption of the label
	html = this.getLabel(node,parentNode,AssetMetadata.prettyName(node.name));
	
	// add * if it is compulsory element
	if (node.min>0)	html += '<span class="compulsory"> *</span>';
	
	// wrap annotation
	if (node.annotation)
		html = '<span class="tooltip">'+html+'<span class="tooltiptext">'+node.annotation+'</span></span>';
	
	elem.innerHTML = html;

	return elem;
}

// Helper function - creates a small button
UIWidgets.Class.prototype.createButton = function(asset,name,indented,taxonomy,level,tab)
{
	var elem = document.createElement('span');
	elem.className = 'button';
	if (indented)
		elem.setAttribute('indented','');

	if (uiStyles.skin && uiStyles.skin.indent=='0')
		elem.style.marginLeft = '0';
		
	if (level==0)
	{
		elem.setAttribute('toplevel','1');
		elem.setAttribute('tab',tab);
	}

	if (taxonomy)
	{
		elem.innerHTML = 'Select concepts';
		elem.onclick = function(event){asset.selectTaxonomyConcepts(event)};
	}
	else
	{
		elem.innerHTML = '+ '+AssetMetadata.prettyName(name);
		elem.onclick = function(event){asset.cloneElement(event)};
	}
	if (options.permissions!='rw') elem.style.visibility='hidden';
	return elem;
}

// Helper function - creates a small clear button
UIWidgets.Class.prototype.createClearButton = function()
{
	var elem = document.createElement('img');
	elem.src = "images/button-delete.png";
	elem.className = 'button';
	elem.setAttribute('clear','');
	if (options.permissions!='rw') elem.style.display='none';
	elem.onclick = function(event){asset.clearElement(event)};
	return elem;
}


// Helper function - write a block
var metaIndex = 0;
UIWidgets.Class.prototype.writeBlock = function(block,node,meta)
{
//	var metaSubdex = 0;
	
	// process attributes
	//console.log('processing attributes');
	block.metaAttr={};
	for (var i=0; i<meta.attributes.length; i++)
	{
		var subNode = AssetMetadata.findNodeByURI(node,meta.attributes[i].name);
		if (!subNode)
		{
			// store all attributes, that are not defined in the schema (i.e. subNode=undefined)
			// and has no ':' in their name (i.e. to eliminate xmlns:)
			if (meta.attributes[i].name.indexOf(':')<0)
				block.metaAttr[meta.attributes[i].name] = meta.attributes[i].value;
		}
		if (!subNode) continue;

		var subRow = AssetMetadata.findElemByNode(block,subNode);
		if (!subRow) continue;

		subRow.widget.write(asset,subRow,meta.attributes[i].value);
		subRow.metaIndex = ++metaIndex;
//console.log('metaIndex'+subRow.metaIndex,subRow);		
//		subRow.metaSubdex = metaSubdex++;
	}

	// process subelements of complex node
	//console.log('processing elements');
	var oldTagName = '';
	for (var subMeta = meta.firstChild; subMeta; subMeta = subMeta.nextSibling)
		if (subMeta.nodeType==1 && subMeta.tagName) // true nodes
		{
			var uri = asset.unpack(subMeta.tagName);
			var subNode = AssetMetadata.findNodeByURI(node,uri);
			if (!subNode) continue;

			var subRow = AssetMetadata.findElemByNode(block,subNode);
			if (!subRow) continue;

			// if the row has data, and if the next row has a cloning button (e.g. [+ Title])
			// then simulate a click on this button and use the newly created row
			if (subRow.widget.hasData(subRow) && subRow.nextSibling && subRow.nextSibling.className=='button')
			{
				var event = {};
				event.target = subRow.nextSibling;
				subRow.nextSibling.onclick(event);
				subRow = subRow.nextSibling;
			}

			subRow.widget.write(asset,subRow,subMeta);

			// set the meta index, increment it only for
			// successive row of the same type
//			if (oldTagName!=subMeta.tagName)
//			{
//				metaSubdex = 0;
//				oldTagName = subMeta.tagName;
//			}

			// set metaIndex only if there is data, because some
			// DOM elements are defined apriori - before writing
			// any metadata - if they are still empty after the
			// initial population, then do not set metaIndex
			if (subRow.widget.hasData(subRow))
			{			
				subRow.metaIndex = ++metaIndex;
//				subRow.metaSubdex = metaSubdex++;
			}		
		}
}

// Helper function - xml a block
UIWidgets.Class.prototype.xmlBlock = function(asset,row,level,attr)
{
	var xml = '';
	var body = '';
	var lastURI='';
	var indent = this.indent(level);

	function errorTab(tabName)
	{
		var tab = document.getElementById(tabName);
		tab.setAttribute('error','true');
	}
	
	function errorRow(row)
	{
		// skip hidden rows (they might be empty, but this is not an error)
		if (row.getAttribute('hidden')) return;

		var tabName = row.getAttribute('tab');
		errorTab('tab-'+tabName);
	}
	
	// process the block
	for (var subRow=this.blockElem(row).firstChild; subRow; subRow=subRow.nextSibling)
	{
		if (!subRow.widget)
			continue;

		if (subRow.widget.hasData(subRow))
		{
			// the row has data, convert them into XML
			var str = '';

			if (!level)
			if ( lastURI!=subRow.node.uri || subRow.widget.blockElem )
			{
				if (lastURI) str += '\n';
				lastURI = subRow.node.uri;
			}

			if (subRow.widget.xml)
			{
				if (subRow.node.attribute)
				{
					attr += subRow.widget.xml(asset,subRow,level+1,'');
				}
				else
					str	+= indent+subRow.widget.xml(asset,subRow,level+1,'');
			}
			else
			{
				str += '\n\t'+indent+'<recursive for '+subRow.node.uri+'|'+subRow.widget.name+'-->';
			}
			body += str;
		}
		else
		{
			// the row has no data
			// check whether this is a problem or not

			// the node is optional, no problem, continue
			if (subRow.node.min==0)
				continue;
			//console.log(subRow.node.name,'error: (1) has no data; (2) min='+subRow.node.min);

			errorTab('tab-'); // tab "All"
			
			var lastRow = subRow;
			subRow.classList.add('incompleteData');

			asset.xmlErrors.push(subRow);
			// mark all parent rows too
			for (var parent=subRow.parentElement; parent; parent=parent.parentElement)
				if (parent.nodeLevel && parent.className=='row')
				{
					parent.classList.add('errorInChild');
					lastRow = parent;
				}
				
			errorRow(lastRow);
		}
	}

	var metaAttr = this.blockElem(row).metaAttr;
	for (var i in metaAttr)
		attr += ' '+i+'="'+metaAttr[i]+'"';

	return {xml:xml, body:body, attr:attr};
}

UIWidgets.Class.prototype.createToggleLabel = function(node,parentNode)
{
	var elem = this.createLabel(node,parentNode);
	elem.setAttribute('toggle','true');
	elem.onclick = AssetMetadata.toggleElement;
	return elem;
}

// Helper function - creates a text box + language listbox (if needed)
UIWidgets.Class.prototype.createTextBox = function(node,parentNode)
{
	var elem = document.createElement('span');

	// create the main text box
	var input = document.createElement('input');
	input.style.width = this.getWidth(node,parentNode,'10em');
	input.setAttribute('placeholder',this.getHint(node,parentNode,''));
	elem.appendChild( input );

	// add language listbox if needed
	if (node.hasLang)
		elem.appendChild( this.createLangBox() );

	if (options.permissions!='rw')
	{
		input.setAttribute('disabled','true');
	}
	
	return elem;
}

// Helper function - creates a text multiline area with specific width, height and hint
UIWidgets.Class.prototype.createTextArea = function(node,parentNode)
{
	var elem = document.createElement('span');

	// create the main text box
	var textarea = document.createElement('textarea');
	textarea.style.width = this.getWidth(node,parentNode,'10em');
	textarea.style.height = this.getHeight(node,parentNode,'3em');
	textarea.setAttribute('placeholder',this.getHint(node,parentNode,''));
	elem.appendChild( textarea );

	// add language listbox if needed
	if (node.hasLang)
		elem.appendChild( this.createLangBox() );

	if (options.permissions!='rw')
	{
		textarea.setAttribute('disabled','true');
	}
	
	return elem;
}

// Helper function - creates a list box with specific list of items
UIWidgets.Class.prototype.createListBox = function(options,namingStyle,addEmptyOption)
{
	var select = document.createElement('select');

	// add empty item in the beginning
	if (addEmptyOption)
	{
		var option = document.createElement('option');
		select.appendChild(option);
	}

	// add the actual items
	for (var i=0; i<options.length; i++)
	{
		var option = document.createElement('option');
		var item = options[i].name || options[i];
		option.setAttribute('label',namingStyle(item));
		option.setAttribute('value',options[i].uri || item);
		option.level = options[i].level || 0;
		option.origLabel = namingStyle(item)+'';
		option.innerHTML = namingStyle(item)+'';
		option.style.fontSize = Math.max(0.5,Math.pow(0.85,options[i].level||0))+'em';
		select.appendChild(option);
	}

	// use 'window.options' because there is local 'options' parameter
	if (window.options.permissions!='rw')
	{
		select.setAttribute('disabled','true');
	}
	
	select.addEventListener('change',this.listBoxOnChange);
	select.addEventListener('mousedown',this.listBoxOnMouseKey);
	select.addEventListener('keydown',this.listBoxOnMouseKey);

	return select;
}

UIWidgets.Class.prototype.listBoxOnChange = function(event)
{
	select = event.target;

	// replaces the value with a trimmed version of the value
	select.options[select.selectedIndex].label = select.options[select.selectedIndex].origLabel||'';
};

UIWidgets.Class.prototype.listBoxOnMouseKey = function(event)
{
	select = event.target;

	// indents the label of each option
	for (var i = 0; i<select.length; i++) if (select.options[i].level)
	{
		var prefix = '';
		for (j=0; j<select.options[i].level; j++)
			prefix+='\u00A0\u00A0\u00A0\u00A0';
		select.options[i].label = prefix+select.options[i].origLabel;
	}
}

// Helper function - creates a file upload box
UIWidgets.Class.prototype.createFileBox = function(id)
{
	var input = document.createElement('input');
	input.setAttribute('multiple','false');
	input.setAttribute('type','file');
	input.setAttribute('id','FB'+id);
	if (options.permissions!='rw')
	{
		input.setAttribute('disabled','true');
	}
	input.onchange = function(event)
	{
		// a new file is selected
		var filename = input.value.split('\\').pop().split('/').pop();
		document.getElementById('BT'+id).innerHTML = filename;
		document.getElementById('BT'+id).files = event.target.files;
		
		// get some of its metadata
		var file = event.target.files[0];
		var date = file.lastModifiedDate;
		//console.log(file);
		//console.log('name',file.name); // name,  http://purl.org/dc/terms/title
		//console.log('format',file.type); // http://purl.org/dc/terms/format
		//console.log('date',date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate())); // http://purl.org/dc/terms/date
		
		var frame = input.parentElement.children[5];
		for (var elem = frame.firstChild; elem; elem=elem.nextSibling)
			if (elem.classList.contains('row'))
			{
				var row = elem;
				switch(row.node.uri)
				{
					case 'name':
						row.widget.write(null,row,file.name);
						break;
					case 'http://purl.org/dc/terms/title':
						if (!row.widget.hasData(row))
							row.widget.write(null,row,'Artefact '+file.name);
						break;
					case 'http://purl.org/dc/terms/date':
						function zeroPad(x){return ('0'+x).slice(-2)}
						row.widget.write(null,row,date.getFullYear()+'-'+zeroPad(date.getMonth()+1)+'-'+zeroPad(date.getDate()));
						break;
					case 'http://purl.org/dc/terms/format':
						row.widget.write(null,row,file.type);
						break;
				}
			}
	}
	return input;
}

// Helper function - creates a file upload button
UIWidgets.Class.prototype.createUploadButton = function(id)
{
	var button = document.createElement('span');
	button.innerHTML = 'Select artefact';
	button.className = 'button upload';
	button.setAttribute('id','BT'+id);
	if (options.permissions!='rw')
	{
		button.setAttribute('disabled','true');
	}
	button.onclick = function(event)
	{
		document.getElementById('FB'+id).click();
		return false;
	}
	return button;
}

// Helper function - creates a language list box
UIWidgets.Class.prototype.createLangBox = function()
{
	var listbox = this.createListBox(['en','bg','de','es','fr','it','nl','pt','ro'], AssetMetadata.sameName,false); // 2017.01.18 no empty option
	listbox.setAttribute('lang','true');
	listbox.style.verticalAlign = 'top';
	if (options.permissions!='rw')
	{
		listbox.setAttribute('disabled','true');
	}
	return listbox;
}

UIWidgets.Class.prototype.extractText = function(meta)
{
	if (typeof meta == 'string')
		return meta
	else
		return meta.textContent||meta.getAttribute('rdf:resource');
}

UIWidgets.Class.prototype.indent = function(level)
{
	return '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'.substring(0,level);
}

UIWidgets.Class.prototype.tagName = function(asset,row)
{
	return asset.shorten(row.node.uri);
}

// Helper function - creates a block of nodes, created by other widgets
UIWidgets.Class.prototype.createBlock = function(asset,node,parentNode,originalParentNode,level)
{
	var elem = document.createElement('div');
	elem.className = 'frame';
	//elem.style.backgroundColor = AssetMetadata.rgb(0xf0,0xf8,0xf0,level);
	//elem.style.backgroundColor = this.getColor(node,parentNode,'rgba(0,0,0,0.05)');
	elem.style.borderColor = AssetMetadata.rgb(0xe8,0xe8,0xe8,level);

	if (level==0)
	{
		elem.style.margin = '0';
	}
	
	if ('acem-rypy' in options)
	{
		var info = document.createElement('div');
		info.className = 'acem-rypy-info';
		info.innerHTML = 'level: '+level;
		elem.appendChild( info );
	}

	for (var i=0; i<node.array.length; i++)
	{
		//console.log('create',node.array[i].uri,'in',node.uri);
		var subElem = UIWidgets.createNodeUI(asset,node.array[i],node,originalParentNode,level+1);
		subElem.node = node.array[i];
		subElem.nodeOriginalParentNode = originalParentNode;
		if ('acem-rypy' in options)
		{
			var info = document.createElement('div');
			info.className = 'acem-rypy-info';
			info.innerHTML = '&darr; '+subElem.node.uri+' [widget '+subElem.widget.name+']';
			elem.appendChild( info );
		}

		elem.appendChild( subElem );
		if (node.array[i].max=='unbounded')
		{
			var indented = elem.lastChild.lastChild.className!='frame';
			elem.appendChild(this.createButton(asset,node.array[i].name,indented,false,level,subElem.getAttribute('tab')));

			// add button for taxonomy concept selection
			if (subElem.widget===UIWidgets.TaxonomyConcept)
				elem.appendChild(this.createButton(asset,'',false,true,level,subElem.getAttribute('tab')));
		}
	}

	return elem;
}

UIWidgets.Class.prototype.files = function(asset,row)
{
	// do nothing, the block widgets should overwrite this method
	return [];
}



/*	========================================================
	TextLineClass

	Class. Implements the widget for creating a labeled
	single-line textbox with an optional language listbox.

	CSS Styling:
		label	- the contents of the label
		width	- the width of the text box
		hint	- the hint inside the text box

	Constructor:
		TextLineClass() - constructor

	Common Methods:
		create(asset,node,parentNode,level)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		textElem(row)
		langElem(row)
		getVal(row)
		getLang(row)
		hasLang(row)
	========================================================
*/
UIWidgets.TextLineClass = function()
{
	UIWidgets.Class.apply(this,arguments);
	this.name = 'TextLine';
}

UIWidgets.TextLineClass.prototype = Object.create(UIWidgets.Class.prototype);

UIWidgets.TextLineClass.prototype.create = function(asset,node,parentNode,level)
{
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);
	row.appendChild( this.createLabel(node,parentNode) );
	row.appendChild( this.createTextBox(node,parentNode) );
	
	if (this.getHidden(node,parentNode,''))
	{
		this.textElem(row).setAttribute('readonly','true');
		row.readonly = true;
		if ('acem-rypy' in options)
			row.setAttribute('acem-rypy-hidden','true');
		else
			row.setAttribute('hidden','true');
	}
	else if (this.getReadonly(node,parentNode,''))
	{
		this.textElem(row).setAttribute('readonly','true');
		row.readonly = true;
	}
	else
	{
		row.appendChild( this.createClearButton() );
	}

	return row;
}

UIWidgets.TextLineClass.prototype.textElem = function(row)
{
	return row.children[1].firstChild;
}

UIWidgets.TextLineClass.prototype.langElem = function(row)
{
	return row.children[1].children[1];
}

UIWidgets.TextLineClass.prototype.getVal = function(row)
{
	return this.textElem(row).value;
}

UIWidgets.TextLineClass.prototype.getLang = function(row)
{
	var elem = this.langElem(row);
	if (elem) return elem.value;
	return '';
}

UIWidgets.TextLineClass.prototype.hasData = function(row)
{
	return Boolean(this.getVal(row));
}

UIWidgets.TextLineClass.prototype.hasLang = function(row)
{
	return Boolean(this.getLang(row));
}

UIWidgets.TextLineClass.prototype.write = function(asset,row,meta)
{
	// set text value
	this.textElem(row).value = this.extractText(meta);

	// there is lang attribute
	if (row.node.hasLang && meta.getAttribute)
		this.langElem(row).value = meta.getAttribute('xml:lang');
}

UIWidgets.TextLineClass.prototype.xml = function(asset,row,level,attr)
{
	var value = this.getVal(row);
	var tagName = this.tagName(asset,row);
	var indent = this.indent(level);

	if (row.node.attribute)
	{
		xml = ' '+tagName+'="'+value+'"';
	}
	else
	{
		var attr = '';
		if (this.hasLang(row))
			attr = ' xml:lang="'+this.getLang(row)+'"';
		if (row.node.realType==asset.RDF_RESOURCE)
			attr = ' rdf:resource="'+value+'"';

		if (row.metaIndex)
			attr += ' metaIndex="'+row.metaIndex+'"'/*+' metaSubdex="'+row.metaSubdex+'"'*/;
		
		if (row.node.realType==asset.RDF_RESOURCE)
			xml = '\n'+indent+'<'+tagName+attr+'/>';
		else
			xml = '\n'+indent+'<'+tagName+attr+'>'+value+'</'+tagName+'>';
	}

	return xml;
}

UIWidgets.TextLine = new UIWidgets.TextLineClass();



/*	========================================================
	TextAreaClass

	Class. Implements the widget for creating a labeled
	multi-line textbox with an optional language listbox.

	CSS Styling:
		label	- the contents of the label
		width	- the width of the text box
		height	- the height of the text box
		hint	- the hint inside the text box

	Constructor:
		TextAreaClass() - constructor

	Common Methods:
		create(asset,node,level,parentNode)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		inputElem(row)
		langElem(row)
		getVal(row)
		getLang(row)
		hasLang(row)
	========================================================
*/
UIWidgets.TextAreaClass = function()
{
	UIWidgets.TextLineClass.apply(this,arguments);
	this.name = 'TextArea';
}

UIWidgets.TextAreaClass.prototype = Object.create(UIWidgets.TextLineClass.prototype);

UIWidgets.TextAreaClass.prototype.create = function(asset,node,parentNode,level)
{
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);
	row.appendChild( this.createLabel(node,parentNode) );
	row.appendChild( this.createTextArea(node,parentNode) );
	
	if (this.getReadonly(node,parentNode,''))
	{
		this.textElem(row).setAttribute('readonly','true');
		row.readonly = true;
	}
	else
	{
		row.appendChild( this.createClearButton() );
	}
	return row;
}

UIWidgets.TextArea = new UIWidgets.TextAreaClass();



/*	========================================================
	TextDateClass

	Class. Implements the widget for creating a labeled
	date textbox.

	CSS Styling:
		label	- the contents of the label
		width	- the width of the text box

	Constructor:
		TextDateClass() - constructor

	Common Methods:
		create(asset,node,parentNode,level)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		inputElem(row)
		getVal(row)
		hasLang(row)
	========================================================
*/
UIWidgets.TextDateClass = function()
{
	UIWidgets.TextLineClass.apply(this,arguments);
	this.name = 'TextDate';
}

UIWidgets.TextDateClass.prototype = Object.create(UIWidgets.TextLineClass.prototype);

UIWidgets.TextDateClass.prototype.create = function(asset,node,parentNode,level)
{
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);
	row.appendChild( this.createLabel(node,parentNode) );
	row.appendChild( this.createTextBox(node,parentNode) );
	this.textElem(row).setAttribute('type','date');
	row.appendChild( this.createClearButton() );
	return row;
}

UIWidgets.TextDate = new UIWidgets.TextDateClass();



/*	========================================================
	ListBoxClass

	Class. Implements the widget for creating a labeled
	listbox.

	CSS Styling:
		label	- the contents of the label

	Constructor:
		ListBoxClass() - constructor

	Common Methods:
		create(asset,node,parentNode,level)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		listElem(row)
		getVal(row)
	========================================================
*/
UIWidgets.ListBoxClass = function()
{
	UIWidgets.Class.apply(this,arguments);
	this.name = 'ListBox';
}

UIWidgets.ListBoxClass.prototype = Object.create(UIWidgets.Class.prototype);

UIWidgets.ListBoxClass.prototype.create = function(asset,node,parentNode,level)
{
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);
	row.appendChild( this.createLabel(node,parentNode) );
	row.appendChild( this.createListBox(node.array,AssetMetadata.prettyName,true) );
	row.appendChild( this.createClearButton() );
	return row;
}

UIWidgets.ListBoxClass.prototype.listElem = function(row)
{
	return row.children[1];
}

UIWidgets.ListBoxClass.prototype.getVal = function(row)
{
	return this.listElem(row).value;
}

UIWidgets.ListBoxClass.prototype.hasData = function(row)
{
	return Boolean(this.getVal(row));
}

UIWidgets.ListBoxClass.prototype.write = function(asset,row,meta)
{
	this.listElem(row).value = this.extractText(meta);
}

UIWidgets.ListBoxClass.prototype.xml = function(asset,row,level,attr)
{
	var value = this.getVal(row);
	var tagName = this.tagName(asset,row);
	var indent = this.indent(level);

	var attr = '';
	if (row.metaIndex)
		attr += ' metaIndex="'+row.metaIndex+'"'/*+' metaSubdex="'+row.metaSubdex+'"'*/;

	var xml;
	if (row.node.enumeration)
		xml = '\n'+indent+'<'+tagName+attr+'>'+value+'</'+tagName+'>';
	else
		xml = '\n'+indent+'<'+tagName+' rdf:resource="'+value+'"'+attr+'/>';

	return xml;
}

UIWidgets.ListBox = new UIWidgets.ListBoxClass();



/*	========================================================
	ListTaxonomiesClass

	Class. Implements the widget for creating a labeled
	listbox of all taxonomies.

	CSS Styling:
		label	- the contents of the label

	Constructor:
		ListTaxonomiesClass() - constructor

	Common Methods:
		create(asset,node,parentNode,level)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		textElem(row)
		getVal(row)
	========================================================
*/
UIWidgets.ListTaxonomiesClass = function()
{
	UIWidgets.ListBoxClass.apply(this,arguments);
	this.name = 'ListTaxonomies';
}

UIWidgets.ListTaxonomiesClass.prototype = Object.create(UIWidgets.ListBoxClass.prototype);

UIWidgets.ListTaxonomiesClass.prototype.create = function(asset,node,parentNode,level)
{
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);
	row.appendChild( this.createLabel(node,parentNode) );
	row.appendChild( this.createListBox(asset.taxonomyList,AssetMetadata.prettyName,true) );
	row.appendChild( this.createClearButton() );

	this.listElem(row).onchange = function(event){asset.checkTaxonomyConcepts(event)};
	if (uiStyles.skin && uiStyles.skin.width)
		this.listElem(row).style.width = uiStyles.skin.width;
		
	return row;
}

UIWidgets.ListTaxonomiesClass.prototype.write = function(asset,row,meta)
{
	var text = this.extractText(meta);
	this.listElem(row).value = text;

	// request loading of the taxonomy if not loaded yet
	if (!asset.taxonomyData[text])
	{
		asset.taxonomyData[text] = new Taxonomy();
		asset.taxonomyData[text].loadFromServer(text,function(){
			asset.showTaxonomyConceptNames(text);
		});
	}
}

UIWidgets.ListTaxonomies = new UIWidgets.ListTaxonomiesClass();




/*	========================================================
	TaxonomyConceptClass

	Class. Implements the widget for creating a read-only
	textbox containing taxonomy concept.

	CSS Styling:
		label	- the contents of the label
		width	- the width of the text box
		hint	- the hint inside the text box

	Constructor:
		TaxonomyConceptClass() - constructor

	Common Methods:
		create(asset,node,parentNode,level)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		textElem(row)
		getVal(row)
	========================================================
*/
UIWidgets.TaxonomyConceptClass = function()
{
	UIWidgets.TextLineClass.apply(this,arguments);
	this.name = 'TaxonomyConcept';
}

UIWidgets.TaxonomyConceptClass.prototype = Object.create(UIWidgets.TextLineClass.prototype);

UIWidgets.TaxonomyConceptClass.prototype.create = function(asset,node,parentNode,level)
{
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);
	row.appendChild( this.createLabel(node,parentNode) );
	row.appendChild( this.createTextBox(node,parentNode) );

	this.textElem(row).readOnly = true;
	this.textElem(row).ondblclick = function(event){asset.selectTaxonomyConcepts(event)};

	row.appendChild( this.createClearButton() );

	return row;
}

UIWidgets.TaxonomyConceptClass.prototype.getVal = function(row)
{
	return row.conceptURI || this.textElem(row).value;
}

UIWidgets.TaxonomyConceptClass.prototype.setVal = function(row,value)
{
	this.textElem(row).value = value;
}

UIWidgets.TaxonomyConceptClass.prototype.xml = function(asset,row,level,attr)
{
	var value = this.getVal(row);
	var tagName = this.tagName(asset,row);
	var indent = this.indent(level);

	var attr = '';
	if (row.metaIndex)
		attr += ' metaIndex="'+row.metaIndex+'"'/*+' metaSubdex="'+row.metaSubdex+'"'*/;
		
	var xml = '\n'+indent+'<'+tagName+' rdf:resource="'+value+'"'+attr+'/>';

	return xml;
}

UIWidgets.TaxonomyConcept = new UIWidgets.TaxonomyConceptClass();




/*	========================================================
	ListTaxonomyClass

	Class. Implements the widget for creating a labeled
	listbox of concept in a fixed taxonomy.

	CSS Styling:
		label	- the contents of the label
		skip	- 'root'=skips the root concept, 'parents'=skips all parent nodes
		uri		- the taxonomy uri

	Constructor:
		ListTaxonomyClass() - constructor

	Common Methods:
		create(asset,node,parentNode,level)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		textElem(row)
		getVal(row)
	========================================================
*/
UIWidgets.ListTaxonomyClass = function()
{
	UIWidgets.ListBoxClass.apply(this,arguments);
	this.name = 'ListTaxonomy';
}

UIWidgets.ListTaxonomyClass.prototype = Object.create(UIWidgets.ListBoxClass.prototype);

UIWidgets.ListTaxonomyClass.prototype.create = function(asset,node,parentNode,level)
{
	var skip = this.getSkip(node,parentNode,'');
	var taxonURI = this.getURI(node,parentNode,'');
	var taxon = asset.taxonomyData[taxonURI];
	if (!taxon)
	{
		console.log('taxonomy not found',taxonURI);
	}

	var taxonArray = taxon.saveToArray();

	// if skip==root remove the root and push all levels left by 1
	if (skip.toLowerCase()=='root')
	{
		taxonArray.shift();
		for (var i=0; i<taxonArray.length; i++)
			if (taxonArray[i].level)
				taxonArray[i].level--;
	}

	// if skip==parents remove all parent nodes and push the rest to level 0
	if (skip.toLowerCase()=='parents')
	{
		for (var i=0; i<taxonArray.length-1; i++)
			if (taxonArray[i].level<taxonArray[i+1].level)
				taxonArray[i].level=-1;

		for (var i=taxonArray.length-2; i>=0; i--)
			if (taxonArray[i].level<0)
				taxonArray.splice(i,1);

		for (var i=0; i<taxonArray.length; i++)
			taxonArray[i].level=0;
	}

	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);
	row.appendChild( this.createLabel(node,parentNode) );
	row.appendChild( this.createListBox(taxonArray,AssetMetadata.prettyName,true) );
	row.appendChild( this.createClearButton() );
	
	if (uiStyles.skin && uiStyles.skin.width)
		this.listElem(row).style.width = uiStyles.skin.width;
		
	return row;
}

UIWidgets.ListTaxonomy = new UIWidgets.ListTaxonomyClass();



/*	========================================================
	LabelBlockClass

	Class. Implements the widget for creating a labeled
	block of other widgets.

	CSS Styling:
		label	- the contents of the label
		autoexpand - whether the widget will be initially expanded even if it is empty

	Constructor:
		LabelBlockClass() - constructor

	Common Methods:
		create(asset,node,parentNode,level)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		blockElem(row)
	========================================================
*/
UIWidgets.LabelBlockClass = function()
{
	UIWidgets.Class.apply(this,arguments);
	this.name = 'LabelBlock';
}

UIWidgets.LabelBlockClass.prototype = Object.create(UIWidgets.Class.prototype);

UIWidgets.LabelBlockClass.prototype.create = function(asset,node,parentNode,level)
{
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);

	if (uiStyles.skin||'')
		var color = uiStyles.skin.color || this.getColor(node,parentNode,'rgba(0,0,0,0.05)');
	else
		var color = this.getColor(node,parentNode,'rgba(0,0,0,0.05)');
	
	if (level)
	{
		row.appendChild( this.createToggleLabel(node,parentNode) );
		row.lastChild.style.width = 'auto';
		row.appendChild( this.createClearButton() );
		row.appendChild( this.createBr() );
	}
	else
	{	// add two empty elements
		row.appendChild(document.createElement('div'));
		row.appendChild(document.createElement('div'));
		row.appendChild(document.createElement('div'));
	}

	row.autoExpand = this.getAutoExpand(node,parentNode,'false').toLowerCase()=='true';

	// create the block (recursively)
	var originalParentNode = node;
	if (asset.nodes[node.fullType]) node = asset.nodes[node.fullType];
	row.appendChild( this.createBlock(asset,node,parentNode,originalParentNode,level) );
	row.lastChild.style.backgroundColor = color;
	
	return row;
}

UIWidgets.LabelBlockClass.prototype.blockElem = function(row)
{
	return row.children[3];
}

UIWidgets.LabelBlockClass.prototype.hasData = function(row)
{
	var block = this.blockElem(row);
	for (var subRow=block.firstChild; subRow; subRow=subRow.nextSibling)
		if (subRow.widget && subRow.widget.hasData(subRow))
			return true;

	return false;
}

UIWidgets.LabelBlockClass.prototype.write = function(asset,row,meta)
{
	//console.log('writing to widget LabelBlock',row.node.uri);
	//console.log(row);

	var node = row.node.realNode || row.node;
	var block = this.blockElem(row);

	this.writeBlock(block,node,meta);

	//console.log('done');
}

UIWidgets.LabelBlockClass.prototype.files = function(asset,row)
{
	var files = [];
	// process the block
	for (var subRow=this.blockElem(row).firstChild; subRow; subRow=subRow.nextSibling)
	{
		if (subRow.widget)
		{
			files = files.concat(subRow.widget.files(asset,subRow));
		}
	}
	
	return files;
}

UIWidgets.LabelBlockClass.prototype.xml = function(asset,row,level,attr)
{
	// empty blocks (except the root) generate nothing
	if (level && !row.widget.hasData(row))
		return '{empty}';

	var tagName = this.tagName(asset,row);
	var indent = this.indent(level);

	var x = this.xmlBlock(asset,row,level,attr);

	if (row.metaIndex)
		x.attr += ' metaIndex="'+row.metaIndex+'"'/*+' metaSubdex="'+row.metaSubdex+'"'*/;
		
	//generate the xml
	x.xml += '\n'+indent+'<'+tagName+(x.attr?x.attr:'')+'>';
	x.xml += x.body;
	x.xml += '\n'+indent+'</'+tagName+'>';
	return x.xml;
}

UIWidgets.LabelBlock = new UIWidgets.LabelBlockClass();




/*	========================================================
	ListBlockClass

	Class. Implements the widget for creating a labeled
	listbox followed by a block of other widgets.

	CSS Styling:
		label	- the contents of the label
		autoexpand - whether the widget will be initially expanded even if it is empty

	Constructor:
		ListBlockClass() - constructor

	Common Methods:
		create(asset,node,parentNode,level)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		textElem(row)
		getVal(row)
		blockElem(row)
	========================================================
*/
UIWidgets.ListBlockClass = function()
{
	UIWidgets.Class.apply(this,arguments);
	this.name = 'ListBlock';
}

UIWidgets.ListBlockClass.prototype = Object.create(UIWidgets.Class.prototype);

UIWidgets.ListBlockClass.prototype.create = function(asset,node,parentNode,level)
{
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);

	row.autoExpand = this.getAutoExpand(node,parentNode,'false').toLowerCase()=='true';
	if (uiStyles.skin||'')
		var color = uiStyles.skin.color || this.getColor(node,parentNode,'rgba(0,0,0,0.05)');
	else
		var color = this.getColor(node,parentNode,'rgba(0,0,0,0.05)');
	
	row.appendChild( this.createToggleLabel(node,parentNode) );
	var originalParentNode = node;
	if (asset.nodes[node.fullType]) node = asset.nodes[node.fullType];
	row.appendChild( this.createListBox(node.array,AssetMetadata.prettyName,true) );


	// create the block (recursively)
	var firstChoice = asset.nodes[node.array[0].fullType];
	row.appendChild( this.createClearButton() );
	row.appendChild( this.createBr() );
	row.appendChild( this.createBlock(asset,firstChoice,node,originalParentNode,level) );
	row.lastChild.style.backgroundColor = color;

	return row;
}

UIWidgets.ListBlockClass.prototype.blockElem = function(row)
{
	return row.children[4];
}

UIWidgets.ListBlockClass.prototype.textElem = function(row)
{
	return row.children[1];
}

UIWidgets.ListBlockClass.prototype.hasData = function(row)
{
	if (this.textElem(row).value)
		return true;

	var block = this.blockElem(row);
	for (var subRow=block.firstChild; subRow; subRow=subRow.nextSibling)
		if (subRow.widget && subRow.widget.hasData(subRow))
			return true;

	return false;
}


UIWidgets.ListBlockClass.prototype.write = function(asset,row,meta)
{
	//console.log('writing to widget LabelBlock',html.node.uri);
	//console.log(html);
	
	// set the listbox data
	var data = meta.firstChild.nextSibling;
	var choice = asset.unpack(data.tagName);
	this.textElem(row).value = choice;

	// set the block data
	var node = (row.node.realNode || row.node).array[0].realNode;
	meta = data;
	var block = this.blockElem(row);
	
	this.writeBlock(block,node,meta);

	// artificial increment, its value is not stored
	// anywhere, but is used in the xml method (see code 2133)
	++metaIndex;	
	
	//console.log('done');
}

UIWidgets.ListBlockClass.prototype.files = function(asset,row)
{
	var files = [];
	
	// process the block
	for (var subRow=this.blockElem(row).firstChild; subRow; subRow=subRow.nextSibling)
	{
		if (subRow.widget)
		{
			files = files.concat(subRow.widget.files(asset,subRow));
		}
	}
	
	return files;
}

UIWidgets.ListBlockClass.prototype.xml = function(asset,row,level,attr)
{
	// empty blocks (except the root) generate nothing
	if (level && !row.widget.hasData(row))
		return '{empty}';

	var indent = this.indent(level);
	var tagName1 = this.tagName(asset,row);
	var tagName2 = asset.shorten(this.textElem(row).value);
	//console.log('generating html for',row,'tag',tagName);
	level++;

	var x = this.xmlBlock(asset,row,level,attr);

	var attr1='';
	if (row.metaIndex)
		attr1 += ' metaIndex="'+row.metaIndex+'"'/*+' metaSubdex="'+row.metaSubdex+'"'*/;
		
	var attr2=x.attr;
	if (row.metaIndex)
		attr2 += ' metaIndex="'+(row.metaIndex-1)+'"'/*+' metaSubdex="0"'*/;
		
	//generate the xml
	x.xml += '\n'+indent+'<'+tagName1+attr1+'>';
	x.xml += '\n'+indent+'\t<'+tagName2+attr2+'>';
	x.xml += x.body;
	x.xml += '\n'+indent+'\t</'+tagName2+'>';
	x.xml += '\n'+indent+'</'+tagName1+'>';
	return x.xml;
}

UIWidgets.ListBlock = new UIWidgets.ListBlockClass();



/*	========================================================
	FileBlockClass

	Class. Implements the widget for creating a block of
	other widgets + possibility to upload a file.

	CSS Styling:
		label	- the contents of the label
		autoexpand - whether the widget will be initially expanded even if it is empty

	Constructor:
		FileBlockClass() - constructor

	Common Methods:
		create(asset,node,parentNode,level)
		write(asset,row,meta)
		xml(asset,row,level,attr)
		hasData(row)

	Specific methods:
		blockElem(row)
	========================================================
*/
UIWidgets.FileBlockClass = function()
{
	UIWidgets.Class.apply(this,arguments);
	this.name = 'FileBlock';
}

UIWidgets.FileBlockClass.prototype = Object.create(UIWidgets.Class.prototype);

UIWidgets.FileBlockClass.prototype.create = function(asset,node,parentNode,level)
{
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('id',++AssetMetadata.id);

	row.autoExpand = this.getAutoExpand(node,parentNode,'false').toLowerCase()=='true';
	if (uiStyles.skin||'')
		var color = uiStyles.skin.color || this.getColor(node,parentNode,'rgba(0,0,0,0.05)');
	else
		var color = this.getColor(node,parentNode,'rgba(0,0,0,0.05)');
	
	row.appendChild( this.createToggleLabel(node,parentNode) );
	row.lastChild.style.width = 'auto';
	row.appendChild( this.createUploadButton(AssetMetadata.id) );
	row.appendChild( this.createFileBox(AssetMetadata.id) );
	row.appendChild( this.createClearButton() );
	row.appendChild( this.createBr() );


	// create the block (recursively)
	var originalParentNode = node;
	if (asset.nodes[node.fullType]) node = asset.nodes[node.fullType];
	row.appendChild( this.createBlock(asset,node,parentNode,originalParentNode,level) );
	row.lastChild.style.backgroundColor = color;
	
	return row;
}

UIWidgets.FileBlockClass.prototype.blockElem = function(row)
{
	return row.children[5];
}

UIWidgets.FileBlockClass.prototype.fileElem = function(row)
{
	return row.children[2];
}

UIWidgets.FileBlockClass.prototype.hasData = function(row)
{
	var block = this.blockElem(row);
	for (var subRow=block.firstChild; subRow; subRow=subRow.nextSibling)
		if (subRow.widget && subRow.widget.hasData(subRow))
			return true;

	return false;
}

UIWidgets.FileBlockClass.prototype.write = function(asset,row,meta)
{
	//console.log('writing to widget FileBlock',row.node.uri);
	//console.log(row);

	var node = row.node.realNode || row.node;
	var block = this.blockElem(row);

	this.writeBlock(block,node,meta);

	//console.log('done');
}

UIWidgets.FileBlockClass.prototype.files = function(asset,row)
{
	// this block may have a file, append it to the files[] array
	var files = [];
	if (this.fileElem(row).files.length)
	{
		this.fileElem(row).files.assetSection = row.nodeOriginalParentNode.name.toLowerCase();
		files = [this.fileElem(row).files];
	}
	
	// in any case, process the block for some nested files
	for (var subRow=this.blockElem(row).firstChild; subRow; subRow=subRow.nextSibling)
	{
		if (subRow.widget)
		{
			files = files.concat(subRow.widget.files(asset,subRow));
		}
	}
	
	return files;
}

UIWidgets.FileBlockClass.prototype.xml = function(asset,row,level,attr)
{
	// empty blocks (except the root) generate nothing
	if (level && !row.widget.hasData(row))
		return '{empty}';

	var tagName = this.tagName(asset,row);
	var indent = this.indent(level);

	var x = this.xmlBlock(asset,row,level,attr);

	if (row.metaIndex)
		x.attr += ' metaIndex="'+row.metaIndex+'"'/*+' metaSubdex="'+row.metaSubdex+'"'*/;
		
	//generate the xml
	x.xml += '\n'+indent+'<'+tagName+(x.attr?x.attr:'')+'>';
	x.xml += x.body;
	x.xml += '\n'+indent+'</'+tagName+'>';
	return x.xml;
}

UIWidgets.FileBlock = new UIWidgets.FileBlockClass();




