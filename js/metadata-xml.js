/*

  *	metadata-xml.js
	Back-end functionality of XML and XSD processing used by the RAGE Metadata Editor.

  * Copyright 2015-2016 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.

  * Definitions:

	new XMLFile(asset,url,isMetadata)
	
	static
		isNamespace(str)
		getNamespace(str)
		rememberNode(where,node)
	prototype properties
		url		- initial URL/URI
		source	- downloaded source of the XML/XSD file
		dom		- XML/XSD parsed into DOM
		prefix	- mapping of namepaces {short:full}
	prototype methods
		dumpNodes()
		simplifyNodes()
		metadataLoaded()
		definitionLoaded()
		analyzeDOM()
		analyzeXSD()
		analyzeElementXSD(elem)
		analyzeRefXSD(elem)
		analyzeChoiceXSD(elem)
		analyzeComplexTypeXSD(elem)
		analyzeEnumerationXSD(elem)
		unpack(type)
			
	--------------------------------------------------------
	
	The entities of schema definitions are extracted into an
	object with the following structure.
	
	Simple type:
		{simple:true, uri, name, min, max, type, fullType}
	Reference type:
		{reference:true, uri, name, min, max}
	Choice type:
		{choice:true, uri, name, min, max, array}
	Extension type:
		{extension:true, min, max, type, fullType}
	Attribute type:
		{attribute:true, name, min, max, type, fullType}
	Complex type:
		{complex:true, uri, name, min, max, array}
	Enumeration type:
		{enumeration:true, uri, name, min, max, array}
*/


if (typeof LOG_XML === 'undefined')
{
	LOG_XML = false;
	LOG_XML_DUMP_NODES = false;
	LOG_HTTP_REQUEST = false;
}

XMLNS = 'xmlns';
XML_SCHEMA = 'http://www.w3.org/2001/XMLSchema';
XML_SCHEMA_ELEMENT		= XML_SCHEMA+'#element';
XML_SCHEMA_COMPLEX_TYPE	= XML_SCHEMA+'#complexType';
XML_SCHEMA_IMPORT		= XML_SCHEMA+'#import';
XML_SCHEMA_ATTRIBUTE	= XML_SCHEMA+'#attribute';
XML_SCHEMA_ENUMERATION	= XML_SCHEMA+'#enumeration';
XML_SCHEMA_CHOICE		= XML_SCHEMA+'#choice';
XML_SCHEMA_EXTENSION	= XML_SCHEMA+'#extension';
XML_SCHEMA_DATE			= XML_SCHEMA+'#date';
XML_SCHEMA_STRING		= XML_SCHEMA+'#string';

DCAT_THEME_TAXONOMY		= 'http://www.w3.org/ns/dcat#themeTaxonomy';
DCAT_THEME				= 'http://www.w3.org/ns/dcat#theme';
DCTERMS_IDENTIFIER		= 'http://purl.org/dc/terms/identifier';
DCTERMS_TITLE			= 'http://purl.org/dc/terms/title';

//xmlCache = {};

/*	========================================================
	XMLFile

	Class. Implements the backbone management of an XML file
	with either asset metadata (XML file) or asset metadata
	schema definition (XSD file).

	new XMLFile(asset,url,isMetadata)

	Constructor. Initiates asyncronous loading of an XML or
	XSD file. Executes specific processing depending on
	isMetadata. If isMetadata==true, process the XML as
	metadata, otherwise process as definition schema.
	========================================================
*/
XMLFile = function(asset,url,isMetadata)
{
//console.log('XMLFile.enter');
	// add path xml/ or xsd/ if the url has no slashes
	if (url.indexOf('/')<0)
		url = (isMetadata?'xml/':'xsd/')+url;

	this.url = url;
	this.prefix = {};
	this.asset = asset;

	var that = this;
	var done = isMetadata
				?function(){that.metadataLoaded();}
				:function(){that.definitionLoaded();}
//console.log('XMLFile.isMetadata =',isMetadata);
	
	asset.pendingRequests++;

	// it is cached? - suddenly stopped working - comment it out
	/*
	if (xmlCache[url])
	{
		if (LOG_HTTP_REQUEST) console.log('\tXML Cache hit',url);
		console.log('\tXML Cache hit',url);
		that.source = xmlCache[url];
		that.dom = (new window.DOMParser()).parseFromString(that.source, "text/xml").documentElement;
		that.analyzeDOM();
		that.status = '';
		that.statusText = '';
		if (done) done();
		return;
	}
	*/
	
	// it is not cached, so request it
	if (LOG_XML) console.log('\tXML Request',url,' ( pending',asset.pendingRequests,')');
	if (LOG_HTTP_REQUEST) console.log('HTTP Request',url,' ('+asset.pendingRequests+')');

	function success(source)
	{
//console.log('XMLFile.success()');
		var bar = document.getElementById('bar');
		if (bar) bar.innerHTML += '&#9899;';
		
		if (LOG_XML) console.log('\tXML Request OK',that.url);
		that.source = source;
		that.dom = (new window.DOMParser()).parseFromString(that.source, "text/xml").documentElement;
		that.analyzeDOM();
		that.status = this.status;
		that.statusText = this.statusText;
		//xmlCache[that.url] = that.source;
		if (done) done();
	}
	
	// if there is "<" in the URL, most likely it is not
	// URL but the actual content with XML tags, so we are done
	if( url.indexOf('<')>-1 )
	{
		success(url);
		return;
	}
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url);
	xhr.timeout = 5000; //5s
	xhr.onload = function()
	{
		if (xhr.status === 200)
		{
			success(xhr.responseText);
		}
		else
			xhr.onerror();
	};
	xhr.onerror = function()
	{
		
		that.status = this.status;
		that.statusText = this.statusText;
		if (LOG_XML) console.log('\tXML Request Error',this.status,this.statusText);

		var msg = '<div>Cannot retrieve file <b>'+url+'</b>.</div>';
		if (that.asset.errorBox.innerHTML.indexOf(msg)<0)
		{
			that.asset.errorBox.innerHTML += msg;
			that.asset.errorBox.style.display = 'inline-block';
		}
		
		if (done) done();
	};
	xhr.ontimeout = xhr.onerror;
	
	if (!EXTERN)
	{
		xhr.send();
	}
	else
	{
		if (isMetadata)
		{
			console.log('EXTERN: Loading XML from EXTERN.XML ('+EXTERN.XML.length+' bytes)');
			setTimeout(function(){success(EXTERN.XML);},10);
		}
		else
		{
			console.log('EXTERN: Loading XSD from EXTERN.XSD ('+EXTERN.XSD.length+' bytes)');
			setTimeout(function(){success(EXTERN.XSD);},10);
		}
	}
}


XMLFile.XMLNS = 'xmlns:';


/*	========================================================
	XMLFile.isNamespace(str)

	Function. Checks whether an attribute name defines an
	alias of a namespace, i.e. the name is 'xmlns:abc'.
	
		str		String. Attribute name.
		
	Outputs true or false.
	========================================================
*/
XMLFile.isNamespace = function(str)
{
	return (str==XMLNS || str.indexOf(XMLNS+':')==0);
}


/*	========================================================
	XMLFile.getNamespace(str)

	Function. Returns the alias of a namespace. I.e. from
	'xmlns:abc' returns 'abc'.
	
		str		String. Attribute name of namespace alias.
		
	Outputs the alias.
	========================================================
*/
XMLFile.getNamespace = function(str)
{
	str = str.substring(XMLNS.length);
	if (str[0]==':') str = str.substring(1);
	return str;
}


/*	========================================================
	XMLFile.prototype.analyzeDOM()

	Command. Extracts the namespace dictionary from the DOM
	of an XML/XSD file. Fills in the prefix property.
	========================================================
*/
XMLFile.prototype.analyzeDOM = function()
{
	var attrs = this.dom.attributes;
	for (var i=0; i<attrs.length; i++)
	{
		var name = attrs[i].name;
		var value = attrs[i].value;
		if (XMLFile.isNamespace(name))
			this.prefix[XMLFile.getNamespace(name)] = value;
	}
}


/*	========================================================
	XMLFile.prototype.metadataLoaded()

	Command. Executed when the metadata file is loaded and
	processed. It extracts the definition schema of the
	metadata and loads it asynchronously.
	========================================================
*/
XMLFile.prototype.metadataLoaded = function()
{
//console.log('XMLFile.metadataLoaded()');
	if (LOG_XML) console.log('2. Metadata',this.url,'is loaded');
	this.asset.pendingRequests--;
	if (LOG_HTTP_REQUEST) console.log('HTTP    Done',this.url,' ('+this.asset.pendingRequests+')');

	// get the definition file of the asset
	this.xsi = this.dom.getAttribute('xsi:schemaLocation');
	var defFile = this.xsi.split(' ')[1];
	if (defFile.substring(0,5)=='file:')
		defFile = defFile.substring(5);
	this.def = new XMLFile(this.asset,defFile,false);
}


/*	========================================================
	XMLFile.prototype.definitionLoaded()

	Command. Executed when a definition schema file is
	loaded. Recursively request the loading of imported
	definition schema files.
	========================================================
*/
XMLFile.prototype.definitionLoaded = function()
{
//console.log('XMLFile.definitionLoaded()');
	this.asset.pendingRequests--;
	if (LOG_XML) console.log('3. Definition file',this.url,'is loaded ( pending',this.asset.pendingRequests,')');
	if (LOG_HTTP_REQUEST) console.log('HTTP    Done',this.url,' ('+this.asset.pendingRequests+')');

	if (this.dom)
	{
		this.targetNamespace = this.dom.getAttribute('targetNamespace');

		if (LOG_XML) console.log('\t5.1. Namespace: ',this.targetNamespace);
		
		// do not process further RDF schema
		if (this.targetNamespace.indexOf('rdf-syntax')<0)
		{
			var imp = this.dom.getElementsByTagName('*');
			for (var i=0; i<imp.length; i++) if (this.unpack(imp[i].tagName)==XML_SCHEMA_IMPORT)
			{
				var fileName = imp[i].getAttribute('schemaLocation');
				new XMLFile(this.asset,fileName,false);
			}
			
			this.analyzeXSD();
		}
		else
		{
			this.asset.RDF = this.targetNamespace;
		}
	}
	
	// there are pending definition schemas
	if (LOG_HTTP_REQUEST) console.log('There are pending',this.asset.pendingRequests,'reuqests')
	if (this.asset.pendingRequests)
		return;
	if (LOG_HTTP_REQUEST) console.log('Great! You are lucky! No more pending reuqests!')
		
	if (LOG_XML)
	{
		console.log('4. Loading definition schema files is completed');
		console.log('----------------');
	}
	this.simplifyNodes();

	if (LOG_XML || LOG_XML_DUMP_NODES)
	{
		this.dumpNodes();
	}
	
	var bar = document.getElementById('bar');
	if (bar) bar.style.display = 'none';

	this.asset.createUI();
	
	// if the metadata is not about asset or artefact,
	// or if there is no required info (assetId for artefact)
	// then hide the SAVE button
	var hide = true;
	if (EXTERN) hide = false;
	if (options.permissions=='rw') hide = false;
	if (this.asset.rootNode.name=='Asset' && options.assetId) hide = false;
	if (this.asset.rootNode.name=='Artefact' && options.assetId && options.artefactId) hide = false;
	if (hide) document.getElementById('saveButton').style.display = 'none';
	document.getElementById('mainButtons').style.display = 'inline';
}


/*	========================================================
	XMLFile.rememberNode(where,node)

	Command. Stores a node of the schema structure. The key
	of the node is its full URI.
	========================================================
*/
XMLFile.rememberNode = function(where,node)
{
	where[node.uri] = node;
}


/*	========================================================
	XMLFile.prototype.unpack(type)

	Function. Returns the full type name/uri.
	========================================================
*/
XMLFile.prototype.unpack = function(type)
{
	var id = type.split(':');
	
	// no prefix, eg: <element>
	if (id.length==1 && this.prefix[''])
	{
		var prefix = this.prefix[''];
		var ch = prefix.slice(-1);
		if (ch!='/' && ch!='#') prefix += '#';
		//console.log(type,'->',prefix+type);
		return prefix+type;
	}
	
	// there is prefix, eg: <xs:element>
	if (this.prefix[id[0]])
	{
		var prefix = this.prefix[id[0]];
		var ch = prefix.slice(-1);
		if (ch!='/' && ch!='#') prefix += '#';
		//console.log(type,'->',prefix+id[1]);
		return prefix+id[1];
	}
	
	return type;
}


/*	========================================================
	XMLFile.prototype.analyzeXSD()

	Command. Analyzes XSD schema and extracts all useful
	data from it.
	========================================================
*/
XMLFile.prototype.analyzeXSD = function()
{
	if (LOG_XML) console.log('5. Analyze schema',this.url);

	// get elements - simple types and choices
	for (var elem = this.dom.firstChild; elem; elem = elem.nextSibling) if (elem.tagName)
	{
		var tag = this.unpack(elem.tagName);
		if (tag==XML_SCHEMA_ELEMENT)
			XMLFile.rememberNode( this.asset.nodes, this.analyzeElementXSD(elem) );
		else
		if (tag==XML_SCHEMA_COMPLEX_TYPE)
			XMLFile.rememberNode( this.asset.nodes, this.analyzeComplexTypeXSD(elem) );
	}
}


/*	========================================================
	XMLFile.prototype.analyzeElementXSD(elem)

	Function. Analyzes <element> node in XSD schema and
	extracts all useful data from it.
	
		elem	- the DOM node of XSD <element>
		
	Returns schema structure.
	========================================================
*/
XMLFile.prototype.analyzeElementXSD = function(elem)
{
	// if there is ref attribute, assume the element is
	// a reference element
	if (elem.getAttribute('ref'))
		return this.analyzeRefXSD(elem);
		
	// if there is <enumeration> subtag, assume the element
	// is an enumaration type
	var enums = false;
	var e = elem.getElementsByTagName('*');
	for (var i=0; i<e.length; i++) if (this.unpack(e[i].tagName)==XML_SCHEMA_ENUMERATION) {enums=true; break;}
	if (enums)
		return this.analyzeEnumerationXSD(elem);
		
	// if there is no type attribute, assume the element is
	// a choice element
	var type = elem.getAttribute('type');
	if (!type)
		return this.analyzeChoiceXSD(elem);
	var name = elem.getAttribute('name');
	if (LOG_XML) console.log('\t5.2. Element',name, type);
	
	return {
		simple:		true,
		uri:		this.targetNamespace + name,
		name:		name,
		min:		elem.getAttribute('minOccurs') || '1',
		max:		elem.getAttribute('maxOccurs') || '1',
		type:		type,
		fullType:	this.unpack(type),
		realType:	undefined,
		hasLang:	undefined,
		annotation:	this.getAnnotation(elem)
	}
}


/*	========================================================
	XMLFile.prototype.getAnnotation(elem)

	Function. Gets the annotation description of an element.
	It is extracted from:
		<elem>
			<annotation>
				<documentation>
					?????
				</documentation>
			</annotation>
		</elem>
	
		elem	- the DOM node of XSD <element>
		
	Returns the anotation as string (empty string if there
	is no annotation).
	========================================================
*/
XMLFile.prototype.getAnnotation = function(elem)
{
	var anno = elem.getElementsByTagName('annotation');
	if (anno.length)
	{	// use the first annotation
		var doco = anno[0].getElementsByTagName('documentation');
		if (doco.length)
		{	// use the first documentation
			var text = doco[0].innerHTML||doco[0].textContent;
			return text.trim();
		}
	}

//	console.log('empty annotation',elem);
	return '';
}


/*	========================================================
	XMLFile.prototype.analyzeRefXSD(elem)

	Function. Analyzes <element> node in XSD schema and
	extracts all useful data from it.
	
		elem	- the DOM node of XSD <element>
		
	Returns schema structure.
	========================================================
*/
XMLFile.prototype.analyzeRefXSD = function(elem)
{
	// if there is no type and no name attributes
	var name = elem.getAttribute('ref');
	if (LOG_XML) console.log('\t5.2. Reference',name, this.unpack(name));

	return {
		reference:	true,
		uri:		this.unpack(name),
		name:		name,
		min:		elem.getAttribute('minOccurs') || '1',
		max:		elem.getAttribute('maxOccurs') || '1',
		realType:	undefined,
		hasLang:	undefined,
		annotation:	this.getAnnotation(elem)
	}
}


/*	========================================================
	XMLFile.prototype.analyzeChoiceXSD(elem)

	Function. Analyzes <choice> node in XSD schema and
	extracts all useful data from it.
	
		elem	- the DOM node containing somewhere a XSD
				  <choice> element
				  
	Returns schema structure.
	========================================================
*/
XMLFile.prototype.analyzeChoiceXSD = function(elem)
{
	var name = elem.getAttribute('name');
	if (LOG_XML) console.log('\t5.3. Choice',name);

	// directly search for <element> assuming the each found
	// <element> will be inside a <choice> element
	var choices = [];
	var e = elem.getElementsByTagName('*');
	for (var i=0; i<e.length; i++) if (this.unpack(e[i].tagName)==XML_SCHEMA_ELEMENT)
	{
		choices.push( this.analyzeRefXSD(e[i]) );
		if (LOG_XML)
		{
			var j = choices.length-1;
			console.log('\t\t- ',choices[j].name,choices[j].uri);
		}
	}
		
	return {
		choice:		true,
		uri:		this.targetNamespace + name,
		name:		name,
		min:		elem.getAttribute('minOccurs') || '1',
		max:		elem.getAttribute('maxOccurs') || '1',
		array:		choices,
		realType:	undefined,
		hasLang:	undefined,
		annotation:	this.getAnnotation(elem)
	}
}


/*	========================================================
	XMLFile.prototype.analyzeComplexTypeXSD(elem)

	Function. Analyzes <complexType> node in XSD schema and
	extracts all useful data from it.
	
		elem	- the DOM node of XSD <complexType>
		
	Returns schema structure.
	========================================================
*/
XMLFile.prototype.analyzeComplexTypeXSD = function(elem)
{
	// if complex type -- treat it like complex type
	var choice = false;
	var elems = elem.getElementsByTagName('*');
	for (var i=0; i<elems.length; i++) if (this.unpack(elems[i].tagName)==XML_SCHEMA_CHOICE) {choice=true; break;}
	if (choice)
		return this.analyzeChoiceXSD(elem);
	
	var name = elem.getAttribute('name');
	if (LOG_XML) console.log('\t5.4. Complex type',name,this.targetNamespace + name);

	var array = [];

	// process extensions
	var exts = elem.getElementsByTagName('*');
	for (var i=0; i<exts.length; i++) if (this.unpack(exts[i].tagName)==XML_SCHEMA_EXTENSION)
	{
		var base = exts[i].getAttribute('base');
		if (LOG_XML) console.log('\t\t- extension',base,this.unpack(base));
		array.push(
			{
				extension:	true,
				min:		'1',
				max:		'1',
				type:		base,
				fullType:	this.unpack(base),
				realType:	undefined,
				hasLang:	undefined,
		});
		break;
	}

	// process attributes
	var attrs = elem.getElementsByTagName('*');
	for (var i=0; i<attrs.length; i++) if (this.unpack(attrs[i].tagName)==XML_SCHEMA_ATTRIBUTE)
	{
		var aName = attrs[i].getAttribute('name');
		var aType = attrs[i].getAttribute('type');
		var aUse = attrs[i].getAttribute('use');
		if (LOG_XML) console.log('\t\t- ',aName,aType);
		array.push(
			{
				attribute:	true,
				uri:		aName,
				name:		aName,
				min:		(aUse=='required')?'1':'0',
				max:		'1',
				type:		aType,
				fullType:	this.unpack(aType),
				realType:	undefined,
				hasLang:	undefined,
				annotation:	this.getAnnotation(attrs[i])
		});
	}
	
	// process elements
	var elems = elem.getElementsByTagName('*');
	for (var i=0; i<elems.length; i++) if (this.unpack(elems[i].tagName)==XML_SCHEMA_ELEMENT)
	{
		array.push( this.analyzeElementXSD(elems[i]) );
	}
	
	return {
		complex:	true,
		uri:		this.targetNamespace + name,
		name:		name,
		min:		'1',
		max:		'1',
		array:		array,
		realType:	undefined,
		hasLang:	undefined,
		annotation:	this.getAnnotation(elem)
	}
}


/*	========================================================
	XMLFile.prototype.analyzeEnumerationXSD(elem)

	Function. Analyzes <enumeration> node in XSD schema and
	extracts all useful data from it.
	
		elem	- the DOM node containing somewhere a XSD
				  <enumeration> element
				  
	Returns schema structure.
	========================================================
*/
XMLFile.prototype.analyzeEnumerationXSD = function(elem)
{
	var name = elem.getAttribute('name');
	if (LOG_XML) console.log('\t5.3. Enumeration',name);

	// directly search for <enumeration> assuming the each found
	// <enumeration> will be inside a <restriction> element
	var enums = [];
	var e = elem.getElementsByTagName('*');
	for (var i=0; i<e.length; i++) if (this.unpack(e[i].tagName)==XML_SCHEMA_ENUMERATION)
	{
		enums.push( e[i].getAttribute('value') );
		if (LOG_XML)
		{
			var j = enums.length-1;
			console.log('\t\t- ',enums[j].value);
		}
	}
		
	return {
		enumeration:true,
		uri:		this.targetNamespace + name,
		name:		name,
		min:		elem.getAttribute('minOccurs') || '1',
		max:		elem.getAttribute('maxOccurs') || '1',
		array:		enums,
		realType:	undefined,
		hasLang:	undefined,
		annotation:	this.getAnnotation(elem)
	}
}

/*	========================================================
	XMLFile.prototype.dumpNodes()

	Command. Dumps all nodes to the console.
	========================================================
*/
XMLFile.prototype.dumpNodes = function()
{
	function dumpNode(node,level)
	{
		var str = '\t\t\t\t\t'.substring(0,level);

		if (node.reference)
		{
			console.log( str+'REFERENCE '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri );
		}
			
		if (node.extension)
		{
			console.log( str+'EXTENSION '+node.type+' ['+node.min+'..'+node.max+'] '+node.fullType );
		}
		
		if (node.simple)
		{
			console.log( str+'ELEMENT '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri+' ['+node.type+']' );
		}
		
		if (node.choice)
		{
			console.log( str+'CHOICE '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri );
			for (var i=0; i<node.array.length; i++)
				dumpNode(node.array[i],level+1);
		}
		
		if (node.attribute)
		{
			console.log( str+'ATTRIBUTE '+node.name+' ['+node.min+'..'+node.max+'] '+node.fullType );
		}
		
		if (node.complex)
		{
			console.log( str+'COMPLEX '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri );
			for (var i=0; i<node.array.length; i++)
				dumpNode(node.array[i],level+1);
		}
		
		if (node.enumeration)
		{
			console.log( str+'ENUMERATION '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri );
			for (var i=0; i<node.array.length; i++)
				console.log( str+'\t '+node.array[i] );
		}
	}

	for (var i in this.asset.nodes)
	{
		dumpNode(this.asset.nodes[i],0);
	}
	
	console.log( 'RDF', this.asset.RDF );
}


/*	========================================================
	XMLFile.prototype.simplifyNodes()

	Command. Simplifies the nodes by:
		- converting reference nodes to their true type
		- unpacking extension nodes
	========================================================
*/
XMLFile.prototype.simplifyNodes = function()
{
	if (LOG_XML) console.log('5. Simplification of nodes');
	
	function simplifyNode(parent,array,index)
	{
		var node = array[index];
		
		// replace reference node with the actual node, but
		// keep the min/max values from the reference
		if (node.reference)
		{
			//if (node.uri=='http://purl.org/dc/terms/description')
			//	console.log('dereferencing',index,node.uri,'in parent',parent.uri);
			var newNode = JSON.parse(JSON.stringify(this.asset.nodes[node.uri])); // deep cloning
			newNode.annotation = node.annotation||newNode.annotation;
			newNode.min = node.min;
			newNode.max = node.max;
			array[index] = newNode;
			node = newNode;
		}

		// replace an extension node with the actual nodes
		if (node.extension)
		{
			array.splice(index,1);
			var extNode = this.asset.nodes[node.fullType];
			for (var i=0; i<extNode.array.length; i++)
			{
				simplifyNode(node,extNode.array,i);
				array.splice(index+i,0,extNode.array[i]);
			}
			return;
		}

		// recursive simplification of complex and choice nodes
		if (node.complex || node.choice)
			for (var i=node.array.length-1; i>=0; i--)
				simplifyNode(node,node.array,i);
	}

	for (var i in this.asset.nodes)
		simplifyNode(null,this.asset.nodes,i);
		
	this.asset.RDF_LITERAL = this.asset.RDF+'LiteralType'; 
	this.asset.RDF_RESOURCE = this.asset.RDF+'ResourceType'; 
	this.asset.RDF_RESOURCE_OR_LITERAL = this.asset.RDF+'ResourceOrLiteralType'; 

	function traverseRealType(node,level)
	{
		node.realType = this.asset.realType(node.fullType);
		if (node.realType)
		{
			node.realNode = this.asset.nodes[node.realType];
			if (node.realNode)
				node.annotation = node.annotation||node.realNode.annotation;
		}
		node.hasLang = (node.realType==this.asset.RDF_LITERAL) || (node.realType==this.asset.RDF_RESOURCE_OR_LITERAL);
		if (node.array)
		{
			for (var i=0; i<node.array.length; i++)
				traverseRealType(node.array[i],level+1);
		}
	}
	for (var i in this.asset.nodes)
		traverseRealType(this.asset.nodes[i],0);
}
