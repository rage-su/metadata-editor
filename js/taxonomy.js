/*

  *	taxonomy.js
	Back-end functionality of taxonomies used by the RAGE Taxonomy Tools.

  * Copyright 2015-2016 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
	
  * Definitions:
  
	new Taxonomy()
		static
			STATE{}
			RAGE_URI
			RAGE_QUERY
			JSON{}
			METRICS{}
			BOX{}
			EXCLUDE{}
			download(filename,text)
			fontSize(node)
			linkWidth(node)
			acroName(name)
		prototype
			loadFromServer(taxonId,done)
			loadFromFile(fileName,done)
			loadFromString(json)
			saveToText()
			saveToArray()
			saveToString()
			createDomNodes(container)
			destroyDomNodes(container)
			clearCanvas()
			drawVerticalList()
			drawVerticalTree()
			drawHorizontalTree()
			resizeCanvas(hCenter,vCenter)
			localize()
			constructNode(parent,node,level)
			fixVisibility()
			expandCollapse()
			nextLayout()
			alignNodes(getSize,getOtherSize,swap)
			keyDownNode(event,node)
			keyUpNode(event,node)
			mouseDownNode(event,node)
			mouseUpkNode(event,node)
			mouseClickNode(event,node)
			mouseDblClickNode(event,node)
			mouseLeaveNode(event,node,that)
			mouseXY(event)
			selectStyle(style,exclude)
			clearSelected()
			getSelected()
			setSelected(items)
			toggleNode(node)
			showButtonDelete()
			deleteNode()
			insertNode()
			findId(id,notNode)
			fixIds()
			resetIds()
*/



/*	========================================================
	Taxonomy

	Class. Implements the backbone management of a RAGE
	taxonomy. Creates an internal representation of the
	taxonomy hierarchy.

	new Taxonomy()

	Constructor. Creates an empty taxonomy object.

	Returns empty taxonomy.
	========================================================
*/
function Taxonomy()
{
	this.state = Taxonomy.STATE.EMPTY;
	this.stateInfo = '';
	
	this.id = undefined;
	this.root = undefined; // root node
	this.jsTaxon = undefined; // raw JS taxonomy
	
	this.langs = {}; // names of all used languages
	this.draw = this.drawVerticalList;
}


// Taxonomy states
Taxonomy.STATE = {
	EMPTY: 0,			// taxonomy instance is empty or not initialized
	PROCESSING: -1,		// taxonomy is being processed, actual state is pending
	SERVER_ERROR: 1,	// error reading taxonomy from server
	TIMEOUT_ERROR: 2,	// timeout reading taxonomy from server
	PARSE_ERROR: 3,		// error parsing the JSON_LD string
	READY: 4, 			// taxonomy is loaded and parsed
}


// Taxonomy URI/QUERY list
Taxonomy.RAGE_URI = 'http://students.it.fmi.uni-sofia.bg:8181/openrdf-sesame/repositories/rage?queryLn=SPARQL&limit=none&infer=true&Accept='+encodeURIComponent('application/ld+json');
//Taxonomy.RAGE_URI = 'http://students.it.fmi.uni-sofia.bg:8181/openrdf-sesame/repositories/rage?queryLn=WRONGURL&Accept='+encodeURIComponent('application/ld+json'); // bad request
//Taxonomy.RAGE_URI = 'http://fake-response.appspot.com/?sleep=5'; // connection failed
//Taxonomy.RAGE_URI = 'http://fake-response.appspot.com/?sleep=15'; // connection timeout
Taxonomy.RAGE_QUERY = 'CONSTRUCT { ?s ?p ?o } FROM <%id%> WHERE { ?s ?p ?o }';


// Taxonomy keywords
Taxonomy.JSON = {
	// JSON-LD keywords
	ID				: '@id',
	GRAPH			: '@graph',
	LANGUAGE		: '@language',
	VALUE			: '@value',
	TYPE			: '@type',
	// SKOS keywords
	PREF_LABEL		: 'http://www.w3.org/2004/02/skos/core#prefLabel',
	IN_SCHEME		: 'http://www.w3.org/2004/02/skos/core#inScheme',
	NARROWER		: 'http://www.w3.org/2004/02/skos/core#narrower',
	BROADER			: 'http://www.w3.org/2004/02/skos/core#broader',
	CONCEPT			: 'http://www.w3.org/2004/02/skos/core#Concept',
	CONCEPT_SCHEME	: 'http://www.w3.org/2004/02/skos/core#ConceptScheme',
	HAS_TOP_CONCEPT	: 'http://www.w3.org/2004/02/skos/core#hasTopConcept',
	TOP_CONCEPT_OF	: 'http://www.w3.org/2004/02/skos/core#topConceptOf',
	// Dublin Core keywords
	TITLE			: 'http://purl.org/dc/terms/title',
}

// size of node captions (in em)
Taxonomy.METRICS = {
	FONT_MAX_SIZE:	1.50,	// em
	FONT_MIN_SIZE:	0.75,	// em
	FONT_SCALE:		0.80,

	LINK_COLOR:		'Black',
	LINK_MAX_WIDTH:	10,		// pixels
	LINK_MIN_WIDTH:	1,		// pixels
	LINK_SCALE:		0.6,
	LINK_RADIUS:	10,		// pixels

	INDENT: 60,				// pixels
	PADDING: 30,			// pixels, padding inside canvas
	GAP: 6,					// pixels, gap around nodes
}

//	Taxonomy.BOX
Taxonomy.BOX = {
	UNCHECKED:	'u',	// node has empty/unselected box
	CHECKED: 	'c',	// node has checked/selected box
	
	RADIOBUTTON:'rb',	// node has radio button
	CHECKBOX:	'cb',	// node has checkbox
	HANDLE:		'h'		// node has handle
}

//	Taxonomy.EXCLUDE
Taxonomy.EXCLUDE = {
	NONE:	0x0,	// nothing is excluded, every node can be selected
	ROOT:	0x1,	// root node cannot be selected
	PARENTS:0x2		// parental nodes cannot be selected (only leaves can)
}



/*	--------------------------------------------------------
	Taxonomy.loadFromServer(taxonId,done)

	Method. Retrieves asynchonously a taxonomy from the
	RAGE repository as a JSON-LD string. Then parses it and
	updates the internal structure of the taxonomy instance.
		taxonId		taxonomy id
		done		a callback function to call when reading
					and parsing is done (or failed)
		error		a callback function to call if failure
		cache		a callback function to call if failure,
					but the taxonomy is in the cache
	--------------------------------------------------------
*/
Taxonomy.prototype.loadFromServer = function(taxonId,done,error,cache)
{
	// test whether taxonId the an actual JSON-LD string with a taxonomy
	if (taxonId.substring(0,2)=='[{')
	{
		this.loadFromString(taxonId);
		if (done) done(taxonId);
		return;
	}

	var that = this;

	// then test whether taxonId is an URI
	if (taxonId.indexOf('http://')==0 || taxonId.indexOf('https://')==0)
	{
		rage.getByURI({
			themeTaxonomy: encodeURIComponent(taxonId),
			success: function(data) {
				that.loadFromString(data);
				if (done) done(taxonId);
			},
			error: function(e) {
				console.log(e)
				if (done) done(taxonId);
			}
		});
		return;
	}
	
	// assume it is pure taxonId
	rage.get({
		taxonId: taxonId,
		success: function(data) {
			that.loadFromString(data);
			if (done) done();
		},
		error: function(e) {
			console.log(e)
			if (done) done();
		}
	});
	
}



/*	--------------------------------------------------------
	Taxonomy.readFromFile(fileName,done)

	Method. Retrieves asynchonously a taxonomy from the
	the local file system as a JSON-LD string. Then parses
	it and updates the internal structure of the taxonomy
	instance.
		fileName	taxonomy file name
		done		a callback function to call when reading
					and parsing is done (or failed)
	--------------------------------------------------------
*/
Taxonomy.prototype.loadFromFile = function(fileName,done)
{
	this.state = Taxonomy.STATE.PROCESSING;
	this.showMessage('message','connecting','12s',TaxonomyLang.CONNECT);
	
	var that = this;
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', fileName);
	xhr.timeout = 12000; //12s
	xhr.onload = function()
	{
		if (xhr.status === 200)
		{
			// load and digest the taxonomy
			that.loadFromString(xhr.responseText);
			if (done) done();
		}
		else
		{
			that.state = Taxonomy.SERVER_ERROR;
			that.stateInfo = this.status+' '+this.statusText;
			that.showMessage('error','error','1s',TaxonomyLang.FAILED,'<br>&lsquo; '+this.status+' '+this.statusText+' &rsquo;');
			if (done) done();
		}
	};
	xhr.onerror = function()
	{
		that.state = Taxonomy.SERVER_ERROR;
		that.stateInfo = this.status+' '+this.statusText;
		that.showMessage('error','error','1s',TaxonomyLang.FAILED,'<br>&lsquo; '+this.status+' '+this.statusText+' &rsquo;');
		if (done) done();
	};
	xhr.ontimeout = function()
	{
		that.state = Taxonomy.TIMEOUT_ERROR;
		that.showMessage('error','error','1s',TaxonomyLang.TIMEOUT);
		if (done) done();
	};
	xhr.send();
}



/*	--------------------------------------------------------
	Taxonomy.findNodeByType(type)

	Method. Finds a JS node of given type. Assumes there is
	always exactly one result.
		type		the type to search for
		
	Returns the JS node.
	--------------------------------------------------------
*/
Taxonomy.prototype.findNodeByType = function(type)
{
	try
	{
		return this.jsTaxon.filter(function(o){return o[Taxonomy.JSON.TYPE][0]==type})[0];
	} catch (err)
	{
		this.state = Taxonomy.STATE.PARSE_ERROR;
		this.stateInfo = '[2019] '+err.message;
		this.showMessage('error','error','1s',TaxonomyLang.BADTAXON,'<br>&lsquo; '+this.stateInfo+' &rsquo;');
		return;
	}
}



/*	--------------------------------------------------------
	Taxonomy.findNodeById(id)

	Method. Finds a JS node of given id. Assumes there is
	always exactly one result.
		id		the id to search for
		
	Returns the JS node.
	--------------------------------------------------------
*/
Taxonomy.prototype.findNodeById = function(id)
{
	try
	{
		return this.jsTaxon.filter(function(o){return o[Taxonomy.JSON.ID]==id})[0];
	} catch (err)
	{
		this.state = Taxonomy.STATE.PARSE_ERROR;
		this.showMessage('error','error','1s',TaxonomyLang.BADTAXON,'<br>&lsquo; '+this.stateInfo+' &rsquo;');
		return;
	}
}



/*	--------------------------------------------------------
	Taxonomy.readFromString(json)

	Method. Parses a taxonomy in a string.
		json		taxonomy as JSON-LD string
	--------------------------------------------------------
*/
Taxonomy.prototype.loadFromString = function(json)
{
	this.state = Taxonomy.STATE.PROCESSING;
	
	// parse JSON-LD into a JS object
	try
	{
		this.jsTaxon = JSON.parse(json);
		this.jsTaxon = this.jsTaxon[0][Taxonomy.JSON.GRAPH] || this.jsTaxon;
	} catch (err)
	{
		this.state = Taxonomy.STATE.PARSE_ERROR;
		this.showMessage('error','error','1s',TaxonomyLang.BADTAXON,'<br>&lsquo; '+this.stateInfo+' &rsquo;');
		return;
	};

	// exit if the JSON-LD is empty
	if (!this.jsTaxon.length)
	{
		state = Taxonomy.STATE.EMPTY;
		return;
	}
	
	// get the root node
	this.root = this.constructNode(null,this.findNodeByType(Taxonomy.JSON.CONCEPT_SCHEME),0);
	if (!this.root.label.en)
		this.root.label.en = '???';
	
	// get the id (strip the concept name after #)
	this.id = this.root.id.split('#')[0]+'#';
	
	this.state = Taxonomy.STATE.READY;
	this.hideMessage();
}



/*	--------------------------------------------------------
	Taxonomy.constructNode(parent, node, level)

	Method. Constructs a single node (either concept or
	concept scheme) from a JS taxonomy object. Recursively
	constructs its children.
		parent	the parent node of the node
		node	a JS taxonomy object node
		level	desired level of the node

	Returns a node object
	--------------------------------------------------------
*/
Taxonomy.prototype.constructNode = function(parent, obj, level)
{
	var node = {};

	node.id = obj[Taxonomy.JSON.ID];
	node.level = level;
	node.parent = parent;
	node.expanded = true;
	node.hidden = false;
	
	// extract labels in all available languages
	var that = this;
	node.label = {};
	(obj[Taxonomy.JSON.PREF_LABEL]||[]).forEach(function(elem)
	{
		var language = elem[Taxonomy.JSON.LANGUAGE];
		node.label[language] = elem[Taxonomy.JSON.VALUE];
		that.langs[language] = true;
	});

	// recursively construct children
	node.child = [];
	(obj[Taxonomy.JSON.NARROWER]||obj[Taxonomy.JSON.HAS_TOP_CONCEPT]||[]).forEach(function(elem)
	{
		elem = that.findNodeById(elem[Taxonomy.JSON.ID]);
		node.child.push( that.constructNode(node,elem,level+1) );
	});
	
	return node;
}



/*	--------------------------------------------------------
	Taxonomy.saveToText()

	Method. Generates a string with indented taxonomy nodes
	in a human readable text format.

	Returns a string with the taxonomy.
	--------------------------------------------------------
*/
Taxonomy.prototype.saveToText = function ()
{
	this.fixIds();
	
	var s = 'Taxonomy: '+this.id+'\r\n\r\n';

	var that = this;
	(function scan(node,prefix)
	{
		s += prefix;
		
		var trans = [];
		for (var lang in node.label)
			trans.push(lang+':'+node.label[lang]);
			
		var id = node.newId||node.id;
		s += trans.join(' | ')+' [#'+id.replace(that.id,'')+']\r\n';

		(node.child||[]).forEach(function(elem)
		{
			scan(elem,prefix+'  ');
		});
	})(this.root,'');

	s += '\r\nExported: '+new Date();
	
	return s;
}



/*	--------------------------------------------------------
	Taxonomy.saveToArray()

	Method. Generates an array of objects {name,uri} with
	taxonomy nodes.

	Returns an array with the taxonomy concepts.
	--------------------------------------------------------
*/
Taxonomy.prototype.saveToArray = function ()
{
	this.fixIds();
	
	var a = [];

	var that = this;
	(function scan(node,level)
	{
		var id = node.newId||node.id;
		a.push( {name:node.label['en'], uri:id, level:level} );
		
		(node.child||[]).forEach(function(elem)
		{
			scan(elem,level+1);
		});
	})(this.root,0);

	return a;
}



/*	--------------------------------------------------------
	Taxonomy.saveToString()

	Method. Generates a JSON-LD string with taxonomy.

	Returns a string with the taxonomy.
	--------------------------------------------------------
*/
Taxonomy.prototype.saveToString = function ()
{
	this.fixIds();

	// recreate a local structure of JS objects
	// representing the taxonomy as if just JSON.parse()'d
	var obj = [];
	
	var that = this;
	(function scan(node)
	{
		var nobj = {};
		
		// id and type
		nobj[Taxonomy.JSON.ID] = node.newId||node.id;
		nobj[Taxonomy.JSON.TYPE] = [(node==that.root)?Taxonomy.JSON.CONCEPT_SCHEME:Taxonomy.JSON.CONCEPT];

		// parent
		if (node!=that.root && node.parent!=that.root)
		{
			nobj[Taxonomy.JSON.BROADER] = [{}];
			nobj[Taxonomy.JSON.BROADER][0][Taxonomy.JSON.ID] = node.parent.newId||node.parent.id;
		}

		// children
		var childObj = [];
		(node.child||[]).forEach(function(elem)
		{
			var cobj = {};
			cobj[Taxonomy.JSON.ID] = elem.newId||elem.id;
			childObj.push(cobj);
		});

		// labels
		var labelObj = [];
		for (var l in node.label)
		{
			var lobj = {};
			lobj[Taxonomy.JSON.LANGUAGE] = l;
			lobj[Taxonomy.JSON.VALUE] = node.label[l];
			labelObj.push(lobj);
		}
				
		// title, has_top_concept and scheme
		if (node==that.root)
		{
			nobj[Taxonomy.JSON.TITLE] = labelObj;
			if (node.child.length)
				nobj[Taxonomy.JSON.HAS_TOP_CONCEPT] = childObj;
		}
		else
		{
			nobj[Taxonomy.JSON.IN_SCHEME] = [{}];
			nobj[Taxonomy.JSON.IN_SCHEME][0][Taxonomy.JSON.ID]=that.root.newId||that.root.id;
		}

		// children
		if (node.child.length)
			nobj[Taxonomy.JSON.NARROWER] = childObj;

		// labels
		nobj[Taxonomy.JSON.PREF_LABEL] = labelObj;

		// mark all top nodes
		if (node.parent==that.root)
		{
			nobj[Taxonomy.JSON.TOP_CONCEPT_OF] = [{}];
			nobj[Taxonomy.JSON.TOP_CONCEPT_OF][0][Taxonomy.JSON.ID]=that.root.newId||that.root.id;
		}

		obj.push(nobj);
		(node.child||[]).forEach(scan);
	})(this.root);

	return JSON.stringify(obj);
}



/*	--------------------------------------------------------
	Taxonomy.download(filename, content)

	Static method. Request the user to download a gtext file
	with the given content.
		filename	String. Suggested name of the file.
		content		String. Text to download.
		
	Code borrowed from:
	http://stackoverflow.com/questions/3665115/create-a-file-in-memory-for-user-to-download-not-through-server
	--------------------------------------------------------
*/
Taxonomy.download = function(filename, text)
{
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}



/*	--------------------------------------------------------
	Taxonomy.createDomNodes(container)

	Method. Generates DOM elements representing taxonomy
	nodes. The elements are placed inside the given
	container.
	--------------------------------------------------------
*/
Taxonomy.prototype.createDomNodes = function (container)
{
	// create divs for nodes
	var that = this;
	(function scan(node)
	{
		var div = document.createElement('div');

		{
			div.node = node;
			node.div = div;

			// style of the node
			div.className = 'node';
			if (node==that.root)
				div.setAttribute('type','root');
			else
			if (node.child.length==0)
				div.setAttribute('type','leaf');
				
			// default content and size
			div.innerHTML = that.getLabel(node);
			div.style.fontSize = Taxonomy.fontSize(node)+'em'; 
			
			// data for acem-rypy mode
			if ('acem-rypy' in options)
			{
				if (node==that.root)
					div.setAttribute('acem-rypy', node.id);
				else
				{
					var acem_rypy =  node.id.replace(that.id,'');
					div.setAttribute('acem-rypy',acem_rypy);
				}
			}

			that.createNodeEvents(div);
		}
		
		// attach to the container
		container.appendChild(div);

		(node.child||[]).forEach(scan);
	})(this.root);
	
	// create the dragging div element (visible only
	// while dragging nodes in Taxonomy Editors
	this.dragDiv = document.createElement('div');
	this.dragDiv.className = 'dragNode';
	this.dragDiv.innerHTML = '&nbsp;';
	container.appendChild(this.dragDiv);
}



/*	--------------------------------------------------------
	Taxonomy.createNodeEvents(div)

	Method. Generates the events associated with a node.
	--------------------------------------------------------
*/
Taxonomy.prototype.createNodeEvents = function (div)
{
	var that = this;

	div.addEventListener('mousedown', function(event)
	{
		that.mouseDownNode(event,event.target.node||event.target.parentElement.node||event.target.parentElement.parentElement.node);
	}, true);

	div.addEventListener('mouseup', function(event)
	{
		that.mouseUpNode(event,event.target.node||event.target.parentElement.node||event.target.parentElement.parentElement.node);
	}, false);
			
	div.addEventListener('keydown', function(event)
	{
		that.keyDownNode(event,event.target.node||event.target.parentElement.node||event.target.parentElement.parentElement.node);
	}, true);
	div.addEventListener('keyup', function(event)
	{
		that.keyUpNode(event,event.target.node||event.target.parentElement.node||event.target.parentElement.parentElement.node);
	}, true);

	if (this.tool instanceof TaxonomyEditor)
	{
		div.setAttribute('draggable','true');
		
		div.addEventListener('dragstart', function(event)
		{
			that.dragStartNode(event,event.target.node||event.target.parentElement.node||event.target.parentElement.parentElement.node);
		}, false);

		div.addEventListener('drag', function(event)
		{
			that.dragNode(event,event.target.node||event.target.parentElement.node||event.target.parentElement.parentElement.node);
		}, false);
	}
}



/*	--------------------------------------------------------
	Taxonomy.destroyDomNodes(container)

	Method. Deletes DOM elements representing taxonomy
	nodes. The elements are places inside the given
	container.
	--------------------------------------------------------
*/
Taxonomy.prototype.destroyDomNodes = function (container)
{
	if (this.state!=Taxonomy.STATE.READY)
		return;
		
	var that = this;
	(function scan(node)
	{
		container.removeChild(node.div);

		node.div.node = undefined;
		node.div.innerHTML = '';

		(node.child||[]).forEach(scan);

		node.div = undefined;
	})(this.root);
}



/*	--------------------------------------------------------
	Taxonomy.getLabel(node)

	Method. Gets the label of a node. Uses the current
	language. If no translation is available, tries to use
	English. If English is not available, uses any other
	language.
		node	the node which label is requested

	Returns a string containing the label
	--------------------------------------------------------
*/
Taxonomy.prototype.getLabel = function (node)
{
	// try requested language
	if (node.label[this.tool.lang])
		return node.label[this.tool.lang];

	// try English
	if (node.label['en'])
		return '<span class="lang">'+this.tool.lang+':</span>'+node.label['en'];

	// try any other language
	for (var lang in node.label)
		if (node.label[lang])
			return '<span class="lang">'+this.tool.lang+':</span>'+node.label[lang];

	// give up
	return '<span class="lang">?</span>';
}



/*	--------------------------------------------------------
	Taxonomy.fontSize(node)
	
	Static method. Calculates font size depending on the
	level. The size is Max*Scale^level, but not less than Min.
		node	a taxonomy node
		
	Returns a size between FONT_MIN_SIZE and FONT_MAX_SIZE
	--------------------------------------------------------
*/
Taxonomy.fontSize = function(node)
{
	var max = Taxonomy.METRICS.FONT_MAX_SIZE;
	var min = Taxonomy.METRICS.FONT_MIN_SIZE;
	var scale = Taxonomy.METRICS.FONT_SCALE;

	return Math.max(min,max*Math.pow(scale,node.level));
}



/*	========================================================
	Taxonomy.linkWidth(node)
	
	Static method. Calculates link width depending on the
	node's level. The width is Max*Scale^level, but not less
	than Min.
		node	the node
		
	Returns a size between LINE_MIN_WIDTH and LINE_MAX_WIDTH
	========================================================
*/
Taxonomy.lineWidth = function(node)
{
	var max = Taxonomy.METRICS.LINK_MAX_WIDTH;
	var min = Taxonomy.METRICS.LINK_MIN_WIDTH;
	var scale = Taxonomy.METRICS.LINK_SCALE;

	return Math.max(min,max*Math.pow(scale,node.level));
}



/*	--------------------------------------------------------
	Taxonomy.drawVerticalList()

	Method. Draws the taxonomy as a vertical list. Nodes go
	from top to bottom, each level is indented.
	--------------------------------------------------------
*/
Taxonomy.prototype.drawVerticalList = function ()
{
	// set the style of the root element
	if (!this.root) return;
	this.root.div.setAttribute('ori','vertical');

	// find the relative position of each node
	var y = 0;
	var that = this;
	(function scan(node)
	{
		if (node.hidden) return;

		node.pos = {x:Taxonomy.METRICS.INDENT*node.level, y:y};
		y += that.getHeight(node)+Taxonomy.METRICS.GAP;
		
		(node.child||[]).forEach(scan);
	})(this.root);

	this.resizeCanvas(false,false);

	// paint links
	var ctx = this.tool.ctx;
	ctx.fillStyle = 'White';
	ctx.fillRect(0,0,this.tool.canvas.width,this.tool.canvas.height);
	(function scan(node,index,array)
	{
		if (node.hidden) return;

		if (node.parent)
		{
			ctx.beginPath();
			ctx.strokeStyle = Taxonomy.METRICS.LINK_COLOR;
			ctx.lineWidth = Taxonomy.lineWidth(node.parent);
			
			var cx = that.getLeft(node);
			var cy = that.getCenterY(node);

			var px = that.getLeft(node.parent)+Taxonomy.METRICS.INDENT/2;
			var py = that.getCenterY(node.parent);
			
			ctx.moveTo(cx,cy);
			if (index<array.length-1)
				ctx.lineTo(px,cy);
			else
			{
				var rad = Taxonomy.METRICS.LINK_RADIUS;
				ctx.lineTo(px+rad,cy);
				ctx.quadraticCurveTo(px,cy,px,cy-rad);
				ctx.lineTo(px,py);
			}
			
			ctx.stroke();
		}
		
		(node.child||[]).forEach(function(node,index,array){scan(node,index,array);});
	})(this.root,0);
}



/*	--------------------------------------------------------
	Taxonomy.resizeCanvas(hCenter,vCenter)

	Method. Resizes the canvas element to fit the taxonomy
	nodes and moves the nodes inside the canvas.
		hCenter		whether nodes are centered horizontally
		vCenter		whether nodes are centered vertically
	--------------------------------------------------------
*/

Taxonomy.prototype.resizeCanvas = function (hCenter,vCenter)
{
	// find area of nodes
	var minX=1E10, minY=1E10, maxX=0, maxY=0;
	var that = this;
	(function scan(node)
	{
		if (node.hidden) return;

		minX = Math.min(minX, node.pos.x - (hCenter?0.5:0)*that.getWidth(node));
		minY = Math.min(minY, node.pos.y - (vCenter?0.5:0)*that.getHeight(node));
		maxX = Math.max(maxX, node.pos.x + (hCenter?0.5:1)*that.getWidth(node));
		maxY = Math.max(maxY, node.pos.y + (vCenter?0.5:1)*that.getHeight(node));

		(node.child||[]).forEach(scan);
	})(this.root);

	// resize the canvas
	this.tool.canvas.width = maxX-minX+2*Taxonomy.METRICS.PADDING;
	this.tool.canvas.height = maxY-minY+2*Taxonomy.METRICS.PADDING;
	this.tool.canvas.style.width = (maxX-minX+2*Taxonomy.METRICS.PADDING)+'px';
	this.tool.canvas.style.height = (maxY-minY+2*Taxonomy.METRICS.PADDING)+'px';
	
	// move all divs inside the canvas
	(function scan(node)
	{
		if (node.hidden) return;

		if (hCenter)
			that.setCenterX(node,node.pos.x+Taxonomy.METRICS.PADDING);
		else
			that.setLeft(node,node.pos.x+Taxonomy.METRICS.PADDING);
		if (vCenter)
			that.setCenterY(node,node.pos.y+Taxonomy.METRICS.PADDING);
		else
			that.setTop(node,node.pos.y+Taxonomy.METRICS.PADDING);
			
		(node.child||[]).forEach(scan);
	})(this.root);
}



/*	--------------------------------------------------------
	Taxonomy.get*(node)
	Taxonomy.set*(node,x)
	Taxonomy.set*(node,y)

	Methods. A collection of methods to get and set the
	position and size of nodes.
		node	the node
		x,y		coordinates

	Getters return the desired value in pixels.
	--------------------------------------------------------
*/

// dimensions of the node
Taxonomy.prototype.getHeight = function(node) {return node.div.offsetHeight;}
Taxonomy.prototype.getWidth  = function(node) {return node.div.offsetWidth;}

// horizontal center of the node
Taxonomy.prototype.getCenterX = function(node)   {return node.div.offsetLeft + Math.round(node.div.offsetWidth/2) - this.tool.canvas.offsetLeft;}
Taxonomy.prototype.setCenterX = function(node,x) {node.div.style.left = Math.round(this.tool.canvas.offsetLeft + x - node.div.offsetWidth/2)+'px';}

// vertical center of the node
Taxonomy.prototype.getCenterY = function(node)   {return node.div.offsetTop + Math.round(node.div.offsetHeight/2) - this.tool.canvas.offsetTop;}
Taxonomy.prototype.setCenterY = function(node,y) {node.div.style.top = Math.round(this.tool.canvas.offsetTop + y - node.div.offsetHeight/2)+'px';}

// left side of the node
Taxonomy.prototype.getLeft = function(node)   {return node.div.offsetLeft - this.tool.canvas.offsetLeft;}
Taxonomy.prototype.setLeft = function(node,x) {node.div.style.left = Math.round(this.tool.canvas.offsetLeft + x)+'px';}

// right side of the node
Taxonomy.prototype.getRight = function(node)   {return node.div.offsetLeft + node.div.offsetWidth - this.tool.canvas.offsetLeft;}
Taxonomy.prototype.setRight = function(node,x) {node.div.style.left = Math.round(this.tool.canvas.offsetLeft + x - node.div.offsetWidth)+'px';}

// top side of the node
Taxonomy.prototype.getTop = function(node)   {return node.div.offsetTop - this.tool.canvas.offsetTop;}
Taxonomy.prototype.setTop = function(node,y) {node.div.style.top = Math.round(this.tool.canvas.offsetTop + y)+'px';}

// bottom side of the node
Taxonomy.prototype.getBottom = function(node) {return node.div.offsetTop + node.div.offsetHeight - this.tool.canvas.offsetTop;}



/*	--------------------------------------------------------
	Taxonomy.localize()
	
	Method. Changes the shown language of all nodes and
	redraws the taxonomy tree.
	--------------------------------------------------------
*/
Taxonomy.prototype.localize = function ()
{
	var that = this;
	(function scan(node)
	{
		node.div.innerHTML = that.getLabel(node);
		(node.child||[]).forEach(scan);
	})(this.root);
		
	this.draw();
	this.showButtonDelete();
}



/*	--------------------------------------------------------
	Taxonomy.keyDownNode(event,node)

	Event handler. Executed when a key is pressed in a node.
		event	the event object
		node	the node being clicked
	--------------------------------------------------------
*/
Taxonomy.prototype.keyDownNode = function (event,node)
{
	// avaibale only for Editor tool
	if (!(this.tool instanceof TaxonomyEditor))
		return;
	//if (this.layout!=Taxonomy.LAYOUT.LIST)
	//	return;

	// INSERT key
	//if (event.keyCode==45)
	//{
	//	//console.log('insert key');
	//}
	
	// TAB key
	//if (event.keyCode==9)
	//{
	//	for (var i=0; i<this.nodes; i++)
	//		this.div[i].contentEditable = true;
	//}

	// ENTER key
	if (event.keyCode==13)
	{
		node.div.contentEditable = false;
		this.currentNode = node;
		this.insertNode();
		event.preventDefault();
		return false;
	}
	
	// ESC key
	if (event.keyCode==27)
	{
		node.div.contentEditable = false;
		node.div.innerHTML = this.getLabel(node);
		return false;
	}
	
	// normal key is pressed
	if (node.div.innerHTML != this.getLabel(node))
	{
		var text = node.div.innerHTML.replace(/(<([^>]+)>)/ig, "");
		node.label[this.tool.lang] = text.trim();
		this.langs[this.tool.lang] = true;
		this.draw();
	}
	return false;
}



/*	--------------------------------------------------------
	Taxonomy.keyUpNode(event,node)

	Event handler. Executed when a key is released in a node.
		event	the event object
		node	the node being clicked
	--------------------------------------------------------
*/
Taxonomy.prototype.keyUpNode = function (event,node)
{
	// avaibale only for Editor tool
	if (!(this.tool instanceof TaxonomyEditor))
		return;

	// normal key is released
	if (node.div.innerHTML != this.getLabel(node))
	{
		var text = node.div.innerHTML.replace(/(<([^>]+)>)/ig, "");
		node.label[this.tool.lang] = text.trim();
		this.langs[this.tool.lang] = true;
		this.draw();
	}
	return false;
}



/*	--------------------------------------------------------
	Taxonomy.mouseDownNode(event,node)

	Event handler. Executed when a node is mouse downed.
		event	the event object
		node	the node being clicked
	--------------------------------------------------------
*/
Taxonomy.prototype.mouseDownNode = function (event,node)
{
//console.log('down',event.timeStamp);
	event.stopPropagation();
//@@	event.preventDefault();

	(function scan(node)
	{
		node.div.contentEditable=false;
		(node.child||[]).forEach(scan);
	})(this.root);

	var dT = event.timeStamp-this.currentTime;
	this.currentTime = event.timeStamp;
	if (dT<200)
	{
		if (this.to) clearTimeout(this.to);
		this.mouseDblClickNode(event,node);
		this.currentTime = 0;
	}
}



/*	--------------------------------------------------------
	Taxonomy.mouseUpNode(event,node)

	Event handler. Executed when a node is mouse upped.
		event	the event object
		node	the node being clicked
	--------------------------------------------------------
*/
Taxonomy.prototype.mouseUpNode = function (event,node)
{
//console.log('up',event.timeStamp);
	event.stopPropagation();
//@@	event.preventDefault();

	var dT = event.timeStamp-this.currentTime;
	if (dT<300)
	{
		var that = this;
		this.to = setTimeout(function(){that.mouseClickNode(event,node);},80);
	}
}



/*	--------------------------------------------------------
	Taxonomy.mouseClickNode(event,node)

	Event handler. Executed when a node is clicked.
		event	the event object
		node	the node being clicked
	--------------------------------------------------------
*/
Taxonomy.prototype.mouseClickNode = function (event,node)
{
	if (!node)
	{
		console.log('error: mouseClickNode with node=',node,'event=',event);
		return;
	}
//console.log('click');
	this.currentNode = node;	
	node.div.contentEditable=false;
//	event.stopPropagation();
//	event.preventDefault();
	
	// in Selector tool there are checkboxes or radiobuttons,
	// so test whether the click is on that image
	if (this.tool instanceof TaxonomySelector)
	{
		//var ofsX = event.clientX-(node.div.offsetLeft-this.tool.container.scrollLeft);
		var ofsX = event.clientX-node.div.getBoundingClientRect().left;
		if (node.div.hasAttribute('kind'))
			if (ofsX < this.getHeight(node))
			{
				this.toggleNode(node);
				return;
			}
	}		
	// in Editor tool there are handles
	// so test whether the click is on that handle
	if (this.tool instanceof TaxonomyEditor)
	{
		var ofsX = event.clientX-(node.div.offsetLeft+this.tool.container.parentElement.offsetLeft-this.tool.container.scrollLeft);
//console.log('click on',this.getLabel(node),'x=',event.clientX,node.div.offsetLeft,this.tool.container.parentElement.offsetLeft,this.tool.container.scrollLeft,'max x=',this.getHeight(node));
		if (node.div.hasAttribute('kind'))
			if (ofsX < this.getHeight(node))
			{
//console.log('click on',this.getLabel(node));
				// clear all marked nodes and then mark the
				// whole branch of the clicked node
				this.setSelected([]);
				var that = this;
				(function scan(node)
				{
					that.toggleNode(node);
					(node.child||[]).forEach(scan);
				})(node);
	
				this.showButtonDelete();
				return;
			}

		this.currentNode = undefined;
		this.clearSelected();
		this.showButtonDelete();
	}
	
	// expand or collapse the node
	// leaf nodes are not expandible
	if (node.child.length==0) return;

	if (node==this.root)
	{
		// expand or collapse all nodes
		this.expandCollapse();
	}
	else
	{
		// toggle the expansion of the node
		node.expanded = !node.expanded;
		this.fixVisibility();

		// is the current element is out of view?

		// beyond left edge?
		if (this.tool.container.scrollLeft>this.getLeft(node))
			this.tool.container.scrollLeft = this.getLeft(node);
			
		// beyond right edge?
		if (this.tool.container.scrollLeft+this.tool.container.offsetWidth<this.getRight(node))
			this.tool.container.scrollLeft = this.getRight(node)-this.tool.container.offsetWidth;
	}
}



/*	--------------------------------------------------------
	Taxonomy.dragStartNode(event,node)

	Event handler. Executed when a node is started to dragged.
		event	the event object
		node	the node to be dragged
	--------------------------------------------------------
*/
Taxonomy.prototype.dragStartNode = function (event,node)
{
	if (!(this.tool instanceof TaxonomyEditor))
		return;
		
	this.currentNode = node;	
	node.div.contentEditable=false;
			
	this.mouseClickNode(event,node);
	event.dataTransfer.setData('application/x-node', node.id);
}



/*	--------------------------------------------------------
	Taxonomy.dragNode(event,node)

	Event handler. Executed while a node is being dragged.
		event	the event object
		node	the node being dragged
	--------------------------------------------------------
*/
Taxonomy.prototype.dragNode = function (event,node)
{
	if (!(this.tool instanceof TaxonomyEditor))
		return;
	this.tool.buttonDelete.style.display = 'none';
	return true;
}



/*	--------------------------------------------------------
	Taxonomy.dragDone(event,node)

	Event handler. Executed while a node is dropped.
		event	the event object
		node	the node being dropped
	--------------------------------------------------------
*/
Taxonomy.prototype.dragDone = function (event)
{
	if (!(this.tool instanceof TaxonomyEditor))
		return;
		
	// node id and 
	var id = event.dataTransfer.getData('application/x-node');
	var pos = this.mouseXY(event);
	var dragNode = this.currentNode;

	var afterNode;
	
	// find the lowerest node before the drop point
	var that = this;
	(function scan(node)
	{
		if (node!=dragNode)
		if (that.getCenterY(node)<pos.y)
		{
			if (!afterNode || that.getCenterY(node)>that.getCenterY(afterNode))
					afterNode = node;
		}
		(node.child||[]).forEach(scan);
	})(this.root);

	// if above all nodes, assume drop is after the root
	if (!afterNode)
		afterNode = this.root;
		
	// protect against recursion (e.g. moving a branch
	// somewhere inside itself)
	var parent = afterNode.parent;
	while (parent)
	{
		if (parent==dragNode) return;
		parent = parent.parent;
	}
	
	// deattach the drag node from its parent
	var siblings = dragNode.parent.child;
	siblings.splice(siblings.indexOf(dragNode),1);
	dragNode.parent = null;



//console.log('drop',this.getLabel(dragNode),'at',[pos.x,pos.y],'after',this.getLabel(afterNode));

	// calculate sibling zone - i.e. where dropping creates
	// a sibling. To the left of the zone are created parents,
	// to the right of the zone are created children
	var zone = Math.round((this.getLeft(afterNode)-pos.x)/Taxonomy.METRICS.INDENT);
	
	var down = {a:0};
	var up = {a:1};
	var nodeList = [];
	
	function scan(node)
	{
		nodeList.push(node);
		if (node==afterNode)
		{
			if (zone<0)
				nodeList.push(down);
			else
			if (zone>0)
				for (var i=0; i<zone; i++) nodeList.push(up);
			scan(dragNode);
			if (zone<0)
				nodeList.push(up);
			else
			if (zone>0)
				for (var i=0; i<zone; i++) nodeList.push(down);
		}
		
		if (node.child.length)
		{
			nodeList.push(down);
			(node.child||[]).forEach(scan);
			node.child = [];
			nodeList.push(up);
		}
	}
	scan(this.root);
	
	var parent = this.root;
	for (var i=2; i<nodeList.length; i++)
	{
		if (nodeList[i]===down)
		{
			if (parent.child.length)
				parent = parent.child[parent.child.length-1];
		}
		else
		if (nodeList[i]===up)
		{
			if (parent.parent) parent = parent.parent;
		}
		else
		{
			nodeList[i].parent = parent;
			parent.child.push(nodeList[i]);
		}
	}

	// fix levels
	(function scan(node)
	{
		if (node.parent)
			node.level = node.parent.level+1;

		node.div.style.fontSize = Taxonomy.fontSize(node)+'em'; 

		if (node.level)
			if (node.child.length==0)
				node.div.setAttribute('type','leaf');
			else
				node.div.removeAttribute('type');


		(node.child||[]).forEach(scan);
	})(this.root);
	
	// redraw the nodes
	this.draw();
	
	return true;
}



/*	--------------------------------------------------------
	Taxonomy.mouseDblClickNode(event,node)

	Event handler. Executed when a node is double-clicked.
		event	the event object
		node	the node being clicked
	--------------------------------------------------------
*/
Taxonomy.prototype.mouseDblClickNode = function (event,node)
{
//console.log('dbl click');
//	event.stopPropagation();
//	event.preventDefault();

	// ignore if not in editor
	if (!(this.tool instanceof TaxonomyEditor))
		return;
	
	if (!node)
	{
		console.log('error: mouseDblClickNode with node=',node,'event=',event);
		return;
	}

	this.clearSelected();
	node.div.contentEditable = true;
	var ofsX = event.clientX-(node.div.offsetLeft-this.tool.container.scrollLeft);
	if (ofsX>this.getHeight(node))
	{
		node.div.focus();
		//var that = this;
		// http://stackoverflow.com/questions/3805852/select-all-text-in-contenteditable-div-when-it-focus-click
		window.setTimeout(function() {
			var sel, range;
			if (window.getSelection && document.createRange) {
				range = document.createRange();
				range.selectNodeContents(node.div);
				sel = window.getSelection();
				sel.removeAllRanges();
				sel.addRange(range);
			} else if (document.body.createTextRange) {
				range = document.body.createTextRange();
				range.moveToElementText(node.div);
				range.select();
			}
		}, 1);
		return;
	}

}



/*	--------------------------------------------------------
	Taxonomy.mouseLeaveNode(event,node,that)

	Event handler. Executed when a node is left.
		event	the event object
		node	the node being clicked
		that	the taxonomy
	--------------------------------------------------------
*/
/*
Taxonomy.prototype.mouseLeaveNode = function (event,node,that)
{
	event.stopPropagation();
	event.preventDefault();
	node.div.contentEditable = false;

	if (node.div.innerHTML != that.getLabel(node))
	{
		var text = node.div.innerHTML.replace(/(<([^>]+)>)/ig, "");
		node.label[that.tool.lang] = text.trim();
		that.langs[that.tool.lang] = true;
	}
}
*/



/*	--------------------------------------------------------
	Taxonomy.showButtonDelete()

	Method. Shows or hides the delete button.
	If shown, use a size comparable to the node where the
	buttons are shown.
	--------------------------------------------------------
*/
Taxonomy.prototype.showButtonDelete = function ()
{
	// ignore if not in editor
	if (!(this.tool instanceof TaxonomyEditor))
		return;
		
	var node = this.currentNode;
	if (node)
	{	// show the [Delete] button
		var gap = this.getHeight(node)/2;
		this.tool.buttonDelete.style.display = 'inline-block';
		this.tool.buttonDelete.style.height = this.getHeight(node)+'px';
		this.tool.buttonDelete.style.width = this.getHeight(node)+'px';
		switch (this.draw)
		{
			case this.drawVerticalList:
			case this.drawHorizontalTree:
				this.tool.buttonDelete.style.top = (this.getCenterY(node)+this.tool.canvas.offsetTop-this.tool.buttonDelete.offsetHeight/2)+'px';
				this.tool.buttonDelete.style.left = (this.getLeft(node)-0.5*this.getHeight(node)+this.tool.canvas.offsetLeft-0.5*Taxonomy.METRICS.INDENT-this.tool.elem.offsetLeft+2)+'px';
				break;
			case this.drawVerticalTree:
				this.tool.buttonDelete.style.top = (this.getTop(node)+this.tool.canvas.offsetTop-this.tool.buttonDelete.offsetHeight/2-Taxonomy.METRICS.INDENT/2)+'px';
				this.tool.buttonDelete.style.left = (this.getCenterX(node)-0.5*this.getHeight(node)+this.tool.canvas.offsetLeft-this.tool.elem.offsetLeft+2)+'px';
				break;
		}
	}
	else
	{	// hide the delete buttons
		this.tool.buttonDelete.style.display = 'none';
	}
}



/*	--------------------------------------------------------
	Taxonomy.fixVisibility()

	Method. Scans all nodes and hide those that are in
	collapsed branches of the taxonomy.
	--------------------------------------------------------
*/
Taxonomy.prototype.fixVisibility = function()
{
	var that = this;
	if (this.root)
	(function scan(node,expanded)
	{
		// set visibility and display style
		node.hidden = !expanded;
		node.div.style.display = expanded?'block':'none';

		// if node is collapsed or is in collapsed branch
		// then all children should be invisible too
		expanded &= node.expanded;
		(node.child||[]).forEach(function(node){scan(node,expanded);});
	})(this.root,true);

	this.draw();
}



/*	--------------------------------------------------------
	Taxonomy.expandCollapse()

	Method. If half of the nodes or more are expanded, than
	collapse the taxonomy down to its first level, otherwise
	expand it.
	--------------------------------------------------------
*/

Taxonomy.prototype.expandCollapse = function ()
{
	// count number of visible nodes
	var visible = 0;
	(function scan(node)
	{
		visible += node.hidden?-1:1;
		(node.child||[]).forEach(scan);
	})(this.root);

	// expand all if visible<0
	(function scan(node)
	{
		if (node.parent) node.expanded = (visible<0);
		(node.child||[]).forEach(scan);
	})(this.root,true);
		
	this.fixVisibility();
}




/*	--------------------------------------------------------
	Taxonomy.nextLayout()

	Method. Switches to the next layout.
	--------------------------------------------------------
*/
Taxonomy.prototype.nextLayout = function ()
{
	if (this.draw==this.drawVerticalList)
		this.draw = this.drawVerticalTree;
	else
	if (this.draw==this.drawVerticalTree)
		this.draw = this.drawHorizontalTree;
	else
		this.draw = this.drawVerticalList;

	// in editor, the new labels are marked, so update
	// the div's to remove the mark
	if (this.tool instanceof TaxonomyEditor)
	{
		var that = this;
		(function scan(node)
		{
			if (that.draw===that.drawVerticalList)
				node.div.setAttribute('draggable','true');
			else
				node.div.setAttribute('draggable','false');
			node.div.innerHTML = that.getLabel(node);
			(node.child||[]).forEach(scan);
		})(this.root);

		this.clearSelected();
	}
	
	this.draw();
	this.showButtonDelete();
}



/*	--------------------------------------------------------
	Taxonomy.clearCanvas()

	Method. Clears the drawing canvas.
	--------------------------------------------------------
*/
Taxonomy.prototype.clearCanvas = function ()
{
	var ctx = this.tool.ctx;
	ctx.fillStyle = 'White';
	ctx.fillRect(0,0,this.tool.canvas.width,this.tool.canvas.height);
}



/*	--------------------------------------------------------
	Taxonomy.drawVerticalTree()

	Method. Draws the taxonomy as a vertical tree. Levels
	go top to bottom, nodes go left to right.
	--------------------------------------------------------
*/
Taxonomy.prototype.drawVerticalTree = function ()
{
	// set the style of the root element
	this.root.div.setAttribute('ori','vertical');

	this.alignNodes(this.getWidth,this.getHeight,true);
	this.resizeCanvas(true,false);

	// paint links
	var ctx = this.tool.ctx;
	ctx.fillStyle = 'White';
	ctx.fillRect(0,0,this.tool.canvas.width,this.tool.canvas.height);

	// draw links child->parent
	var that = this;
	(function scan(node,index,array)
	{
		if (node.hidden) return;

		if (node.parent)
		{
			ctx.beginPath();
			ctx.strokeStyle = Taxonomy.METRICS.LINK_COLOR;
			ctx.lineWidth = Taxonomy.lineWidth(node.parent);
			
			var cx = that.getCenterX(node);
			var cy = that.getCenterY(node);

			var px = that.getCenterX(node.parent);
			var py = that.getBottom(node.parent);
			var my = py+Taxonomy.METRICS.INDENT/2;
			
			var rad = Taxonomy.METRICS.LINK_RADIUS;
			var first = (index==0) && (array.length>1);
			var last  = (index==array.length-1) && (array.length>1);
			
			ctx.moveTo(cx,cy);
			if (first)
			{	// drawing the the link from the left-most child
				ctx.lineTo(cx,my+rad);
				ctx.quadraticCurveTo(cx,my,cx+rad,my);
				ctx.lineTo(px,my);
			} else
			if (last)
			{	// drawing the link from the right-most child
				ctx.lineTo(cx,my+rad);
				ctx.quadraticCurveTo(cx,my,cx-rad,my);
				ctx.lineTo(px,my);
			} else
			{	// drawing internal links
				ctx.lineTo(cx,my);
			}

			// link to parent
			if (index==0)
				ctx.lineTo(px,py);
			
			ctx.stroke();
		}
		
		(node.child||[]).forEach(function(node,index,array){scan(node,index,array);});
	})(this.root,0);
}



/*	--------------------------------------------------------
	Taxonomy.alignNodes(getSize,getOtherSize,swap)

	Method. Aligns the nodes in a tree. Used internally by
	drawVerticalTree and drawHorizontalTree.
		getSize		function to get the main size of a node
					(this is either getWidth or getHeight)
		getOtherSize function to get the other size of a node
					(this is either getHeight or getWidth)
		swap		swap x<->y coordinates
	--------------------------------------------------------
*/
Taxonomy.prototype.alignNodes = function (getSize,getOtherSize,swap)
{
	// find the size of each branch starting from a node -
	// it is max(node-height,sum(child-branch-height))
	(function scan(node)
	{
		node.w = 0;
		if (node.hidden) return;

		node.w = Math.max(0,(node.child.length-1)*Taxonomy.METRICS.GAP);
		(node.child||[]).forEach(function(child){
			scan(child);
			node.w += child.w;
		});

		var ownW = getSize(node);
		node.wOfs = (ownW>node.w)?(ownW-node.w)/2:0;
		node.w = Math.max(node.w,ownW);
	})(this.root);
	
	// set position of each node with (x,y) the upper left
	// corner of its branch
	(function scan(node,x,y)
	{
		if (node.hidden) return;

		if (swap)
		{
			node.pos = {x:x+node.w/2, y:y};
			x += node.wOfs;
			y += getOtherSize(node)+Taxonomy.METRICS.INDENT;
		}
		else
		{
			node.pos = {x:x, y:y+node.w/2};
			y += node.wOfs;
			x += getOtherSize(node)+Taxonomy.METRICS.INDENT;
		}
		
		(node.child||[]).forEach(function(child){
			scan(child,x,y);
			if (swap)
				x += child.w+Taxonomy.METRICS.GAP;
			else
				y += child.w+Taxonomy.METRICS.GAP;
		});
	})(this.root,0,0);
}



/*	--------------------------------------------------------
	Taxonomy.drawHorizontalTree()

	Method. Draws the taxonomy as a horizontal tree. Levels
	go left to right, nodes go top to bottom.
	--------------------------------------------------------
*/
Taxonomy.prototype.drawHorizontalTree = function ()
{
	// set the style of the root element
	this.root.div.setAttribute('ori','horizontal');

	this.alignNodes(this.getHeight,this.getWidth,false);
	this.resizeCanvas(false,true);

	// paint links
	var ctx = this.tool.ctx;
	ctx.fillStyle = 'White';
	ctx.fillRect(0,0,this.tool.canvas.width,this.tool.canvas.height);

	// draw links child->parent
	var that = this;
	(function scan(node,index,array)
	{
		if (node.hidden) return;

		if (node.parent)
		{
			ctx.beginPath();
			ctx.strokeStyle = Taxonomy.METRICS.LINK_COLOR;
			ctx.lineWidth = Taxonomy.lineWidth(node.parent);
			
			var cx = that.getCenterX(node);
			var cy = that.getCenterY(node);

			var px = that.getRight(node.parent);
			var py = that.getCenterY(node.parent);
			var mx = px+Taxonomy.METRICS.INDENT/2;
			
			var rad = Taxonomy.METRICS.LINK_RADIUS;
			var first = (index==0) && (array.length>1);
			var last  = (index==array.length-1) && (array.length>1);
			
			ctx.moveTo(cx,cy);
			if (first)
			{	// drawing the the link from the top-most child
				ctx.lineTo(mx+rad,cy);
				ctx.quadraticCurveTo(mx,cy,mx,cy+rad);
				ctx.lineTo(mx,py);
			} else
			if (last)
			{	// drawing the link from the right-most child
				ctx.lineTo(mx+rad,cy);
				ctx.quadraticCurveTo(mx,cy,mx,cy-rad);
				ctx.lineTo(mx,py);
			} else
			{	// drawing internal links
				ctx.lineTo(mx,cy);
			}

			// link to parent
			if (index==0)
				ctx.lineTo(px,py);
			
			ctx.stroke();
		}
		
		(node.child||[]).forEach(function(node,index,array){scan(node,index,array);});
	})(this.root,0);
}



/*	--------------------------------------------------------
	Taxonomy.selectStyle(style,exclude)

	Method. Sets non-excluded nodes as selectible as
	checkboxes or radiobuttons.
		style		Number. A value from Taxonomy.BOX
		exclude		Number. A value from Taxonomy.EXCLUDE
	--------------------------------------------------------
*/
Taxonomy.prototype.selectStyle = function(style,exclude)
{
	var that = this;
	(function scan(node)
	{
		if (!node) return;
		if (exclude==Taxonomy.EXCLUDE.NONE
		|| (exclude==Taxonomy.EXCLUDE.ROOT && node!=that.root)
		|| (exclude==Taxonomy.EXCLUDE.PARENTS && !node.child.length))
		{
			node.div.setAttribute('kind',style);
		}
		
		(node.child||[]).forEach(scan);
	})(this.root);

	this.clearSelected();
}



/*	--------------------------------------------------------
	Taxonomy.clearSelected()

	Method. Set all nodes as unselected.
	--------------------------------------------------------
*/
Taxonomy.prototype.clearSelected = function()
{
	this.currentNode = undefined;
	this.setSelected([]);
}



/*	--------------------------------------------------------
	Taxonomy.toggleNode(node)

	Method. Toggles node between checked and unchecked
	state. If radio buttons are used, then allow at most one
	node to be checked.
		node	the node to toggle
	--------------------------------------------------------
*/
Taxonomy.prototype.toggleNode = function (node)
{
	if (!node.div.hasAttribute('kind')) return;
	
	
	if (node.div.getAttribute('checked')==Taxonomy.BOX.CHECKED)
		node.div.setAttribute('checked',Taxonomy.BOX.UNCHECKED);
	else
	{
		if (node.div.getAttribute('kind')==Taxonomy.BOX.RADIOBUTTON)
			this.clearSelected();
		node.div.setAttribute('checked',Taxonomy.BOX.CHECKED);
	}
}



/*	--------------------------------------------------------
	Taxonomy.getSelected()

	Methods. Gets a list of ids and labels of all selected
	items.

	Returns an array of {id:string, label:string}.
	--------------------------------------------------------
*/
Taxonomy.prototype.getSelected = function()
{
	var result = [];

	var that = this;
	(function scan(node)
	{
		if (node.div.getAttribute('checked')==Taxonomy.BOX.CHECKED)
			result.push({id: node.id, label:that.getLabel(node)});
		
		(node.child||[]).forEach(scan);
	})(this.root);

	return result;
}



/*	--------------------------------------------------------
	Taxonomy.setSelected(items)

	Method. Set items as selected.
		items	A list of repository id strings
	--------------------------------------------------------
*/

Taxonomy.prototype.setSelected = function(items)
{
	var that = this;
	(function scan(node)
	{
		if (!node) return;
		if (node.div.hasAttribute('kind'))
		{
			node.div.setAttribute('checked',Taxonomy.BOX.UNCHECKED);
			if (items.indexOf(node.id)>-1)
				that.toggleNode(node);
		}
		
		(node.child||[]).forEach(scan);
	})(this.root);
}



/*	--------------------------------------------------------
	Taxonomy.deleteNode() //68
	
	Method. Delete a node with index in currentNode and
	also delete all its children.
	--------------------------------------------------------
*/
Taxonomy.prototype.deleteNode = function()
{
	if (!this.currentNode)
		return; // should never happen, but can't fight Murphy's law

	// destroy DOM nodes, but keep the root
	var that = this;
	var parent = this.currentNode.parent;
	(function scan(node)
	{
		if (node!=that.root)
		{
			that.tool.container.removeChild(node.div);

			node.div.node = undefined;
			node.div.innerHTML = '';
			node.div = undefined;
			node.parent = undefined;
		}
		(node.child||[]).forEach(scan);
		node.child = [];
	})(this.currentNode);

	if (parent)
		parent.child.splice(parent.child.indexOf(this.currentNode),1);
	this.currentNode = undefined;
	this.draw();
	this.showButtonDelete();
}



/*	========================================================
	Taxonomy.insertNode()
	
	Method. Inserts empty node as a sibling
	========================================================
*/
Taxonomy.prototype.insertNode = function()
{
	// insert node after node
	var node = this.currentNode;
	var parentNode = node.parent;

	var newNode = {
		child: [],
		expanded: true,
		hidden: false,
		label: {},
		id: '',//v4//this.uniqueId(),
	}

	if (parentNode)
	{
		parentNode.child.splice(parentNode.child.indexOf(node)+1,0,newNode);
	}
	else
	{
		parentNode = this.root;
		parentNode.child.splice(0,0,newNode);
	}
	newNode.parent = parentNode;
	newNode.level = parentNode.level+1;
	
	var div = document.createElement('div');
	div.className = 'node';
	div.setAttribute('type','leaf');
	div.setAttribute('kind',Taxonomy.BOX.HANDLE);
	div.setAttribute('checked',Taxonomy.BOX.UNCHECKED);
	div.innerHTML = this.getLabel(newNode);
	div.style.fontSize = Taxonomy.fontSize(newNode)+'em'; 
	div.node = newNode;
	
	if ('acem-rypy' in options)
	{
		div.setAttribute('acem-rypy', newNode.id.replace(this.id,''));
	}

	this.createNodeEvents(div);

	this.tool.container.appendChild(div);
	newNode.div = div;

	this.draw();

	div.contentEditable=true;
	div.focus();
	document.execCommand('selectAll',false,null);
}



/*	========================================================
	Taxonomy.mouseXY(event)

	Method. Retrieves the mouse coordinates in the canvas
	coordinate system.
		event	a mouse event object
	========================================================
*/
Taxonomy.prototype.mouseXY = function (event)
{
	return {
		x:	event.clientX
			- this.tool.canvas.offsetLeft
			- this.tool.container.offsetLeft
			- this.tool.container.offsetParent.offsetLeft
			+ window.scrollX
			+ this.tool.container.scrollLeft
			+ this.tool.container.offsetParent.scrollLeft,
			//- 1.0*this.getLineHeight(this.drag),
		y:	event.clientY
			- this.tool.canvas.offsetTop
			- this.tool.container.offsetTop
			- this.tool.container.offsetParent.offsetTop
			+ window.scrollY
			+ this.tool.container.scrollTop
			+ this.tool.container.offsetParent.scrollTop
	};
}



/*	--------------------------------------------------------
	Taxonomy.findId(id,notNode)

	Method. Searched for a node with a given id.
	
	Returns the node (if found), otherwise - undefined
	--------------------------------------------------------
*/
Taxonomy.prototype.findId = function (id,notNode)
{
	var found;
	(function scan(node)
	{
		if (node!=notNode)
			if (found || node.id==id)
			{
				if (!found) found = node;	// fix Jan-27
				//found = node; 			// fix Jan-27
				return;
			}
		(node.child||[]).forEach(scan);
	})(this.root);
	return found;
}



/*	--------------------------------------------------------
	Taxonomy.getName()

	Method. Gets the name of a taxonomy. Uses only the root
	node. 

	Returns a string containing the name
	--------------------------------------------------------
*/
Taxonomy.prototype.getName = function ()
{
	// try requested language
	if (this.root.label[this.tool.showLang])
		return this.root.label[this.tool.showLang];

	// try English
	if (this.root.label['en'])
		return this.root.label['en'];

	// try any other language
	for (var lang in this.root.label)
		if (this.root.label[lang])
			return this.root.label[lang];

	// give up
	var a = this.id.split('/');
	a = a[a.length-1];
	a = a.split('#');
	a = a[0];
	return a;
}



/*	--------------------------------------------------------
	Taxonomy.showMessage(className,animation,duration,caption,aux)

	Method. Show a message or an error message.
	--------------------------------------------------------
*/
Taxonomy.prototype.showMessage = function(className,animation,duration,caption,aux)
{
	(function scan(node)
	{
		if (!node||!node.div) return;
		node.div.style.display = 'none';
		(node.child||[]).forEach(scan);
	})(this.root);
	
	if (this.tool)
	{
		this.tool.canvas.style.display = 'none';
		this.tool.info.className = className;
		this.tool.info.style.animationName = animation;
		this.tool.info.style.animationDuration = duration;
		this.tool.info.style.display = 'block';
		this.tool.info.innerHTML = (caption[this.tool.lang]||caption['en'])+(aux||'');
	}
	else
	{
		//console.log('TAXONOMY MESSAGE',caption.en);
	}
}
Taxonomy.prototype.hideMessage = function()
{
	if (this.tool)
	{
		this.tool.canvas.style.display = 'block';
		this.tool.info.style.display = 'none';
	}
}


/*	--------------------------------------------------------
	Taxonomy.acroName(name)

	Function. Returns a simple acronymic version of a name.
	--------------------------------------------------------
*/
Taxonomy.acroName = function (name)
{
	function v(ch) { return ch=='a' || ch=='o' || ch=='i' || ch=='e' || ch=='u' || ch=='y'; } 
	function acroWord(w)
	{
		var i=0, n=w.length;
		while (i<n && v(w[i])) i++;
		while (i<n && !v(w[i])) i++;
		while (i<n && v(w[i])) i++;
		while (i<n && !v(w[i])) i++;
		//while (i<n && v(w[i])) i++;
		//while (i<n && !v(w[i])) i++;
		if (i>=n-2) i+=2;
		var head = w.substring(0,i);
		var tail = w.substring(i,n).split('a').join('').split('o').join('').split('i').join('').split('e').join('').split('u').join('').split('y').join('');
		return head+tail;
	}
	
	var offWords = ['&amp;','&nbsp;','#',' and ',' for ',' of ',' off ',' in ',' on ',' a ',' an ',' the ','-','(',')','_',',','.',' to '];
	// remove & and spaces
	name = ' '+name.toLowerCase()+' ';
	for (var i=0; i<offWords.length; i++)
		name = name.split(offWords[i]).join(' ');

	name = name.split(' ');	
	for (var i=name.length-1; i>=0; i--)
		if (!name[i])
			name.splice(i,1);

	if (name.length==1)
		return escape(name[0]);
		
	if (name.length>3)
		name.splice(3,name.length);
		
	for (var i=0; i<name.length; i++)
		name[i] = acroWord(name[i]);

	return escape(name.join('-'));
}



/*	--------------------------------------------------------
	Taxonomy.fixIds()

	Method. Replaces all empty Ids with a text derived
	from the English name of the node.
	--------------------------------------------------------
*/
Taxonomy.prototype.fixIds = function ()
{
	var that = this;

	// process taxonomy nodes' ids
	var i=0;
	(function scan(node)
	{
		var name = node.label['en'];
		var id = node.id.replace(that.id,''); // pure id

		if (!id)
		{
			id = that.id+Taxonomy.acroName(name);
			if (that.findId(id,node))
			{
				i++;
				while (that.findId(id+'('+i+')',node)) i++;
				id = id+'('+i+')';
			}
			node.id = id;

			// show the name
			if ('acem-rypy' in options)
			{
				node.div.setAttribute('acem-rypy',node==that.root?node.id:node.id.split('#')[1]);
			}
		}
		
		//console.log(name,newId);
		(node.child||[]).forEach(scan);
	})(this.root);
}



/*	--------------------------------------------------------
	Taxonomy.resetIds()

	Method. Replaces all Ids with a text derived
	from the English name of the node.
	--------------------------------------------------------
*/
Taxonomy.prototype.resetIds = function ()
{
	var that = this;

	var taxonId = 'http://rageproject.eu/taxonomy/'+Taxonomy.acroName(this.root.label['en'])+'#';
	
	// process taxonomy nodes' ids
	var i=0;
	(function scan(node)
	{
		var name = node.label['en'];
		var id = node.id.replace(that.id,''); // pure id

		id = taxonId+Taxonomy.acroName(name);
		if (that.findId(id,node))
		{
			i++;
			while (that.findId(id+'('+i+')',node)) i++;
			id = id+'('+i+')';
		}
//		console.log(node.id,'->',id);
		node.id = id;
		
		// show the name
		if ('acem-rypy' in options)
		{
			node.div.setAttribute('acem-rypy',node==that.root?node.id:node.id.split('#')[1]);
		}
		
		//console.log(name,newId);
		(node.child||[]).forEach(scan);
	})(this.root);
}


