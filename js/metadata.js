/*

  *	metadata.js
	Implementation of the RAGE Metadata Meta-editor.

  * Copyright 2015-2016 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union\’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.

  * Definitions:
		
	new AssetMetadata(url,id)
	
	static methods
		unescape(text)
		rgb(r,g,b,level)
		gradient(r,g,b,level)
		sameName(name)
		prettyName(name)
		findNode(parent,uri)
		findHTML(html,node)
		
	prototype methods
		realType(type)
		toggleElement(event)
		cloneElement(event)
		clearElement(event)
		selectTaxonomyConcepts(event)
		checkTaxonomyConcepts(event)
		showTaxonomyConceptNames(taxonURI)
		createUI()
		populateUI()
		compactUI()
		generateMetadataXML()
		checkXML()
		showXML()
*/


var ASSET_MANAGER_URL = '../Asset Manager/assetmanager.html';
var ARTEFACT_MANAGER_URL = '../Asset Manager/artefactmanager.html';

if (typeof LOG_XML === 'undefined')
{
	LOG_XML = false;
	LOG_XML_DUMP_NODES = false;
	LOG_HTTP_REQUEST = false;
}


// Global options extracted from document's URL
options = {};
var EXTERN = EXTERN||'';

function retrieveOptions()
{
	var match,
		pl     = /\+/g,  // Regex for replacing addition symbol with a space
		search = /([^&=]+)=?([^&]*)/g,
		decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
		query  = window.location.search.substring(1);

	while (match = search.exec(query))
	   options[decode(match[1])] = decode(match[2]);

	if (EXTERN)
	{	// the editor is run offline as configuration editor

		console.log('EXTERN: Loading CSS from EXTERN.CSS ('+EXTERN.CSS.length+' bytes)');
		var css = document.createElement('style');
		css.type = 'text/css';
		if (css.styleSheet) css.styleSheet.cssText = EXTERN.CSS; // IE
		else css.appendChild(document.createTextNode(EXTERN.CSS)); // non-IE
		document.getElementsByTagName("head")[0].appendChild(css);
	}
	else
	{	// the editor is run online as metadata editor
	
		//load css files with widget styling info
		var link = document.createElement( "link" );
		link.href = 'css/'+(options.ui||'metadata')+'-ui.css';
		link.type = "text/css";
		link.rel = "stylesheet";
		link.media = "screen,print";
		document.getElementsByTagName( "head" )[0].appendChild( link );

		//load skin
		var link = document.createElement( "link" );
		link.href = 'css/'+(options.skin||'classic')+'-skin.css';
		link.type = "text/css";
		link.rel = "stylesheet";
		link.media = "screen,print";
		document.getElementsByTagName( "head" )[0].appendChild( link );
	}
}
retrieveOptions();

// Global custom metadata properties
uiStyles = {};
function retrieveUIStyles()
{
	var sheets = document.styleSheets;
	for (var s in sheets) if (sheets[s])
	{
		//console.log('CSS',sheets[s].href);
		//console.log('CSS',sheets[s].cssRules);
		try
		{
		var rules = sheets[s].rules || sheets[s].cssRules;
		}
		catch (e){};
		if (!rules) continue;
		//console.log('has',rules.length,'rules');
		for (var r in rules) if (rules[r] && rules[r].selectorText)
		{
			if (rules[r].selectorText.indexOf('metadata-editor')==0)
			{
				//console.log('\t',rules[r].selectorText,'=','|'+rules[r].style.content.slice(1,-1)+'|');
				var uri = rules[r].selectorText.split('uri=')[1];
				if (uri.length<2) continue;
				//console.log('->>>',uri);
				if (uri.indexOf('"')>=0)
					uri = uri.split('"')[1];
				else
					uri = uri.split("'")[1];
				uiStyles[uri]={};
				var content = rules[r].style.content.slice(1,-1).split(';');
				//console.log(content);
				for (var c in content)
				{
					var pair = content[c].split(':');
					if (pair.length<2) continue;
					//console.log(pair[0].trim(),'=',pair[1].trim());
					uiStyles[uri][pair[0].trim()] = pair.slice(1).join(':').trim();
				}
			}
		}
	}

	var elems = document.getElementsByClassName('acem-rypy');
	for (var i=0; i<elems.length; i++)
	{
		elems[i].style.display = ('acem-rypy' in options)?'inline-block':'none';
	}
	
	if ('acem-rypy' in options)
	{
		LOG_XML |= ('xml' in options);
		LOG_XML_DUMP_NODES |= ('nodes' in options);
		LOG_HTTP_REQUEST |= ('http' in options);
	}

	if ('developer' in options)
	{
		var elems = document.getElementsByClassName('developer');
		for (var i=0; i<elems.length; i++)
			elems[i].style.display = 'inline-block';
	}
	
	if (('back' in options) && options.back)
	{
		if (options.permissions=='rw')
			document.getElementById('cancelButton').style.display = 'inline-block';
		else
			document.getElementById('backButton').style.display = 'inline-block';
	}
}

/*	========================================================

	AssetMetadata(url,id)

	Constructor. Creates an instance of an Asset metadata.

		url		- URL of an XML file containing metadata
		id		- DOM id where to generate the UI
		
	========================================================
*/
AssetMetadata = function(url,id)
{
	retrieveUIStyles();

	// if there is no URL, then try options.resource & options.file
	if (!url)
	{
		if (options.resource)
			//url = rage.SERVER+'/assets/metadata?resource=' + encodeURIComponent(options.resource+'/files/metadata');
			url = rage.SERVER+'/assets/' + options.resource + '/metadata';
		else
		if (options.file)
			url = decodeURIComponent(options.file);
		else
			url = 'xml/SampleAsset.xml';
	}
	if (LOG_XML)
	{
		console.log('Metadata URI =',url);
	}
	
	this.nodes = {};
	this.pendingRequests = 0;
	this.domId = id;
	this.RDF = ''; 						// set by XMLFile.definitionLoaded()
	this.RDF_LITERAL = ''; 				// set by XMLFile.simplifyNodes()
	this.RDF_RESOURCE = ''; 			// set by XMLFile.simplifyNodes()
	this.RDF_RESOURCE_OR_LITERAL = ''; 	// set by XMLFile.simplifyNodes()

	this.xmlErrors = [];
	
	this.taxonomyList = [];
	this.taxonomyData = {};
	this.metadata = undefined;
	this.url = url;

	// create hidden taxonomy box
	this.box = document.createElement('div');
	this.box.style.height = '400px';
	this.box.style.border = 'solid 4px Tomato';
	this.box.style.fontSize = '75%';
	
	var that = this;
	this.selector = new TaxonomySelector(this.box,'');
	this.selector.buttonCancel.onclick = function(){ that.box.parentElement.removeChild(that.box); };
	
	this.rootHTML = document.getElementById(this.domId);
	
	// create error box
	this.errorBox = document.createElement('div');
	this.errorBox.className = 'errorbox';
	this.rootHTML.appendChild( this.errorBox );

	this.loadCSSTaxonomies();
}
AssetMetadata.id = 0;

/*	--------------------------------------------------------
	Taxonomy.usingTaxonomy(taxonId,done)

	Method. Retrieves asynchonously a taxonomy from the
	RAGE repository as a JSON-LD string. 
		taxonId		taxonomy id
		done		a callback function to call when reading
					and parsing is done (or failed)
	--------------------------------------------------------
*/
AssetMetadata.prototype.usesTaxonomy = function(taxonURI,done)
{
	// if it exists - then just exit
	if (this.taxonomyData[taxonURI]) return;

	this.pendingRequests++;
	this.taxonomyData[taxonURI] = new Taxonomy();

	if (LOG_HTTP_REQUEST) console.log('HTTP Request',taxonURI,' ('+this.pendingRequests+')');

	var that = this;
	
	if (taxonURI.indexOf('json/')==0)
	{ // this is a local taxonomy
		if (LOG_HTTP_REQUEST) console.log('attemp loading local taxonomy',taxonURI);
		that.taxonomyData[taxonURI].loadFromFile(taxonURI,function(){
			that.pendingRequests--;
			if (LOG_HTTP_REQUEST) console.log('HTTP    Completed',taxonURI,' ('+that.pendingRequests+')');
			if (done) done();
		});
	}
	else
	{ // this is a server taxonomy
		rage.getByURI({
			themeTaxonomy: encodeURIComponent(taxonURI),
			success: function(data) {
				that.pendingRequests--;
				if (LOG_HTTP_REQUEST) console.log('HTTP    Done',taxonURI,' ('+that.pendingRequests+')');
				that.taxonomyData[taxonURI].loadFromString(data);
				if (done) done();
			},
			error: function(e) {
				if (LOG_HTTP_REQUEST) console.log('HTTP    Fail',taxonURI,' ('+that.pendingRequests+')');
				that.errorBox.style.display = 'inline-block';
				var name = taxonURI.split('#'), name = name[name.length-1];
				that.errorBox.innerHTML += '<div>Cannot retrieve taxonomy "<b>'+name+'</b>".</div>';
			},
			cache: function(e) {
				that.errorBox.style.display = 'inline-block';
				var name = taxonURI.split('#'), name = name[name.length-1];
				that.errorBox.innerHTML += '<div>Cannot retrieve taxonomy "<b>'+name+'</b>" - a cached copy is used.</div>';
			}
		});
	}
}



AssetMetadata.prototype.loadCSSTaxonomies = function()
{
	var failedTaxons = [];
	// request content of all taxonomies that are statically used in the styles
	for (var i in uiStyles)
		if (uiStyles[i].widget=='ListTaxonomy')
		{
			var taxonURI = uiStyles[i].uri;
			var that = this;
			this.usesTaxonomy(taxonURI,function()
			{
				if (that.pendingRequests==0)
				{
					if (LOG_HTTP_REQUEST)
						console.log('Luchy you! Taxonomies are loaded. Coninue with the list of taconomies.');
					that.loadListOfTaxonomies();
				}
				else
				{
					if (LOG_HTTP_REQUEST)
						console.log('Pending taxonomies:',that.pendingRequests);
				}
			});
		}

	// if no new requests are issued, then the loadListOfTaxonomies above will never be called	
	if (this.pendingRequests==0)
		this.loadListOfTaxonomies();
}
AssetMetadata.prototype.loadListOfTaxonomies = function()
{
	function success(data)
	{
		var json = JSON.parse(data);
		for (var i in json)
		{
			var taxon = {
				uri:  json[i][DCAT_THEME_TAXONOMY][0]['@id'],
				//id:   json[i][DCTERMS_IDENTIFIER][0]['@value'],
				id:   json[i]['@id'],
				name: json[i][DCTERMS_TITLE][0]['@value']
			};

			var title = json[i][DCTERMS_TITLE];
			for (var t in title)
				if (title[t]['@language']=='en')
					taxon.name = title[t]['@value'];
			that.taxonomyList.push(taxon);
			//console.log(taxon);
		}
		
		that.metadata = new XMLFile(that,that.url,true); // asset editor (online)
	}
	
	function error(e)
	{
		that.errorBox.style.display = 'inline-block';
		that.errorBox.innerHTML += '<div>Cannot retrieve the list of all taxonomies.</div>';
	}
	
	function cache(e)
	{
		that.errorBox.style.display = 'inline-block';
		that.errorBox.innerHTML += '<div>Cannot retrieve the list of all taxonomies - a cached copy is used.</div>';
	}
	
	// request a list of all taxonomies
	var that = this;
	rage.list({success: success, error: error, cache: cache});
}

/*	========================================================
	AssetMetadata.rgb(r,g,b,level)

	Function. Returns a CSS color by darkening (r,g,b) by
	the level depth.
	
		r		red component (0-255)
		g		green component (0-255)
		b		blue component (0-255)
		level	depth level
	
	Outputs CSS color string.
	========================================================
*/
AssetMetadata.rgb = function(r,g,b,level)
{
	return 'rgb('+(r-10*level)+','+(g-10*level)+','+(b-10*level)+')';
}


/*	========================================================
	AssetMetadata.unescape(text)

	Function. Returns the same text with some escaped symbols
	unescaped: &amp;
	
		text	string to unescape
	
	Outputs unescaped string.
	========================================================
*/
AssetMetadata.unescape = function(text)
{
	text = text.split('&amp;').join('&');
	return text;
}


/*	========================================================
	AssetMetadata.gradient(r,g,b,level)

	Function. Returns a CSS horizontal gradient by darkening
	(r,g,b) by the level depth.
	
		r		red component (0-255)
		g		green component (0-255)
		b		blue component (0-255)
		level	depth level
	
	Outputs CSS gradient string.
	========================================================
*/
AssetMetadata.gradient = function(r,g,b,level)
{
	return 'linear-gradient(to right,rgb('+(r-10*level)+','+(g-10*level)+','+(b-10*level)+'),white)';
}


/*	========================================================
	AssetMetadata.sameName(name)

	Function. Dummy function. Used in list boxes options.
	
		name	name to return
	
	Outputs the same name as the input.
	========================================================
*/
AssetMetadata.sameName = function(name)
{
	return name;
}


/*	========================================================
	AssetMetadata.prettyName(name)

	Function. Splits a name into words depending on the
	letter case: 'thisIsNameURL' -> 'this is name URL'.
	
		name	name to prettify
	
	Outputs a string with the new name.
	========================================================
*/
AssetMetadata.prettyName = function(name)
{
	// add space between locase and upcase characters
	for (var i=name.length-1; i>0; i--)
		if ( (name[i] == name[i].toUpperCase()) && (name[i-1] != name[i-1].toUpperCase()) && ')"-'.indexOf(name[i])<0)
			name = name.substring(0,i)+' '+name.substring(i);
	return name;
}


/*	========================================================
	AssetMetadata.prototype.realType(type)

	Function. Returns the most basic type of a given type.
	Iterates through the nodes[] array. For example, the
	real type of 'http://purl.org/dc/terms/description' is
	'http://www.w3.org/1999/02/22-rdf-syntax-ns#LiteralType'.
	
		type	a string representing a type
		
	Outputs a string of the real type.
	========================================================
*/
AssetMetadata.prototype.realType = function(type)
{
	while (this.nodes[type] && this.nodes[type].fullType)
		type = this.nodes[type].fullType;
	return type;
}


/*	========================================================
	AssetMetadata.prototype.unpack(type)

	Function. Returns the full type name/uri based on the
	prefixes stored in the metadata. For example, 'dcterms:title'
	unpacks into 'http://purl.org/dc/terms/title'.
	
		type	a string representing a type
		
	Outputs a string of the full type.
	========================================================
*/
AssetMetadata.prototype.unpack = function(type)
{
	return this.metadata.unpack(type);
}


/*	========================================================
	AssetMetadata.prototype.createUI()

	Command. Generate the user interface of an asset
	metadata editor.
	========================================================
*/
AssetMetadata.prototype.createUI = function()
{
	// create tabs
	var tabs = document.createElement('div');
	tabs.className='tabs';
	this.rootHTML.appendChild(tabs);

	var that = this;
	function createTab(tab,cn)
	{
		if (document.getElementById('tab-'+tab))
			return;
		var elem = document.createElement('span');
		elem.className = cn||"tabbutton";
		elem.id = 'tab-'+tab;
		if (tab!='&nbsp;')
		{
			elem.setAttribute('href','');
			elem.addEventListener('click',that.showTab);
		}
		else
			elem.style.cursor = 'default';
		
		if (!tab)
		{
			elem.style.marginLeft = '1em';
			elem.innerHTML = 'All';
		}
		else
			elem.innerHTML = tab.replace('_',' ');
			
		tabs.appendChild(elem);
	}
	
	createTab('Main');
	for (var i in uiStyles) if (uiStyles[i].tab)
		createTab(uiStyles[i].tab);
	createTab(''); // tab [All]
	createTab('&nbsp;','tabborder'); // tab for background line
	
	// if there are only three tabs (main, all and line), clear all tabs
	if (tabs.children.length<=3)
	{
		tabs.style.display = 'none';
	}
	
	//this.metadata.dumpNodes();
	
	// get the main tag of the metadata
	//console.log('@@@METADATA=',this.metadata);
	//console.log(this.unpack(this.metadata.dom.tagName));
	this.rootNode = this.nodes[this.unpack(this.metadata.dom.tagName)];
	if (!this.rootNode) throw '1415';
	
	// create the metadata
	var elem = UIWidgets.createNodeUI(this,this.rootNode,null,null,0);
	this.rootHTML.appendChild( elem );

	// fill data
	this.populateUI();

	// retest all loaded taxonomies
	for (var t in this.taxonomyData)
	{
		this.showTaxonomyConceptNames(t);
	}
	
	// compact empty frames
	this.compactUI();
	this.showTab({target:tabs.children[0]});
	
	// store the original XML - this is used to find the
	// difference with the final version
	this.originalXML = this.generateMetadataXML();
	
// debug
//console.log(this.generateMetadataXML()	);
	//this.checkXML();
}



/*	========================================================
	AssetMetadata.toggleElement(event)

	Event handler. Expands or collapses a UI node. Only
	labels could be toggable.
	========================================================
*/
AssetMetadata.toggleElement = function(event)
{
	var elem = event.target;
	var expand = Boolean(elem.getAttribute('collapsed'));
	if (expand)
		elem.removeAttribute('collapsed');
	else
		elem.setAttribute('collapsed','true');
		
	// when tooltips were added (2017.01) the label became
	// buried one level deeper, so go up one level
	if (!elem.nextSibling)
	{
		elem = elem.parentNode;
	}
	
	while (elem.nextSibling)
	{
		elem = elem.nextSibling;
		if (elem && elem.tagName!='SELECT')
		{
			if (expand)
				elem.classList.remove('collapsed');
			else
				elem.classList.add('collapsed');
		}
	}
}


/*	========================================================
	AssetMetadata.prototype.cloneElement(event)

	Event handler. Clones a node and all its data. Clones
	event handlers and clears inputs fields.
	========================================================
*/
AssetMetadata.prototype.cloneElement = function(event)
{
	// first hide the taxonomy selector, if it is visible
	if (this.box.parentElement) this.box.parentElement.removeChild(this.box);

	var button = event.target;
	var row = button.previousSibling;
	var newRow = UIWidgets.createNodeUI(this,row.node,row.nodeParentNode,row.nodeOriginalParentNode,row.nodeLevel);

	button.parentNode.insertBefore(newRow,button);
}


/*	========================================================
	AssetMetadata.prototype.clearElement(event)

	Event handler. Clears a node and all its data.
	========================================================
*/
AssetMetadata.prototype.clearElement = function(event)
{
	// first hide the taxonomy selector, if it is visible
	if (this.box.parentElement) this.box.parentElement.removeChild(this.box);

	var button = event.target;
	var row = button.parentElement;

	// there are two options:
	//	(1) deleting a row with clones should just remove the row
	//	(2) the last row of a given type should remain, it is just cleared
	var keepRow = true;
	if(row.node.max=='unbounded')
	{
		var canDelete = (row.previousSibling && row.previousSibling.node==row.node)
					 || (row.nextSibling && row.nextSibling.node==row.node);

		// keep the row only if it is the last of a kind
		keepRow = !canDelete;
	}
	
	// clearning is done by creating a new row and removing the current row
	var newRow = UIWidgets.createNodeUI(this,row.node,row.nodeParentNode,row.nodeOriginalParentNode||null,row.nodeLevel);
	if (keepRow) row.parentNode.insertBefore(newRow,row);
	row.parentNode.removeChild(row);
	
	if (keepRow)
	{
		var rows = newRow.getElementsByClassName('row');
		for (var i=0; i<rows.length; i++)
			this.compactRow(rows[i]);
	}
}


/*	========================================================
	AssetMetadata.prototype.showTaxonomyConceptNames(taxonURI)

	A taxonomy is just loaded. Scan all HTML elements for
	nodes having values from this taxonomy, set the values
	to the labels and move the concept uri to the uri
	(because initially the uri is stored as a value)
	========================================================
*/
AssetMetadata.prototype.showTaxonomyConceptNames = function(taxonURI)
{
	var taxon = this.taxonomyData[taxonURI];
	if (taxon.state!=Taxonomy.STATE.READY)
		return;
		
	//console.log('retesting taxonomy',taxonURI);

	var rows = document.getElementsByClassName('row');
	//console.log('retesting taxonomy rows count',rows.length);
	for (var i=0; i<rows.length; i++)
		if (rows[i].widget) 							// the row has a widget
		if (rows[i].widget.name=='ListTaxonomies')		// and it is ListTaxonomies
		if (rows[i].widget.getVal(rows[i])==taxonURI)	// and its value is taxonURI
	{
		//console.log(rows[i],'->',rows[i].widget.getVal(this,rows[i]));
		
		// now rows[i] is the Theme Taxonomy row, scan all
		// sibling rows and fix the labels of used concepts
		for (var row=rows[i].nextSibling; row; row=row.nextSibling)
		{
			// the row has no widget, it must some row for buttons or acem-rypy
			if (!row.widget)
				continue;
				
			// the row is already coded (i.e. has uri field)
			if (row.conceptURI)
				continue;
				
			// stop scanning until a row with another widget is found
			if (row.widget.name!='TaxonomyConcept')
				break;

			var value = row.widget.getVal(row);
			//console.log('\t',value);
			var node = taxon.findId(value);
			if (!node)
				continue;
			//console.log('\ttaxon node',node);
			
			row.conceptURI = node.id;
			row.widget.setVal(row,AssetMetadata.unescape(node.label['en']));	
		}
	}
	//console.log('retesting taxonomy',taxonURI,'done');
}


/*	========================================================
	AssetMetadata.prototype.selectTaxonomyConcepts(event)

	Event handler. Selects taxonomy concepts and updates
	the list of concepts as inputs fields.
	========================================================
*/
AssetMetadata.prototype.selectTaxonomyConcepts = function(event)
{
	var button = event.target;

	// find the taxonomy
	var taxonElem = undefined;
	var taxonRow = button;

	// if the click in on INPUT, then move to parent row
	if (taxonRow.tagName=='INPUT')
		taxonRow = taxonRow.parentElement.parentElement;

	// scan backwards for a row of type ListTaxonomy		
	for (; taxonRow; taxonRow=taxonRow.previousSibling)
	{
		if (taxonRow.widget && taxonRow.widget.name=='ListTaxonomies')
			break;
	}
	if (!taxonRow) return;
	var taxonURI = taxonRow.widget.getVal(taxonRow);
	if (!taxonURI) return;
	
	//console.log('working with',taxonURI);

	// collect currently selected concepts
	var selected = [];
	for (var row=taxonRow.nextSibling; row; row = row.nextSibling)
	{
		if (!row.widget) continue;
		if (row.widget.name!='TaxonomyConcept') break;
		selected.push( row.widget.getVal(row) );
	}
	
	taxonRow.parentElement.appendChild(this.box);
	this.box.scrollIntoView(false);
	
	var that = this;

	this.selector.buttonOK.onclick = function ()
		{	// OK is pressed
			// hide the taxonomy selector
			that.box.parentElement.removeChild(that.box);

			// clear the selected nodes
			for (var row=taxonRow.nextSibling; row; row = row.nextSibling)
			{
				if (!row.widget) continue;
				if (row.widget.name!='TaxonomyConcept') break;
				row.widget.setVal(row,'');
				row.conceptURI = undefined;
			}

			// extract the selected nodes
			var selected = that.selector.taxon.getSelected();
			row = taxonRow.nextSibling;
			if ('acem-rypy' in options) row = row.nextSibling;
			
			for (var i=0; i<selected.length; i++)
			{
				//console.log('row=',row);
				// reached the end of rows, create a new one
				if (row.className=='button')
				{
					var event = {};
					event.target = row;
					row.onclick(event);
					row = row.previousSibling;
				}

				// if the row is for taxonomy concepts, then display the concept
				if (row.widget && row.widget.name=='TaxonomyConcept')
				{
					row.widget.setVal(row,AssetMetadata.unescape(selected[i].label));
					row.conceptURI = selected[i].id;
					//console.log('row.conceptURI =',selected[i].id);
				}
				row=row.nextSibling;
			}
			taxonRow.scrollIntoView(true);
		}
		
	this.selector.detachTaxonomy();
	if (!this.taxonomyData[taxonURI])
	{
		// load a taxonomy
		var that = this;
		this.taxonomyData[taxonURI] = new Taxonomy();
		this.taxonomyData[taxonURI].loadFromServer(taxonURI,function(){
			that.selector.attachTaxonomy(that.taxonomyData[taxonURI]);
			that.taxonomyData[taxonURI].selectStyle(Taxonomy.BOX.CHECKBOX,Taxonomy.EXCLUDE.ROOT);
			that.selector.taxon.setSelected(selected);
			that.selector.buttonLayout.style.display = 'none';
		});
	}
	else
	{
		// switch a taxonomy
		this.selector.attachTaxonomy(this.taxonomyData[taxonURI]);
		this.taxonomyData[taxonURI].selectStyle(Taxonomy.BOX.CHECKBOX,Taxonomy.EXCLUDE.ROOT);
		this.taxonomyData[taxonURI].setSelected(selected);
		this.selector.buttonLayout.style.display = 'none';
	}	
}



/*	========================================================
	AssetMetadata.prototype.checkTaxonomyConcepts(event)

	Event handler. Removes taxonomy concepts because the
	current selected taxonomy has changed.
	========================================================
*/
AssetMetadata.prototype.checkTaxonomyConcepts = function(event)
{
	// hide the box if shown
	if (this.box.parentElement)
		this.box.parentElement.removeChild(this.box);

	// find the taxonomy
	var taxonRow = event.target.parentElement;
	if (!taxonRow) return;
	
	//console.log('working with',taxonURI);

	// clear currently selected concepts
	for (var row=taxonRow.nextSibling; row; row = row.nextSibling)
	{
		if (!row.widget) continue;
		if (row.widget.name!='TaxonomyConcept') break;
		row.widget.setVal(row,'');
		row.conceptURI = undefined;
	}

	// automatically open to select concepts
	this.selectTaxonomyConcepts({target:taxonRow});
}



/*	========================================================
	AssetMetadata.prototype.populateUI()

	Command. Extracts metadata from asset metadata file
	and inserts them in the visual elements from the UI.
	If needed, creates new rows of metadata.
	========================================================
*/
AssetMetadata.prototype.populateUI = function()
{
	// skip the first 2 children -- errorBox and tabs 
	UIWidgets.populateNodeUI( this, this.rootHTML.children[2], this.metadata.dom );
}


/*	========================================================
	AssetMetadata.findNodeByURI(parent,uri)

	Function. Searches a node from an array given its uri.
	
		parent	parent node, must have array[] element
		uri		the uri string to search for
		
	Ouputs the node with the given uri, or null if not found.
	========================================================
*/
AssetMetadata.findNodeByURI = function(parent,uri)
{
	if (parent.array)
		for (var i in parent.array)
			if (parent.array[i].uri && parent.array[i].uri==uri)
				return parent.array[i];
	//console.log('not found',uri,'in',parent);
	return null;
}


/*	========================================================
	AssetMetadata.findHTML(html,node)

	Function. Searches the last node of specific type in
	a DOM representation of some HTML code.
	
		html	the DOM of an HTML code
		node	the node to search for
		
	Ouputs the last HTML element referencing the given node,
	or null if not found.
	========================================================
*/
AssetMetadata.findElemByNode = function(html,node)
{
	//console.log('search',node.name);
	for (var elem = html.lastChild; elem; elem=elem.previousSibling)
		if (elem.node && elem.node===node)
		{
			// check for the last element with the same node
			return elem;
		}
	return null;
}


/*	========================================================
	AssetMetadata.prototype.compactUI()

	Command. Executed after metadata population. All fragment
	that are empty from metadata and that can be collapsed
	are collapse by toggling their label.
	
	This operation is only for making the UI shorter by
	collapsing all unused metadata elements.
	========================================================
*/
AssetMetadata.prototype.compactRow = function(row)
{
	if (!row.widget) return;
	if (row.autoExpand) return;
	if (row.widget.hasData(row)) return;
	
	var label = row.widget.labelElem(row);
	if (label.onclick)
		label.onclick({target:label});
}
AssetMetadata.prototype.compactUI = function()
{
	var box = document.getElementById(this.domId);

	var rows = box.getElementsByClassName('row');
	for (var i=0; i<rows.length; i++)
		this.compactRow(rows[i]);
}


/*	========================================================
	AssetMetadata.prototype.generateMetadataXML()

	Function. Generates XML metadata from the current data
	in the UI.
	
	Outputs a string with the XML contents.
	========================================================
*/
AssetMetadata.prototype.generateMetadataXML = function()
{
	// prefixes
	var attr = '';
	var xmlns = ''; // potentially duplicated xmlns cases problems - the DOM cannot be constructed from such metadata
	for (prefix in this.metadata.prefix)
	{
		var name = XMLNS+(prefix?':':'')+prefix;
		var value = this.metadata.prefix[prefix];
		attr += '\n\t'+name+'="'+value+'"';
		if (!prefix) xmlns = name+'="'+value+'"';
	}
	attr += '\n\txsi:schemaLocation="'+this.metadata.xsi+'"';

	var row = this.rootHTML.firstChild;
	attr += ' metaIndex="1"';// metaSubdex="0"';

	// forget all errors of type incompleteData
	// NB! the array is scanned backwards, becase this is a
	// live list -- when one element is removed from the class
	// it is dynamically removed from the list
	var errors = this.rootHTML.getElementsByClassName('incompleteData');
	for (var i=errors.length-1; i>=0; i--)
		errors[i].classList.remove('incompleteData');
	var errors = this.rootHTML.getElementsByClassName('errorInChild');
	for (var i=errors.length-1; i>=0; i--)
		errors[i].classList.remove('errorInChild');
	
	while (row && !row.widget)
		row = row.nextSibling;

	var str = '<?xml version="1.0" encoding="UTF-8"?>';
	this.xmlErrors = [];
	str += row.widget.xml(this,row,0,attr);
	str += '\n\n<!-- The RAGE Metadata Editor | '+(new Date()).toUTCString()+' -->';
	
	// now check the number of occurences of xmlns
	var first = str.indexOf(xmlns);
	var last = str.lastIndexOf(xmlns);
	if (first<last)
		str = str.substring(0,first)+str.substring(first+xmlns.length);
	
	return str;
}


/*	========================================================
	AssetMetadata.prototype.shorten(name)

	Function. Shorten a name using the available prefixes.
	This is the opposite operation of unpack. For example,
	'http://rageproject.eu/2015/asset/Asset' when shorten
	becomes 'rage:asset'.
	
		name	URI name
		
	Outputs a string for the tag name (shrten uri).
	========================================================
*/
AssetMetadata.prototype.shorten = function(name)
{
	var prefixes = this.metadata.prefix;
	for (var prefix in prefixes)
	{
		if (name.indexOf(prefixes[prefix])==0)
			return prefix+(prefix?':':'')+name.substring(prefixes[prefix].length);
	}
	return name;
}


/*	========================================================
	AssetMetadata.prototype.checkXML()

	Command. Generates the XML file and checks it.
	========================================================
*/
AssetMetadata.prototype.checkXML = function(extraMessage,okMessage)
{
	this.errorBox.style.display = 'none';
	
	// clear the tabs
	var tabs = document.getElementsByClassName('tabbutton');
	for (var i=0; i<tabs.length; i++)
		tabs[i].setAttribute('error','false');

	
	var xml = this.generateMetadataXML();
	//console.log(this.xmlErrors);
	if (this.xmlErrors.length)
	{
		showMessage('There are incomplete metadata.<br>They are coloured in red.'+(extraMessage||''),closeMessage);

		/*
		this.errorBox.style.display = 'inline-block';
		var html = 'Incomplete metadata: ';
		for (var i=0; i<this.xmlErrors.length; i++)
		{
			html += '<a href="#'+this.xmlErrors[i].getAttribute('id')+'">';
			
			html += AssetMetadata.prettyName(this.xmlErrors[i].node.name);
			if (this.xmlErrors[i].nodeOriginalParentNode)
			{
				//console.log(this.xmlErrors[i].nodeOriginalParentNode);
				html +=' of '+AssetMetadata.prettyName(this.xmlErrors[i].nodeOriginalParentNode.name)
			}
			html += '</a>';
			if (i<this.xmlErrors.length-1)
				html += ', ';
		}

		var errElem = document.getElementById('incomplete');
		if (errElem)
			errElem.innerHTML = html;
		else
			this.errorBox.innerHTML += '<div id="incomplete">'+html+'</div>';
		this.errorBox.style.display = 'inline-block';
		*/
		xml = '';
	}
	else
	{	// no missing metadata
		if (okMessage)
			showMessage(okMessage,closeMessage);
	}
	
	return xml;
}



/*	========================================================
	AssetMetadata.prototype.showXML()

	Command. Generates the XML file and shows it in a new
	browser tab.
	========================================================
*/
AssetMetadata.prototype.showXML = function()
{
	var xml = this.checkXML();
	
	xml = xml.split('<').join('&lt;');
	xml = xml.split('>').join('&gt;');
	xml = xml.split('\t').join('    ');
	x=window.open();
	x.document.open();
	x.document.write('<!DOCTYPE html><head><meta charset="utf-8"/></head><body><pre>'+xml+'</pre></body>');
	x.document.close();
}



/*	========================================================
	AssetMetadata.prototype.showXMLDiff()

	Command. Generates the XML diff file and shows it in the
	console.
	========================================================
*/
AssetMetadata.prototype.showXMLDiff = function()
{
	var xml = this.checkXML();
	
	metadiff(this.originalXML,xml);
}



/*	========================================================
	AssetMetadata.prototype.showStyles()

	Command. Generates the HTML with debug info of uiStyles[]
	and shows it in a new browser tab.
	========================================================
*/
AssetMetadata.prototype.showStyles = function()
{
	var html = '';
	
	// scan all styles
	var keys = Object.keys(uiStyles).sort();
	for (var i in keys)
	{
		html += '<div style="border-bottom: solid 1px black">';
		html += '<h3>'+keys[i]+'</h3><ul>';
		
		// scan style properties
		var style = uiStyles[keys[i]];
		var subkeys = Object.keys(style).sort();
		for (var j in subkeys)
		{
			html += subkeys[j]+'\t= \''+style[subkeys[j]]+'\'<br>';
		}
		
		html += '</ul></div>';
	}
	x=window.open();
	x.document.open();
	x.document.write('<!DOCTYPE html><head><meta charset="utf-8"/></head><body style="zoom: 100%"><pre>'+html+'</pre></body>');
	x.document.close();
}


/*	========================================================
	AssetMetadata.prototype.showNodes()

	Command. Generates the HTML with debug info of nodes[]
	and shows it in a new browser tab.
	========================================================
*/
AssetMetadata.prototype.showNodes = function()
{
	var html = '';
	
	function htmlNode(node)
	{
		var html = '';
		html += '<div>';

		html += '<big><b>'+node.name+'</b></big> ['+node.min+'..'+node.max+'] ';
		if (node.name!=node.uri) html+='<span style="color: gray;">uri=</span><b style="color:Teal;">'+node.uri+'</b><br>';
		
		html += '<div style="margin-left: 2em;">';
		var props = Object.keys(node);
		for (var p in props)
		{
			if (['array','uri','name','min','max','realType','realNode'].indexOf(props[p])>-1)
				continue;
	
			html += '<div style="display:inline-block; color: gray; width:5em"><small>'+props[p]+'</small></div>= '
			html += node[props[p]];
			html += '<br>';
			
		}
		
		if (node.array)
			if (node.enumeration)
			{
				html += '<div style="display:inline-block; color: gray; width:5em;"><small>array</small></div>= ['+node.array+']<br>';
			}
			else
			{
				html += '<div style="border: solid 1px DarkSeaGreen; padding: 0.1em 0.3em; margin-bottom:0.5em;">';
				for (var i=0; i<node.array.length; i++)
				{
					html += htmlNode(node.array[i]);
				}
				html += '</div>';
			}
		
		html += '</div>';
		html += '</div>';
		return html;
	}
	
	// scan all top-level nodes
	for (var i in this.nodes)
	{
		html += htmlNode(this.nodes[i],i);
	}
	x=window.open();
	x.document.open();
	x.document.write('<!DOCTYPE html><head><meta charset="utf-8"/></head><body style="zoom: 100%"><pre>'+html+'</pre></body>');
	x.document.close();
}


/*
XMLFile.prototype.dumpNodes = function()
{
	function dumpNode(node,level)
	{
		if (node.reference)
		{
			console.log( str+'REFERENCE '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri );
		}
			
		if (node.extension)
		{
			console.log( str+'EXTENSION '+node.type+' ['+node.min+'..'+node.max+'] '+node.fullType );
		}
		
		if (node.simple)
		{
			console.log( str+'ELEMENT '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri+' ['+node.type+']' );
		}
		
		if (node.choice)
		{
			console.log( str+'CHOICE '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri );
			for (var i=0; i<node.array.length; i++)
				dumpNode(node.array[i],level+1);
		}
		
		if (node.attribute)
		{
			console.log( str+'ATTRIBUTE '+node.name+' ['+node.min+'..'+node.max+'] '+node.fullType );
		}
		
		if (node.complex)
		{
			console.log( str+'COMPLEX '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri );
			for (var i=0; i<node.array.length; i++)
				dumpNode(node.array[i],level+1);
		}
		
		if (node.enumeration)
		{
			console.log( str+'ENUMERATION '+node.name+' ['+node.min+'..'+node.max+'] '+node.uri );
			for (var i=0; i<node.array.length; i++)
				console.log( str+'\t '+node.array[i] );
		}
	}

	for (var i in this.asset.nodes)
	{
		dumpNode(this.asset.nodes[i],0);
	}
	
	console.log( 'RDF', this.asset.RDF );
}
*/

/*	========================================================
	AssetMetadata.prototype.showTab(tab)

	Command. All nodes with the given tab are shows. All
	others are hidden.
	========================================================
*/
AssetMetadata.prototype.showTab = function(e)
{
	var buts = document.querySelectorAll('.tabbutton');
	for (var i=0; i<buts.length; i++)
		buts[i].setAttribute('selected','false');
	
	e.target.setAttribute('selected','true');
	
	var tab = e.target.id.split('tab-')[1];
	
	if (tab)
	{
		// hide all
		var rows = document.querySelectorAll('[toplevel]');
		for (var i=0; i<rows.length; i++)
		{
			rows[i].style.display = 'none';
		}
		// show some
		var rows = document.querySelectorAll('[toplevel][tab='+tab+']');
		for (var i=0; i<rows.length; i++)
		{
			rows[i].style.display = rows[i].className=='button'?'inline-block':'block';
		}
	}
	else
	{
		// show all
		var rows = document.querySelectorAll('[toplevel]');
		for (var i=0; i<rows.length; i++)
		{
			rows[i].style.display = rows[i].className=='button'?'inline-block':'block';
		}
	}
}


/*	========================================================
	AssetMetadata.prototype.localSaveXML()

	Command. A temporary function to save the XML in a local
	file.
	========================================================
*/
AssetMetadata.prototype.localSaveXML = function()
{
	var xml = this.checkXML();

	// remove all metaIndex="..."
	xml = xml.replace(/ metaIndex="\d+"/g,'');
	
	var clock = new Date();
	var blob = new Blob([xml], {type: "text/plain;charset=utf-8"});
	saveAs(blob, "metadata_"+clock.getTime()+".xml");
}


/*	========================================================
	AssetMetadata.prototype.localLoadXML()

	Command. A temporary function to load metadata from a
	local XML file.
	========================================================
*/
AssetMetadata.prototype.localLoadXML = function(filePath)
{
	if(filePath.files && filePath.files[0])
	{
		var reader = new FileReader();
		reader.onload = function (e)
		{
			var xml = e.target.result;
			document.getElementById(asset.domId).innerHTML = '';
			asset = new AssetMetadata(xml,'assetbox');
		};
		reader.readAsText(filePath.files[0]);
	}
}


/*	========================================================
	AssetMetadata.prototype.toBack()

	Command. Go back to predefined address (stored in
	options.back.
	========================================================
*/
AssetMetadata.prototype.toBack = function()
{
	var url = options.back;
	window.location.href = decodeURIComponent(url);
}


/*	========================================================
	closeMessage()

	Command. Hides the message window.
	========================================================
*/
function closeMessage()
{
	document.getElementById('whitening').style.display = 'none';
	document.getElementById('message').style.display = 'none';
}



/*	========================================================
	showMessage(message,ok)

	Command. Shows a given message in the message window.
	========================================================
*/
function showMessage(message,ok)
{
	document.getElementById('messageText').innerHTML = message;
	document.getElementById('whitening').style.display = 'block';
	document.getElementById('message').style.display = 'block';
	
	var buttonOK = document.getElementById('messageOK');
	if (ok)
	{
		buttonOK.style.display = 'block';
		buttonOK.onclick = ok;
	}
	else
	{
		buttonOK.style.display = 'none';
		buttonOK.onclick = undefined;
	}
}


/*	========================================================
	AssetMetadata.prototype.collectFilesForUpload()

	Function. Collect all files, which the user has selected
	for upload within FileBlock widgets.
	========================================================
*/
AssetMetadata.prototype.collectFilesForUpload = function()
{
	var files = [];
	
	var row = this.rootHTML.firstChild;
	while (row && !row.widget)
		row = row.nextSibling;

	files = files.concat(row.widget.files(this,row));
	
	return files;
}

/*	========================================================
	AssetMetadata.prototype.uploadArtefacts(files)

	Command. Uploads artefacts one by one. This should not
	be in the metadata editor, but I was forced to add it.
	========================================================
*/
AssetMetadata.prototype.uploadArtefacts = function(files)
{
	// collect files to upload
	var filesToUpload = [];
	
	for (var i in files)
	{
		var fileList = files[i];
		for (var j=0; j<fileList.length; j++)
		{
			console.log('UPLOADING file',fileList[j].name,'to section',fileList.assetSection,'of asset',options.assetId);
			filesToUpload.push({file:fileList[j],section:fileList.assetSection});
		}
	}

	function uploadFilesNow(index)
	{
		console.log('uploading',filesToUpload[index].file.name);
		function error()
		{
			rage.hideLoader();
			console.log('Could not upload the artefact',filesToUpload[index].file.name);
		}
		
		function progress(event)
		{
			console.log('uploaded',fileSize(event.loaded));
		}
		
		function success(data)
		{
			rage.hideLoader();
			index++;
			if (index<filesToUpload.length)
				uploadFilesNow(index); // recursion into the next files
			else
			{
				console.log('uploading done');
				window.location.href = ASSET_MANAGER_URL;
			}
		}

		rage.showLoader('File №'+index);
		rage.uploadArtefact({success: success, error: error, progress: progress, assetId:options.assetId, size:filesToUpload[index].file.size, file:filesToUpload[index].file, section:filesToUpload[index].section});
	}
	
	if (filesToUpload.length)
		uploadFilesNow(0);
	else
		window.location.href = ASSET_MANAGER_URL;
}

/*	========================================================
	AssetMetadata.prototype.saveXML()

	Command. Generates the XML file and uploads it to the
	server. Supported metadata uploads are only for Asset
	and Artefact.
	========================================================
*/
AssetMetadata.prototype.saveXML = function()
{
	var xml = this.checkXML('<br><br>The metadata cannot be saved to the server if they are incomplete.');
	if (!xml) return;
	xml = xml.replace(/ metaIndex="\d+"/g,'');
	//xml = xml.split('<').join('&lt;');
	//xml = xml.split('>').join('&gt;');
	//xml = xml.split('\t').join('    ');
	
	var files = this.collectFilesForUpload();
	
	var that = this;
	if (this.rootNode.name=='Asset')
	{
		rage.uploadAssetMetadata({
			success: function() {
				// var count = 0;
				// showMessage('Connecting...');
				// var timer = setInterval(function()
				// {
				// 	switch (++count)
				// 	{
				// 		case 1: showMessage('Metadata transfering...'); break;
				// 		case 2: showMessage('Asset saved.'); break;
				// 		case 3:
				// 			clearInterval(timer);
				// 			window.location.href = ASSET_MANAGER_URL;
				// 	} 
				// }, 1000 ); 

				that.uploadArtefacts(files);
			},
			error: function(e) {
				that.errorBox.style.display = 'inline-block';
				that.errorBox.innerHTML += '<div>Cannot save asset\'s metadata.</div>';
			},
			assetId:  options.assetId,
			metadata: xml});
		return;
	}
	
	if (this.rootNode.name=='Artefact')
	{
		rage.uploadArtefactMetadata({
			success: function() {
				window.location.href = ARTEFACT_MANAGER_URL+'?assetId='+options.assetId+'&permissions=rw';
			},
			error: function(e) {
				that.errorBox.style.display = 'inline-block';
				that.errorBox.innerHTML += '<div>Cannot save artefact\'s metadata.</div>';
			},
			assetId:    options.assetId,
			artefactId: options.artefactId,
			metadata:   xml,
			resource: options.resource});
			return;
	}
	
	window.alert('This functionality is suspended for metadata, which is neither Asset nor Artefact');
}



/*	========================================================
	metadiff(meta1,meta2)

	Function. Compares two metadata (in XML format). The old
	metadata is meta1. The new metadata is meta2.
	========================================================
*/
function metadiff(meta1,meta2)
{
	var diffXML = '';
	
	var seri = new XMLSerializer();
	var dom1 = (new window.DOMParser()).parseFromString(meta1, "text/xml").documentElement;
	var dom2 = (new window.DOMParser()).parseFromString(meta2, "text/xml").documentElement;

	function attrs(elem)
	{
		var s = '';
		for (var i=0; i<elem.attributes.length; i++)
		{
			var attr = elem.attributes[i];
			if (attr.name=='metaIndex') continue;
			if (attr.name=='metaSubdex') continue;
			if (attr.name=='name') continue;
			s = s+' '+attr.name+'="'+attr.value+'"';
		}
		return s;
	}
	
	function clearXMLNS(str)
	{
		return clearAttribute(str,' xmlns:');
	}
	
	function clearMETA(str)
	{
		return clearAttribute(str,' metaIndex');
	}
	
	function clearAttribute(str,attr)
	{	// remove xmlns attributes from str
		// to be implemented
		var pos = 0;
		
		//console.log('STR',str);
		// search for ...attr...="..."...
		while (pos>-1)
		{
			// position of attr
			pos = str.indexOf(attr,pos);
			if (pos<0) continue; // no more pos
			
			// position of ="
			var i = str.indexOf('="',pos+1);
			if (i<0) {pos++; continue;};
			
			// position of "
			i = str.indexOf('"',i+2);
			if (i<0) {pos++; continue;};
			
			str = str.substring(0,pos)+str.substring(i+1);
			//console.log('NEW',str);
		}
		//console.log('END',str);
		return str;
	}
	
	function compare(dom1,dom2,level)
	{
		var ident='                                                       '.slice(0,3*level);
		var str1 = seri.serializeToString(dom1);
		var str2 = seri.serializeToString(dom2);
	
		if (str1!=str2)
		{
			diffXML += ident+'<'+dom1.tagName+/*' metaSubdex="'+dom1.getAttribute('metaSubdex')+*/ attrs(dom1)+'>\n';

			// STEP 1: CHECK ATTRIBUTES

			// 1.1. convert attributes to associative arrays
			var attr1={}, attr2={}, elem;
			for (var i=0; i<dom1.attributes.length; i++)
				attr1[dom1.attributes[i].name] = dom1.attributes[i].value;
			for (var i=0; i<dom2.attributes.length; i++)
				attr2[dom2.attributes[i].name] = dom2.attributes[i].value;

			// 1.2. compare attributes old->new (find changed and deleted)
			for (var name in attr1)
			{
				if (! (name in attr2)) // exists in Dom1, missing in Dom2
					diffXML += ident+'   <metadiff:deletedAttribute name="'+name+'"/>\n';
				else if (attr1[name] != attr2[name]) // exists in Dom1 & Dom2, but different value
					diffXML += ident+'   <metadiff:changedAttribute name="'+name+'" value="'+attr2[name]+'" oldValue="'+attr1[name]+'"/>\n';
			}
			
			// 1.3. compare attributes new->old (find newly added)
			for (var name in attr2)
			{
				if (! (name in attr1)) // missing in Dom1, exists in Dom2
					diffXML += ident+'   <metadiff:newAttribute name="'+name+'" value="'+attr2[name]+'"/>\n';
			}

			// STEP 2: CHECK TAGS
			
			// 2.1. collect meta indexes
			var idx1={}, idx2={};
			for (var elem=dom1.firstChild; elem; elem=elem.nextSibling)
				if (elem.nodeType==1 && elem.getAttribute('metaIndex'))
					idx1[elem.getAttribute('metaIndex')] = elem;
			for (var elem=dom2.firstChild; elem; elem=elem.nextSibling)
				if (elem.nodeType==1 && elem.getAttribute('metaIndex'))
					idx2[elem.getAttribute('metaIndex')] = elem;

			// 2.2. compare tag block old->new (find deleted and modified)
			for (var idx in idx1)
			{
				if (! (idx in idx2)) // exists in Dom1, missing in Dom2
				{
					// if there are children, then show them too
					if (idx1[idx].children.length)
					{
						var elem = idx1[idx].children[0];
						diffXML += ident+'   <metadiff:deletedElement name="'+idx1[idx].tagName+'"'+/*' metaSubdex="'+idx1[idx].getAttribute('metaSubdex')+*/ attrs(idx1[idx])+'>\n';
						diffXML += ident+'      '+clearMETA(clearXMLNS(seri.serializeToString(elem)))+'\n';
						diffXML += ident+'   </metadiff:deletedElement>\n';
					}
					else
					{
						diffXML += ident+'   <metadiff:deletedElement name="'+idx1[idx].tagName+'" value="'+idx1[idx].textContent+'"'+/*' metaSubdex="'+idx1[idx].getAttribute('metaSubdex')+*/ attrs(idx1[idx])+'/>\n';
					}
				}
				else
				{
					compare(idx1[idx],idx2[idx],level+1);
				}
			}

			// 2.3. find newly added
			for (var elem=dom2.firstChild; elem; elem=elem.nextSibling)
				if (elem.nodeType==1 && !elem.getAttribute('metaIndex'))
				{
					diffXML += ident+'   <metadiff:newElement>\n';
					console.log('a',seri.serializeToString(elem));
					diffXML += ident+'      '+clearMETA(clearXMLNS(seri.serializeToString(elem)))+'\n';
					console.log('a',clearMETA(clearXMLNS(seri.serializeToString(elem))));
					diffXML += ident+'   </metadiff:newElement>\n';
				}
			
			// STEP 3: CHECK TEXT CONTENT
			
			if (dom1.children.length==0)
			if (dom2.children.length==0)
			if (dom1.textContent!=dom2.textContent)
			{
				diffXML += ident+'   <metadiff:changedValue value="'+dom2.textContent.trim()+'" oldValue="'+dom1.textContent.trim()+'"'+attrs(dom2)+'/>\n';
			}
			
			diffXML += ident+'</'+dom1.tagName+'>\n';
		}
		//else console.log('@',str1);
	}
	
	compare(dom1,dom2,0);
	//console.log(diffXML);

	diffXML = diffXML.split('<').join('&lt;');
	diffXML = diffXML.split('>').join('&gt;');
	diffXML = diffXML.split('\t').join('    ');
	x=window.open();
	x.document.open();
	x.document.write('<!DOCTYPE html><head><meta charset="utf-8"/></head><body style="zoom: 100%"><pre>'+diffXML+'</pre></body>');
	x.document.close();
}


function onConfigFileSelect(event) {
	//console.table(event.target.files);
	if (event.target.files.length < 1)
		return;

	// take only the first file
	var file = event.target.files[0]; 
	
	// exit if the file extension is not json
	if (file.name.slice(-4)!='.xml') return;

	
	var reader = new FileReader();

	reader.onload = function (e) {
		EXTERN.XML = e.target.result;
		asset = new AssetMetadata('','assetbox');
		document.getElementById('loadButton').style.display = 'none';
	};

	reader.readAsText(file);
	document.getElementById('selectFileJS').value = '';
}
