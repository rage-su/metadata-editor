<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema" xmlns:rage="http://rageproject.eu/2015/asset/"
    targetNamespace="http://rageproject.eu/2015/asset/" elementFormDefault="qualified"
    xmlns:dcterms="http://purl.org/dc/terms/" xmlns:adms="http://www.w3.org/ns/adms#"
    xmlns:dcat="http://www.w3.org/ns/dcat#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron">

    <annotation>
        <appinfo>
            <sch:title>Schematron validation</sch:title>
            <sch:ns prefix="rage" uri="http://rageproject.eu/2015/asset/"/>
        </appinfo>
    </annotation>

    <import namespace="http://www.w3.org/1999/02/22-rdf-syntax-ns#" schemaLocation="rdf.xsd"/>

    <import namespace="http://purl.org/dc/terms/" schemaLocation="dcterms.xsd"/>
    <import namespace="http://www.w3.org/ns/dcat#" schemaLocation="dcat.xsd"/>
    <import namespace="http://www.w3.org/ns/adms#" schemaLocation="adms.xsd"/>
    <import namespace="http://xmlns.com/foaf/0.1/" schemaLocation="foaf.xsd"/>

    <complexType name="OwnerType">
        <annotation>
            <documentation>
                A person or organisation that is the owner of the asset
            </documentation>
        </annotation>
        <sequence>
            <choice>
                <element ref="foaf:Organizaton"/>
                <element ref="foaf:Person"/>
            </choice>
        </sequence>
    </complexType>

    <complexType name="LicenseDocumentType">
        <annotation>
            <documentation>
                Conditions or restrictions that apply to the use of an asset or artefact
            </documentation>
        </annotation>
        <complexContent>
            <extension base="dcterms:LicenseDocument">
                <sequence>
                    <element ref="dcat:accessURL" minOccurs="0" maxOccurs="1">
					<annotation>
						<documentation>
							Access URL of a license
						</documentation>
					</annotation>
				  </element>	
                </sequence>
                <attribute name="url" type="anyURI" use="optional"/>
            </extension>
        </complexContent>
    </complexType>

    <complexType name="ArtefactType">
        <annotation>
            <documentation>
                A physical element of an asset corresponding to a file on a file system
            </documentation>
        </annotation>
        <sequence>
            <element name="reference" type="rdf:LiteralType" maxOccurs="1" minOccurs="0">
				<annotation>
					<documentation>
						The relative file path of the artefact within the project or package
					</documentation>
				</annotation>
			</element>	
            <element ref="dcterms:title" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Title of the artefact 
					</documentation>
				</annotation>
			</element>	
            <element ref="dcterms:description" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						General description for the artefact 
					</documentation>
				</annotation>
			</element>	
            <element ref="dcterms:type" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Artefact type
					</documentation>
				</annotation>
			</element>	
            <element ref="dcterms:date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Date of last update of the artefact in YYYY-MM-DD format 
					</documentation>
				</annotation>
			</element>	
            <element ref="dcterms:creator" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						A person or organisation that created the artefact 
					</documentation>
				</annotation>
			</element>	
            <element ref="dcterms:publisher" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						A person or organisation that published the artefact 
					</documentation>
				</annotation>
			</element>	
            <element name="versionInfo" type="string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Version number in major.minor.rev.build format 
					</documentation>
				</annotation>
			</element>	
            <element ref="dcterms:format" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						File format of the artefact (MIME Format) 
					</documentation>
				</annotation>
			</element>	
            <element ref="dcat:accessURL" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Access URL of the artefact 
					</documentation>
				</annotation>
			</element>	
            <element name="License" type="rage:LicenseDocumentType" minOccurs="0" maxOccurs="1"/>
            <element name="CustomMetadata" type="rage:CustomMetadataType" minOccurs="0"
                maxOccurs="unbounded">
				<annotation>
					<documentation>
						  User defined custom metadata 
					</documentation>
				</annotation>
			</element>	
        </sequence>
        <attribute name="name" type="string" use="required">
			<annotation>
				<documentation>
					Name of the artefact 
				</documentation>
			</annotation>
		</attribute>	
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>

    <complexType name="ContextType">
        <annotation>
            <documentation>
                A context defines a conceptual frame, which helps explain the meaning of other elements in the asset.
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:title" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Title of the classification context
					</documentation>
				</annotation>
			</element>
            <element ref="dcterms:description" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						General description of the classification context
					</documentation>
				</annotation>
			</element>
            <element ref="dcat:themeTaxonomy" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						The knowledge organization system (KOS) used to classify assets within the context
					</documentation>
				</annotation>
			</element>
            <element ref="dcat:theme" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						The main category/theme the asset is classified to. An asset can have multiple themes.
					</documentation>
				</annotation>
			</element>
        </sequence>
        <attribute name="name" type="string" use="required">
			<annotation>
				<documentation>
					Unique name of the classification context
				</documentation>
			</annotation>
		</attribute>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>
    <complexType name="ClassificationType">
        <annotation>
            <documentation>
                Classification section, listing a set of descriptors for classifying the asset as well as a description of the context(s) for which the asset is relevant
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:description" minOccurs="1" maxOccurs="unbounded">
				<annotation>
					<documentation>
						General description
					</documentation>
				</annotation>
			</element>
            <element name="Context" type="rage:ContextType" minOccurs="1" maxOccurs="unbounded"/>
        </sequence>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>

    <complexType name="RequirementsType">
        <annotation>
            <documentation>
                Requirements section, containing artefacts like models, use-cases, diagrams.
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:description" minOccurs="0" maxOccurs="unbounded"/>
            <element name="Artefact" type="rage:ArtefactType" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						A physical element of an asset corresponding to a file on a file system
					</documentation>
				</annotation>
			</element>	
        </sequence>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>
    <complexType name="DesignType">
        <annotation>
            <documentation>
                Design section, containing artefacts like diagrams, models, interface specifications, etc.
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:description" minOccurs="0" maxOccurs="unbounded"/>
            <element name="Artefact" type="rage:ArtefactType" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						A physical element of an asset corresponding to a file on a file system
					</documentation>
				</annotation>
			</element>	
        </sequence>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>
    <complexType name="ImplementationType">
        <annotation>
            <documentation>
                The Implementation section has a collection of artefacts, that identify the binary and other files that provide the component implementation.
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:description" minOccurs="0" maxOccurs="unbounded"/>
            <element name="gameEngine" type="rdf:ResourceType" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Target game engine of the implementation
					</documentation>
				</annotation>
			</element>	
            <element name="gamePlatform" type="rdf:ResourceType" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Target game platform of the implementation
					</documentation>
				</annotation>
			</element>	
            <element name="progLanguage" type="rdf:ResourceType" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Programming language used for the implementation
					</documentation>
				</annotation>
			</element>	
            <element name="CustomMetadata" type="rage:CustomMetadataType" minOccurs="0"
                maxOccurs="unbounded">
				<annotation>
					<documentation>
						User defined custom metadata 
					</documentation>
				</annotation>
			</element>	
            <element name="Artefact" type="rage:ArtefactType" minOccurs="1" maxOccurs="unbounded">
				<annotation>
					<documentation>
						A physical element of an asset corresponding to a file on a file system
					</documentation>
				</annotation>
			</element>	
        </sequence>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>
    <complexType name="TestsType">
        <annotation>
            <documentation>
                The Tests section contains artefacts (models, diagrams, artefacts, and so on) that are intended to describe the testing of the component.
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:description" minOccurs="0" maxOccurs="unbounded"/>
            <element name="Artefact" type="rage:ArtefactType" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						A physical element of an asset corresponding to a file on a file system
					</documentation>
				</annotation>
			</element>	
        </sequence>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>

    <complexType name="UsageType">
        <annotation>
            <documentation>
                Usage section, containing information for installing, customizing, and using the asset.
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:description" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Description about installing, customizing and using the asset 
					</documentation>
				</annotation>
			</element>	
            <element name="Artefact" type="rage:ArtefactType" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						A physical element of an asset corresponding to a file on a file system
					</documentation>
				</annotation>
			</element>	
        </sequence>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>

    <complexType name="SolutionType">
        <annotation>
            <documentation>
                Solution section, describing the artefacts of the asset.
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:description" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						  General description of the solution 
					</documentation>
				</annotation>
			</element>	
            <element name="Requirements" type="rage:RequirementsType" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						  Collection of artefacts defining the requirements of the asset, like use-cases, models, diagrams, etc. 
					</documentation>
				</annotation>
			</element>	
            <element name="Design" type="rage:DesignType" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Collection of artefacts defining the design of the asset, like diagrams, models, interface specifications, etc.
					</documentation>
				</annotation>
			</element>	
            <element name="Implementation" type="rage:ImplementationType" minOccurs="1"
                maxOccurs="1">
				<annotation>
					<documentation>
						Collection of artefacts defining the implementation of the asset, like binary and other files 
					</documentation>
				</annotation>
			</element>	
            <element name="Tests" type="rage:TestsType" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Collection of artefacts defining the testing of the asset, like models, diagrams, test cases, etc. 
					</documentation>
				</annotation>
			</element>	
        </sequence>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>

    <complexType name="RelatedAssetType">
        <annotation>
            <documentation>
                Related Asset element, describing this asset’s relationship to other assets.
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:description" minOccurs="1" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Description of the related asset 
					</documentation>
				</annotation>
			</element>
            <element name="relationType" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Type of the relationship to the asset
					</documentation>
				</annotation>
                <simpleType>
                    <restriction base="string">
                        <enumeration value="isRequiredBy"/>
                        <enumeration value="requires"/>
                        <enumeration value="isPartOf"/>
                        <enumeration value="hasPart"/>
                        <enumeration value="nextVersion"/>
                        <enumeration value="prevVersion"/>
                    </restriction>
                </simpleType>
            </element>
            <element ref="dcat:accessURL" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Access URL of the related asset
					</documentation>
				</annotation>
			</element>
        </sequence>
        <attribute name="name" type="string" use="required">
			<annotation>
				<documentation>
					Name of the related asset 
				</documentation>
			</annotation>
		</attribute>
        <attribute type="string" name="minVersion" use="required">
			<annotation>
				<documentation>
					The minimal version of the related asset 
				</documentation>
			</annotation>
		</attribute>
        <attribute type="string" name="maxVersion" use="required">
			<annotation>
				<documentation>
					The maximal version of the related asset; <br />if it is the same as the minimal, then a concrete version of the related asset is related 
				</documentation>
			</annotation>
		</attribute>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>

    <complexType name="CustomMetadataType">
        <annotation>
            <documentation>
                An element that serves as an extension point for adding custom metadata as name/value pairs.
            </documentation>
        </annotation>
        <sequence>
            <element name="name" type="string">
				<annotation>
					<documentation>
						Custom name 
					</documentation>
				</annotation>
			</element>
            <element name="value" type="string">
				<annotation>
					<documentation>
						Custom value
					</documentation>
				</annotation>
			</element>
        </sequence>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>

    <complexType name="AssetType">
        <annotation>
            <documentation>
                An abstract entity that reflects the intellectual content of the asset.
            </documentation>
        </annotation>
        <sequence>
            <element ref="dcterms:title" minOccurs="1" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Title of the asset 
					</documentation>
				</annotation>
			</element>
            <element ref="dcterms:description" minOccurs="1" maxOccurs="unbounded">
				<annotation>
					<documentation>
						General description of the asset 
					</documentation>
				</annotation>
			</element>
            <element ref="dcterms:type" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						A general classification of the asset
					</documentation>
				</annotation>
			</element>
            <element ref="dcterms:date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Date of last update of the asset in YYYY-MM-DD format
					</documentation>
				</annotation>
			</element>
            <element ref="dcterms:language" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Language used in the asset for user interface, error messages, etc. 
					</documentation>
				</annotation>
			</element>
            <element ref="dcterms:creator" minOccurs="1" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Person or organisation that created the asset 
					</documentation>
				</annotation>
			</element>
            <element ref="dcterms:publisher" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Person or organisation that published the asset 
					</documentation>
				</annotation>
			</element>
            <element name="owner" type="rage:OwnerType" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Person or organisation that owns the asset 
					</documentation>
				</annotation>
			</element>
            <element ref="dcat:keyword" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						A keyword or tag describing the asset.
					</documentation>
				</annotation>
			</element>
            <element name="versionInfo" type="string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Version number in major.minor.rev.build format 
					</documentation>
				</annotation>
			</element>
            <element ref="adms:versionNotes" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Description of changes since the previous version 
					</documentation>
				</annotation>
			</element>
            <element ref="adms:status" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Status of the asset
					</documentation>
				</annotation>
			</element>
            <element name="maturityLevel" type="string"  minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						The maturity level of the asset 
					</documentation>
				</annotation>
			</element>
            <element ref="dcat:accessURL" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Access URL of the asset
					</documentation>
				</annotation>
			</element>
            <element name="CustomMetadata" type="rage:CustomMetadataType" minOccurs="0"
                maxOccurs="unbounded">
				<annotation>
					<documentation>
						User defined custom metadata
					</documentation>
				</annotation>
			</element>
            <element maxOccurs="unbounded" minOccurs="0" name="RelatedAsset"
                type="rage:RelatedAssetType"/>
            <element maxOccurs="1" minOccurs="1" name="Classification"
                type="rage:ClassificationType"/>
            <element maxOccurs="1" minOccurs="1" name="Solution" type="rage:SolutionType"/>
            <element maxOccurs="1" minOccurs="1" name="Usage" type="rage:UsageType"/>
            <element maxOccurs="1" minOccurs="0" name="License" type="rage:LicenseDocumentType"/>
        </sequence>
        <attribute name="name" type="string" use="required"/>
        <attribute name="url" type="anyURI" use="optional"/>
    </complexType>

    <element name="Asset" type="rage:AssetType">
        <annotation>
            <documentation>
                An abstract entity that reflects the intellectual content of the asset.
            </documentation>
        </annotation>
    </element>

    <element name="Artefact" type="rage:ArtefactType">
		<annotation>
			<documentation>
				A physical element of an asset corresponding to a file on a file system
			</documentation>
		</annotation>
    </element>
    
</schema>
