<?xml version="1.0" encoding="UTF-8"?>
<!--

  *	config.xsd
	Schema file for an imaginary 3D Maze Asset configuration  metadata.

  * Copyright 2016 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
-->
<schema xmlns="http://www.w3.org/2001/XMLSchema" xmlns:rage="http://rageproject.eu/2015/maze3d/"
    targetNamespace="http://rageproject.eu/2015/maze3d/" elementFormDefault="qualified"
    xmlns:dcterms="http://purl.org/dc/terms/" xmlns:adms="http://www.w3.org/ns/adms#"
    xmlns:dcat="http://www.w3.org/ns/dcat#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron">
	
    <import namespace="http://www.w3.org/1999/02/22-rdf-syntax-ns#" schemaLocation="rdf.xsd"/>

    <import namespace="http://purl.org/dc/terms/" schemaLocation="dcterms.xsd"/>
    <import namespace="http://www.w3.org/ns/dcat#" schemaLocation="dcat.xsd"/>
    <import namespace="http://www.w3.org/ns/adms#" schemaLocation="adms.xsd"/>
    <import namespace="http://xmlns.com/foaf/0.1/" schemaLocation="foaf.xsd"/>
	
	
    <complexType name="Maze3D.Type">
        <sequence>
            <element name="about" type="rdf:LiteralType" minOccurs = "0" maxOccurs = "unbounded"/>
            <element name="size" type="rage:MazeSize" minOccurs = "1" maxOccurs = "1"/>
            <element name="style" type="rage:MazeStyle" minOccurs = "0" maxOccurs = "1"/>
            <element name="nvidia" type="rage:nvidia" minOccurs = "1" maxOccurs = "1"/>
            <element name="intel" type="rage:intel" minOccurs = "1" maxOccurs = "1"/>
            <element name="agent" type="rage:AgentType" minOccurs = "0" maxOccurs = "unbounded"/>
        </sequence>
    </complexType>
    
    <complexType name="MazeSize">
        <sequence>
            <element name="width" type="string" minOccurs = "1" maxOccurs = "1"/>
            <element name="height" type="string" minOccurs = "1" maxOccurs = "1"/>
        </sequence>
    </complexType>

    <complexType name="MazeStyle">
        <sequence>
            <element name="connectivity" minOccurs = "0" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="low"/>
						<enumeration value="medium"/>
						<enumeration value="high"/>
					</restriction>
				</simpleType>
			</element>
            <element name="density" minOccurs = "1" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="sparse"/>
						<enumeration value="dense"/>
					</restriction>
				</simpleType>
			</element>
        </sequence>
    </complexType>
    
    <complexType name="nvidia">
        <sequence>
            <element name="ao" minOccurs = "0" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="on"/>
						<enumeration value="off"/>
					</restriction>
				</simpleType>
			</element>
            <element name="antialias" minOccurs = "0" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="2x"/>
						<enumeration value="8x"/>
						<enumeration value="8x CSAA"/>
						<enumeration value="16xQ CSAA"/>
						<enumeration value="32x CSAA"/>
					</restriction>
				</simpleType>
			</element>
            <element ref="rage:power" minOccurs = "0" maxOccurs = "unbounded"/>
        </sequence>
    </complexType>

	<complexType name="intel">
        <sequence>
            <element name="color" minOccurs = "1" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="16 Bit"/>
						<enumeration value="32 Bit"/>
					</restriction>
				</simpleType>
			</element>
            <element name="refresh" type="string" minOccurs = "0" maxOccurs = "1"/>
            <element name="vsync" minOccurs = "1" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="on"/>
						<enumeration value="off"/>
					</restriction>
				</simpleType>
			</element>
            <element ref="rage:power" minOccurs = "0" maxOccurs = "unbounded"/>
        </sequence>
    </complexType>
	
	<complexType name="power">
        <sequence>
            <element name="source" minOccurs = "1" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="battery"/>
						<enumeration value="plugged"/>
					</restriction>
				</simpleType>
			</element>
            <element name="mode" minOccurs = "1" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="adaptive"/>
						<enumeration value="max performance"/>
						<enumeration value="max battery"/>
					</restriction>
				</simpleType>
			</element>
        </sequence>
    </complexType>
	
	<complexType name="AgentType">
        <sequence>
            <element name="min" type="string" minOccurs = "0" maxOccurs = "1"/>
            <element name="max" type="string" minOccurs = "0" maxOccurs = "1"/>
            <element name="voice" minOccurs = "0" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="male"/>
						<enumeration value="female"/>
						<enumeration value="robot"/>
					</restriction>
				</simpleType>
			</element>
            <element name="data" type="rdf:ResourceType" minOccurs = "1" maxOccurs = "1"/>
            <element name="behaviour" minOccurs = "0" maxOccurs = "1">
				<simpleType>
					<restriction base="string">
						<enumeration value="agressive"/>
						<enumeration value="passive"/>
						<enumeration value="indifferent"/>
					</restriction>
				</simpleType>
			</element>
        </sequence>
    </complexType>
	
    <!-- Declaration of the root element(s) ***************************************************************  -->
    
    <element name="maze3d" type="rage:Maze3D.Type"/>
    
    <!-- =====================================  -->

</schema>
