Version date 2016-06-03
=======================

Changes:

1. Added element "Artefact" to DefaultProfile.xsd (required by Pavel)

2. Added optional "url" attribute to the following elements
(file DefaultProfile.xsd)
rage:Asset
rage:Artefact
rage:Solution
rage:Requirements
rage:Design
rage:Implementation
rage:Tests
rage:Usage
rage:Classification
rage:Context
rage:License
rage:RelatedAsset
rage:CustomMetadata

(file foaf.xsd)
foaf:Person
foaf:Organization

3. Added element "Artefact" to element "Asset"
<element name="Artefact" type="rage:ArtefactType" minOccurs="0" maxOccurs="unbounded"/>

4. Added element "maturityLevel" to element "Asset"
<element name="maturityLevel" type="string"  minOccurs="0" maxOccurs="1"/>

Version date 2016-04-13
========================
Initial Version
